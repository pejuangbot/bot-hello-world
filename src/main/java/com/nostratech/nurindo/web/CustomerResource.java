package com.nostratech.nurindo.web;


import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.CustomerContactPersonService;
import com.nostratech.nurindo.service.CustomerService;
import com.nostratech.nurindo.service.dto.CustomerDTO;
import com.nostratech.nurindo.service.dto.CustomerListDTO;
import com.nostratech.nurindo.service.dto.CustomerProductRoleDTO;
import com.nostratech.nurindo.service.dto.ResultPageDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class CustomerResource {
    public static final String ENTITY_NAME = "customer";

    private final CustomerService customerService;

    private final CustomerContactPersonService customerContactPersonService;

    public CustomerResource(CustomerService customerService, CustomerContactPersonService customerContactPersonService) {
        this.customerService = customerService;
        this.customerContactPersonService = customerContactPersonService;
    }


    /**
     * POST  /customers  : Creates a new Customer.
     *
     * @param customerDTO the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request)
     *         if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BadRequestAlertException 400 (Bad Request) if the login or email is already in use
     */

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "insert new Customers")
    @PostMapping("/customers")
    public ResponseEntity<Boolean> createCustomers(@RequestBody CustomerDTO customerDTO) throws URISyntaxException {
        log.debug("REST request to save Menu : {}", customerDTO);
        if (customerDTO.getId() != null) {
            throw new BadRequestAlertException("A new task cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Boolean result = customerService.createCustomer(customerDTO);
        return ResponseEntity.created(new URI("/api/customers/" + customerDTO.getId()))
                .headers(HeaderUtil.createAlert( "Customers.created", customerDTO.getCustomerName()))
                .body(result);
    }

    /**
     * PUT /customers : Updates an existing Customer.
     *
     * @param customerDTO the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user
     */

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "update new Customers")
    @PutMapping("/customers")
    public ResponseEntity<Boolean> updateCustomers(@Valid @RequestBody CustomerDTO customerDTO) throws URISyntaxException {
        log.debug("REST request to save Customer : {}", customerDTO);
        if (customerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid ID to Update", ENTITY_NAME, "idnotexists");
        }

        Boolean result = customerService.updateCustomer(customerDTO);
        return ResponseEntity.created(new URI("/api/users/" + customerDTO.getId()))
                .headers(HeaderUtil.createAlert( "Customers.updated", customerDTO.getCustomerName()))
                .body(result);
    }


    /**
     * GET  /customers : get all the menus.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of customer in body
     */

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get customers detail")
    @GetMapping("/customers/{customerId}")
    public ResponseEntity<CustomerListDTO> getDetailCustomersAndContactPerson(@PathVariable Long customerId) {
        return ResponseUtil.wrapOrNotFound(
                customerService.getCustomersById(customerId)
        );
    }


    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get detail product price based on customer and role")
    @GetMapping("/customers/price")
    public ResponseEntity<ResultPageDTO> getDetailPriceByCustomerId(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                                             @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                                             @RequestParam(value = "sortBy", required = false) String sortBy,
                                                                             @RequestParam(value = "direction", required = false) String direction,
                                                                             @RequestParam(value = "customerId", required = false) Long customerId,
                                                                             @RequestParam(value = "roleId", required = false) Long roleId,
                                                                             @RequestParam(value = "productName", required = false) String productName,
                                                                             @RequestParam(value = "productType", required = false) String productType) {
        Map<String, Object> pageMap = customerService.getDetailPriceByCustomerId(page, limit, sortBy, direction, customerId, roleId, productName, productType);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }


    /**
     * GET /products : get all products.
     *
     * @param page the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all products
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all customers")
    @GetMapping("/list/customers")
    public ResponseEntity<ResultPageDTO> getAllCustomers(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                         @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                         @RequestParam(value = "sortBy", required = false) String sortBy,
                                                         @RequestParam(value = "direction", required = false) String direction,
                                                         @RequestParam(value = "customerName", required = false) String customerName,
                                                         @RequestParam(value = "roleId", required = false) Long roleId) {
        Map<String, Object> pageMap = customerService.getAllCustomerByName(page, limit, sortBy, direction, customerName, roleId);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }


    /**
     * DELETE  /roles/:id : delete the "id" role.
     *
     * @param id the id of the roleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "delete role base on id")
    @DeleteMapping("/contact/person/{id}")
    public ResponseEntity<Boolean> deleteCustomerContactPerson(@PathVariable Long id) {
        log.debug("REST request to delete Roles : {}", id);
        Boolean result = customerContactPersonService.delete(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert( "Customers.updated", id.toString()))
                .body(result);
    }

}
