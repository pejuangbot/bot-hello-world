package com.nostratech.nurindo.web;

import com.nostratech.nurindo.config.RoleListConfig;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.exception.EmailAlreadyUsedException;
import com.nostratech.nurindo.exception.UsernameAlreadyUsedException;
import com.nostratech.nurindo.repository.UserRepository;
import com.nostratech.nurindo.service.EmailService;
import com.nostratech.nurindo.service.UserService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * REST controller for managing users.
 * <p>
 * This class accesses the User entity, and needs to fetch its collection of authorities.
 * <p>
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.</li>
 * <li> Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).</li>
 * <li> As this manages users, for security reasons, we'd rather have a DTO layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1")
public class UserResource {

    @Autowired
    TokenStore tokenStore;

    private final UserService userService;

    private final UserRepository userRepository;

    private final RoleListConfig roleListConfig;

    private final EmailService emailService;

    public UserResource(UserService userService, UserRepository userRepository, RoleListConfig roleListConfig, EmailService emailService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.roleListConfig = roleListConfig;
        this.emailService = emailService;
    }

    /**
     * POST  /users  : Creates a new user.
     * <p>
     * Creates a new user if the login and email are not already used, and sends an
     * mail with an activation link.
     * The user needs to be activated on creation.
     *
     * @param userDTO the user to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request)
     *         if the login or email is already in use
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BadRequestAlertException 400 (Bad Request) if the login or email is already in use
     */
//    @PreAuthorize("isAuthenticated()")
    @PostMapping("/users")
    @ApiOperation(value = "Creates a new user")
    @ApiResponses( value = {
            @ApiResponse(code = 201, message = "User created"),
            @ApiResponse(code = 400, message = "A new user cannot already have an same username or email is already in use")
    })
    public ResponseEntity<Boolean> createUser(@Valid @RequestBody @ApiParam(name = "body",
            value = "Creates a new user if the login and email are not already used, and sends and mail with an activation link",
            required = true)
                                                      UserCreateDTO userDTO) throws URISyntaxException {
        log.debug("REST request to save User : {}", userDTO);
        if (userDTO.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
            // Lowercase the username before comparing with database
        } else if (userRepository.findOneByUsername(userDTO.getUsername().toLowerCase()).isPresent()) {
            throw new UsernameAlreadyUsedException();
        } else if (userRepository.findOneByEmailIgnoreCase(userDTO.getEmail()).isPresent()) {
            throw new EmailAlreadyUsedException();
        } else {
            Boolean newUser = userService.createUser(userDTO);

            //TODO: send to notification engine
            return ResponseEntity.created(new URI("/api/users/" + userDTO.getUsername()))
                    .headers(HeaderUtil.createAlert( "userManagement.created", userDTO.getUsername()))
                    .body(newUser);
        }
    }

    /**
     * PUT /users : Updates an existing User.
     *
     * @param userDTO the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already in use
     * @throws UsernameAlreadyUsedException 400 (Bad Request) if the username is already in use
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing User")
    @PutMapping("/users")
    public ResponseEntity<Boolean> updateUser(@Valid @RequestBody UserUpdateDTO userDTO) throws URISyntaxException {
        log.debug("REST request to update User : {}", userDTO);
        Optional<User> existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.getEmail());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new EmailAlreadyUsedException();
        }
        existingUser = userRepository.findOneByUsername(userDTO.getUsername().toLowerCase());
        if (existingUser.isPresent() && (!existingUser.get().getId().equals(userDTO.getId()))) {
            throw new UsernameAlreadyUsedException();
        }
        Boolean updatedUser = userService.updateUser(userDTO);

        return ResponseEntity.created(new URI("/api/users/" + userDTO.getUsername()))
                .headers(HeaderUtil.createAlert( "userManagement.created", userDTO.getUsername()))
                .body(updatedUser);
    }

    /**
     * GET /users : get all users.
     *
     * @param page the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all users
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all users")
    @GetMapping("/users")
    public ResponseEntity<ResultPageDTO> getAllUsers(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                     @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                     @RequestParam(value = "sortBy", required = false) String sortBy,
                                                     @RequestParam(value = "direction", required = false) String direction,
                                                     @RequestParam(value = "username", required = false, defaultValue = "") String username,
                                                     @RequestParam(value = "role", required = false, defaultValue = "") String role) {
        Map<String, Object> pageMap = userService.getAllUsers(page, limit, sortBy, direction, username, role);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }

    /**
     * GET /users/:username : get the "username" user.
     *
     * @param username the username of the user to find
     * @return the ResponseEntity with status 200 (OK) and with body the "username" user, or with status 404 (Not Found)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get user base on username")
    @GetMapping("/users/{username:" + Constants.User.USERNAME_REGEX + "}")
    public ResponseEntity<UserDetailDTO> getUser(@PathVariable String username) {
        log.debug("REST request to get User : {}", username);

        return ResponseUtil.wrapOrNotFound(
                userService.getUser(username));
    }

    /**
     * DELETE /users/:username : delete the "username" User.
     *
     * @param username the username of the user to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Delete user base on username")
    @DeleteMapping("/users/{username:" + Constants.User.USERNAME_REGEX + "}")
    public ResponseEntity<Void> deleteUser(@PathVariable String username) {
        log.debug("REST request to delete User: {}", username);
        userService.deleteUser(username);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "userManagement.deleted", username)).build();
    }

    /**
     * POST /users/change-password/:id : change password user.
     *
     * @param id the id of the user
     * @param changePasswordDTO the dto to change password
     *
     * @return boolean true
     */
    @ApiOperation(value = "Change password base on id and username")
    @PostMapping("/users/change-password/{id}")
    public ResponseEntity<Boolean> changePassword(@PathVariable(value = "id") String id,
                                                  @Valid @RequestBody ChangePasswordDTO changePasswordDTO) throws URISyntaxException {
        log.debug("REST request to change password user: {}", id + " " + changePasswordDTO);
        Boolean user = userService.changePassword(id, changePasswordDTO);

        return ResponseEntity.created(new URI("/api/users/" + id))
                .headers(HeaderUtil.createAlert( "userManagement.created", id))
                .body(user);
    }

    @ApiOperation(value = "send Mail Forgot Password")
    @PostMapping("/sendEmailForgotPassword")
    public ResponseEntity<Boolean> sendMailForgotPassword(@RequestBody EmailDTO emailDTO) throws URISyntaxException {
        log.debug("REST request to end Mail Forgot Password : {}", emailDTO);
        Boolean result = emailService.sendEmailConfirmPassword(emailDTO);
        return ResponseEntity.created(new URI("/api/users/" + emailDTO.getRecepients()))
                .headers(HeaderUtil.createAlert( "userManagement.created", emailDTO.getRecepients()))
                .body(result);
    }

    /**
     * PUT  /users/change-status : Change status a existing user.
     *
     * @param id the id of user
     * @param statusDto the statusDto to change status user
     * @return boolean true if change status is success
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Change status a existing users")
    @PreAuthorize("isAuthenticated()")
    @PutMapping("/users/change-status/{id}")
    public ResponseEntity<Boolean> changeStatus(@RequestBody StatusDTO statusDto,
                                                @PathVariable Long id) throws URISyntaxException{
        log.debug("REST request to save status User : {}", statusDto);
        Boolean result = userService.changeStatus(id, statusDto);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("user", statusDto.getActivated().toString()))
                .body(result);
    }


    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all user by role")
    @GetMapping("/users/by-role")
    public ResponseEntity<List<UserDTO>> getUserByRole(@RequestParam(value = "name") String name) {
        log.debug("REST request to get all user : {}", name);
        final List<UserDTO> result = userService.findAllByRoles(name);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("user", result.toString()))
                .body(result);
    }


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/logout")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Boolean> logout(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null) {
            String tokenValue = authHeader.replace("Bearer", "").trim();
            OAuth2AccessToken accessToken = tokenStore.readAccessToken(tokenValue);
            tokenStore.removeAccessToken(accessToken);
        }
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("user", authHeader))
                .body(Boolean.TRUE);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/list-role-menu")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<RoleListDTO>> getRoleAndMenu(OAuth2Authentication authentication) {
        final List<RoleListDTO> result = roleListConfig.getRoleAndMenu(authentication);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("user", result.toString()))
                .body(result);
    }
}
