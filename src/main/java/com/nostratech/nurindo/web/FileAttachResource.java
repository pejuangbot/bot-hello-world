package com.nostratech.nurindo.web;

import com.nostratech.nurindo.service.FileAttachService;
import com.nostratech.nurindo.service.ReportService;
import com.nostratech.nurindo.service.dto.FileUploadDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class FileAttachResource {

    private final FileAttachService fileAttachService;

    private final ReportService reportService;

    public FileAttachResource(FileAttachService fileAttachService, ReportService reportService) {
        this.fileAttachService = fileAttachService;
        this.reportService = reportService;
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "send Mail Forgot Password")
    @PostMapping(value = "/upload/{fileType}",
        consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<FileUploadDTO> uploadFile(MultipartFile file,
                                                    @PathVariable String fileType
    ) throws MaxUploadSizeExceededException {
        return ResponseUtil.wrapOrNotFound(
                fileAttachService.uploadFile(file, fileType)
        );
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get Report PQ detail")
    @GetMapping("/exportReport")
    public void exportPriceQuotation(HttpServletResponse response,
                                 @RequestParam("idPq") long idPq,
                                 @RequestParam("docType") String docType) throws IOException {
        Map<String, Object> mapFile = reportService.exportReportPriceQuotation(docType, idPq);
        File file = (File) mapFile.get("file");

        System.out.println();
        InputStream inputStream = new FileInputStream(file);
        response.setHeader("Content-Disposition", "filename=" + file.getName());
        IOUtils.copy(inputStream, response.getOutputStream());

        response.flushBuffer();
        inputStream.close();
    }
}
