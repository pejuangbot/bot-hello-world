package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.PrinterService;
import com.nostratech.nurindo.service.dto.MaterialDTO;
import com.nostratech.nurindo.service.dto.PrinterDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class PrinterResource {

    public static final String ENTITY_NAME = "material";

    @Autowired
    private PrinterService printerService;

    /**
     * POST  /printers : Create a new printer.
     *
     * @param printerDTO the printerDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new materialDTO, or with status 400 (Bad Request) if the task has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Creates a new printer")
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/printers")
    public ResponseEntity<Boolean> createPrinter(@RequestBody PrinterDTO printerDTO) throws URISyntaxException{
        log.debug("REST request to save material : {}", printerDTO);
        if (printerDTO.getId() != null) {
            throw new BadRequestAlertException("A new printer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Boolean result = printerService.save(printerDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, printerDTO.getName()))
                .body(result);
    }

    /**
     * PUT  /printers : Updates an existing printer.
     *
     * @param printerDTO the printerDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated materialDTO,
     * or with status 400 (Bad Request) if the materialDTO is not valid,
     * or with status 500 (Internal Server Error) if the materialDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing material")
    @PutMapping("/printers")
    public ResponseEntity<Boolean> updateMaterial(@RequestBody PrinterDTO printerDTO) throws URISyntaxException{
        log.debug("REST request to update printer : {}", printerDTO);
        if (printerDTO.getId() == null){
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Boolean result = printerService.save(printerDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, printerDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET  /printers : get all the printers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of materials in body
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all printers")
    @GetMapping("/printers")
    public ResponseEntity<List<PrinterDTO>> getAllPrinter() {
        log.debug("get all printers");
        final List<PrinterDTO> result = printerService.findAll();
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.toString()))
                .body(result);
    }

    /**
     * GET  /printers/:id : get the "id" printer.
     *
     * @param id the id of the materialDto to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuDTO, or with status 404 (Not Found)
     */
    @ApiOperation(value = "get printer base on id")
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/printers/{id}")
    public ResponseEntity<PrinterDTO> getPrinter(@PathVariable Long id) {
        log.debug("REST request to get printer : {}", id);
        Optional<PrinterDTO> printerDTO = printerService.findOne(id);
        return ResponseUtil.wrapOrNotFound(printerDTO);
    }

    /**
     * DELETE  /printers/:id : delete the "id" printer.
     *
     * @param id the id to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @ApiOperation(value = "delete material base on id")
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/printers/{id}")
    public ResponseEntity<Void> deleteMaterials(@PathVariable Long id) {
        log.debug("REST request to delete deletePrinter : {}", id);
        printerService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

