package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.CityService;
import com.nostratech.nurindo.service.dto.CityDTO;
import com.nostratech.nurindo.service.dto.ResultPageDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class CityResource {

    public static final String ENTITY_NAME = "city";

    private final CityService cityService;

    public CityResource(CityService cityService) {
        this.cityService = cityService;
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "insert new City")
    @PostMapping("/city")
    public ResponseEntity<Boolean> createCustomers(@RequestBody CityDTO cityDTO) throws URISyntaxException {
        log.debug("REST request to save City : {}", cityDTO);
        if (cityDTO.getId() != null) {
            throw new BadRequestAlertException("A new task cannot already have an ID", ENTITY_NAME, "idexists");
        }

        Boolean result = cityService.createCity(cityDTO);
        return ResponseEntity.created(new URI("/api/city/" + cityDTO.getId()))
                .headers(HeaderUtil.createAlert( "City.created", cityDTO.getCityName()))
                .body(result);
    }


    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "update City")
    @PutMapping("/city")
    public ResponseEntity<Boolean> updateCustomers(@Valid @RequestBody CityDTO cityDTO) throws URISyntaxException {
        log.debug("REST request to update City : {}", cityDTO);
        if (cityDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid ID to Update", ENTITY_NAME, "idnotexists");
        }

        Boolean result = cityService.updateCity(cityDTO);
        return ResponseEntity.created(new URI("/api/city/" + cityDTO.getId()))
                .headers(HeaderUtil.createAlert( "city.updated", cityDTO.getCityName()))
                .body(result);
    }


    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all customers")
    @GetMapping("/list/city")
    public ResponseEntity<ResultPageDTO> getAllCity(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                         @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                         @RequestParam(value = "sortBy", required = false) String sortBy,
                                                         @RequestParam(value = "direction", required = false) String direction,
                                                         @RequestParam(value = "cityName", required = false) String cityName,
                                                         @RequestParam(value = "island", required = false) String island,
                                                         @RequestParam(value = "cityCode", required = false) String cityCode) {
        Map<String, Object> pageMap = cityService.getAllCityByName(page, limit, sortBy, direction, cityName, island, cityCode);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "delete city base on id")
    @DeleteMapping("/city/{id}")
    public ResponseEntity<Boolean> deleteCity(@PathVariable Long id) {
        log.debug("REST request to delete Roles : {}", id);
        Boolean result = cityService.deleteCity(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert( "City.deleted", id.toString()))
                .body(result);
    }
}
