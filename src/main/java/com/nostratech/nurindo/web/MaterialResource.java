package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.MaterialService;
import com.nostratech.nurindo.service.dto.MaterialDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class MaterialResource {

    public static final String ENTITY_NAME = "material";

    @Autowired
    private MaterialService materialService;

    /**
     * POST  /materials : Create a new material.
     *
     * @param materialDTO the materialDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new materialDTO, or with status 400 (Bad Request) if the task has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Creates a new material")
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/materials")
    public ResponseEntity<Boolean> createMaterial(@RequestBody MaterialDTO materialDTO) throws URISyntaxException{
        log.debug("REST request to save material : {}", materialDTO);
        if (materialDTO.getId() != null) {
            throw new BadRequestAlertException("A new material cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Boolean result = materialService.save(materialDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, materialDTO.getName()))
                .body(result);
    }

    /**
     * PUT  /materials : Updates an existing material.
     *
     * @param materialDTO the materialDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated materialDTO,
     * or with status 400 (Bad Request) if the materialDTO is not valid,
     * or with status 500 (Internal Server Error) if the materialDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing material")
    @PutMapping("/materials")
    public ResponseEntity<Boolean> updateMaterial(@RequestBody MaterialDTO materialDTO) throws URISyntaxException{
        log.debug("REST request to update material : {}", materialDTO);
        if (materialDTO.getId() == null){
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Boolean result = materialService.save(materialDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, materialDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET  /menus : get all the materials.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of materials in body
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all materials")
    @GetMapping("/materials")
    public ResponseEntity<List<MaterialDTO>> getAllMaterial() {
        log.debug("get all materials");
        final List<MaterialDTO> result = materialService.findAll();
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.toString()))
                .body(result);
    }

    /**
     * GET  /menus/:id : get the "id" material.
     *
     * @param id the id of the materialDto to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the menuDTO, or with status 404 (Not Found)
     */
    @ApiOperation(value = "get menu base on id")
    @PreAuthorize("isAuthenticated()")
    @GetMapping("/materials/{id}")
    public ResponseEntity<MaterialDTO> getMaterials(@PathVariable Long id) {
        log.debug("REST request to get materials : {}", id);
        Optional<MaterialDTO> materialDTO = materialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialDTO);
    }

    /**
     * DELETE  /menus/:id : delete the "id" menu.
     *
     * @param id the id of the menuDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @ApiOperation(value = "delete material base on id")
    @PreAuthorize("isAuthenticated()")
    @DeleteMapping("/materials/{id}")
    public ResponseEntity<Void> deleteMaterials(@PathVariable Long id) {
        log.debug("REST request to delete materials : {}", id);
        materialService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}

