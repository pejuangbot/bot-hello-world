package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.RoleService;
import com.nostratech.nurindo.service.dto.RoleDTO;
import com.nostratech.nurindo.service.dto.RoleListDTO;
import com.nostratech.nurindo.service.dto.StatusDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.PaginationUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Role.
 */
@Slf4j
@RestController
@RequestMapping("api/v1")
public class RoleResource {

    private static final String ENTITY_NAME = "role";

    private final RoleService roleService;

    public RoleResource(RoleService roleService) {this.roleService = roleService;}

    /**
     * POST  /roles : Create a new role.
     *
     * @param roleDTO the roleDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new roleDTO, or with status 400 (Bad Request) if the task has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Creates a new role")
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/roles")
    public ResponseEntity<Boolean> createRole(@RequestBody RoleDTO roleDTO) throws URISyntaxException{
        log.debug("REST request to save Role : {}", roleDTO);
        if (roleDTO.getId() != null) {
            throw new BadRequestAlertException("A new task cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Boolean result = roleService.save(roleDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roleDTO.getName()))
                .body(result);
    }

    /**
     * PUT  /roles : Updates an existing role.
     *
     * @param roleDTO the roleDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated roleDTO,
     * or with status 400 (Bad Request) if the roleDTO is not valid,
     * or with status 500 (Internal Server Error) if the roleDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing role")
    @PutMapping("/roles")
    public ResponseEntity<Boolean> updateRole(@RequestBody RoleDTO roleDTO) throws URISyntaxException{
        log.debug("REST request to update Role : {}", roleDTO);
        if (roleDTO.getId() == null){
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Boolean result = roleService.save(roleDTO);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, roleDTO.getId().toString()))
                .body(result);
    }

    /**
     * GET  /roles : get all the roles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of roles in body
     */
//    @GetMapping("/roles")
//    public List<RoleDTO> getAllRoles() {
//        log.debug("REST request to get all Roles");
//        return roleService.findAll();
//    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all roles")
    @GetMapping("/roles")
    public ResponseEntity<List<RoleListDTO>> getAllRoles(@RequestParam(value = "name", required = false) String name) {
        log.debug("Resources pageable");
        final List<RoleListDTO> result = roleService.findAll(name);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.toString()))
                .body(result);
    }

    /**
     * GET  /roles/:id : get the "id" role.
     *
     * @param id the id of the taskDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the roleDTO, or with status 404 (Not Found)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get role base on id")
    @GetMapping("/roles/{id}")
    public ResponseEntity<RoleDTO> getRole(@PathVariable Long id) {
        log.debug("REST request to get Role : {}", id);
        Optional<RoleDTO> roleDTO = roleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(roleDTO);
    }

    /**
     * DELETE  /roles/:id : delete the "id" role.
     *
     * @param id the id of the roleDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "delete role base on username")
    @DeleteMapping("/roles/{id}")
    public ResponseEntity<Void> deleteRole(@PathVariable Long id) {
        log.debug("REST request to delete Roles : {}", id);
        roleService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * PUT  /roles/change-status : Change status a existing role.
     *
     * @param id the id of role
     * @param statusDto the statusDto to change status role
     * @return boolean true if change status is success
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Change status a existing role")
    @PreAuthorize("isAuthenticated()")
    @PutMapping("/roles/change-status/{id}")
    public ResponseEntity<Boolean> changeStatus(@RequestBody StatusDTO statusDto,
                                                @PathVariable Long id) throws URISyntaxException{
        log.debug("REST request to save status Role : {}", statusDto);
        Boolean result = roleService.changeStatus(id, statusDto);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, statusDto.getActivated().toString()))
                .body(result);
    }
}
