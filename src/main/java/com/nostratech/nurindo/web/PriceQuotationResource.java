package com.nostratech.nurindo.web;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.exception.EmailAlreadyUsedException;
import com.nostratech.nurindo.exception.UsernameAlreadyUsedException;
import com.nostratech.nurindo.service.PriceQuotationService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class PriceQuotationResource {

    @Autowired
    private PriceQuotationService priceQuotationService;

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/price-quotations")
    @ApiOperation(value = "Creates a new price quotation")
    public ResponseEntity<PqNumberDTO> create(@Valid @RequestBody PriceQuotationDTO pricaQuotation) throws URISyntaxException {
        log.debug("REST request to save price quotation : {}", pricaQuotation);
        if (pricaQuotation.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
        } else {
            PqNumberDTO pqNumber = priceQuotationService.create(pricaQuotation);

            return ResponseEntity.created(new URI("/api/price-quotations/" + pqNumber.getValue().replace(" ", "")))
                    .headers(HeaderUtil.createAlert( "priceQuotationsManagement.created", pqNumber.getValue().replace(" ", "")))
                    .body(pqNumber);
        }
    }

    /**
     * PUT /price-quotations : Updates an existing User.
     *
     * @param priceQuotationDTO the user to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated user
     * @throws EmailAlreadyUsedException 400 (Bad Request) if the email is already in use
     * @throws UsernameAlreadyUsedException 400 (Bad Request) if the username is already in use
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing priceQuotation")
    @PutMapping("/price-quotations")
    public ResponseEntity<PqNumberDTO> update(@Valid @RequestBody PriceQuotationDTO priceQuotationDTO) throws URISyntaxException {
        log.debug("REST request to update price quotation : {}", priceQuotationDTO);
        PqNumberDTO pqNumber = priceQuotationService.update(priceQuotationDTO);

        return ResponseEntity.created(new URI("/api/price-quotations/" + pqNumber.getValue().replace(" ", "")))
                .headers(HeaderUtil.createAlert( "priceQuotationsManagement.created", pqNumber.getValue().replace(" ", "")))
                .body(pqNumber);
    }

    /**
     * GET /price-quotations : get all price quotations.
     *
     * @param page the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all price quotations.
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all price quotations")
    @GetMapping("/price-quotations")
    public ResponseEntity<ResultPageDTO> getAllUsers(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                     @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                     @RequestParam(value = "sortBy", required = false) String sortBy,
                                                     @RequestParam(value = "direction", required = false) String direction,
                                                     @RequestParam(value = "pqNumber", required = false, defaultValue = "") String pqNumber,
                                                     @RequestParam(value = "roleId", required = false) Long roleId,
                                                     @RequestParam(value = "customerId", required = false) Long customerId){
        Map<String, Object> pageMap = priceQuotationService.getAll(page, limit, sortBy, direction, pqNumber, roleId, customerId);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }

    /**
     * GET /price-quotations/:id : get the "id" price quotation.
     *
     * @param id the id of the price quotation to find
     * @return the ResponseEntity with status 200 (OK) and with body the "id" price quotation, or with status 404 (Not Found)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get price quotation base on id")
    @GetMapping("/price-quotations/{id}")
    public ResponseEntity<PriceQuotationDetailDTO> getPriceQuotation(@PathVariable Long id) {
        log.debug("REST request to get price quotation : {}", id);

        return ResponseUtil.wrapOrNotFound(
                priceQuotationService.getPriceQuotation(id));
    }

    /**
     * DELETE /price-quotations/:id : delete the "id" price quotations.
     *
     * @param id the id of the price quotation to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Delete user base on id")
    @DeleteMapping("/price-quotations/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to delete price quotations: {}", id);
        Boolean result = priceQuotationService.delete(id);

        return ResponseEntity.created(new URI("/api/price-quotations/" + id.toString()))
                .headers(HeaderUtil.createAlert( "priceQuotationsManagement.deleted", id.toString()))
                .body(result);
    }

    /**
     * PUT /price-quotations/change-status/{id} : Change status a existing price-quotations.
     *
     * @param id the id of price-quotations
     * @param statusDto the statusDto to change status price-quotations
     * @return boolean true if change status is success
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @ApiOperation(value = "Change status a existing price-quotations")
    @PreAuthorize("isAuthenticated()")
    @PutMapping("/price-quotations/change-status/{id}")
    public ResponseEntity<Boolean> changeStatus(@RequestBody StatusPQDTO statusDto,
                                                @PathVariable Long id) throws URISyntaxException{
        log.debug("REST request to save status price quotations : {}", statusDto);
        Boolean result = priceQuotationService.statusUpdate(id, statusDto);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("user", statusDto.getStatus()))
                .body(result);
    }

}
