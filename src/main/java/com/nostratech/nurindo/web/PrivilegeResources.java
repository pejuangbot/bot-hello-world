package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.PrivilegeService;
import com.nostratech.nurindo.service.dto.PrivilegeDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class PrivilegeResources {

    public static final String ENTITY_NAME = "privilege";

    private final PrivilegeService privilegeService;

    public PrivilegeResources(PrivilegeService privilegeServices) {this.privilegeService = privilegeServices;}

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "insert new privilege")
    @PostMapping("/privileges")
    public ResponseEntity<PrivilegeDTO> createMenu(@RequestBody PrivilegeDTO privilegeDTO) throws URISyntaxException {
        log.debug("REST request to save Privilege : {}", privilegeDTO);
        if (privilegeDTO.getId() != null) {
            throw new BadRequestAlertException("A new task cannot already have an ID", ENTITY_NAME, "idexists");
        }

        PrivilegeDTO result = privilegeService.save(privilegeDTO);
        return ResponseEntity.created(new URI("/api/privileges/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }
}
