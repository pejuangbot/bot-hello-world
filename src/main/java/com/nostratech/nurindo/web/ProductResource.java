package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.UserRepository;
import com.nostratech.nurindo.service.ProductService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.PaginationUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * REST controller for managing products.
 * <p>
 * This class accesses the Product entity, and needs to fetch its collection of authorities.
 */
@Slf4j
@RestController
@RequestMapping("/api/v1")
public class ProductResource {

    @Autowired
    private ProductService productService;

    /**
     * POST  /products  : Creates a new product.
     * <p>
     * Creates a new product
     *
     * @param productDTO the product to create
     * @return the ResponseEntity with status 201 (Created) and with body the new user, or with status 400 (Bad Request)
     *         if create product fail
     * @throws URISyntaxException if the Location URI syntax is incorrect
     * @throws BadRequestAlertException 400 (Bad Request) if the login or email is already in use
     */
    @PreAuthorize("isAuthenticated()")
    @PostMapping("/products")
    @ApiOperation(value = "Creates a new product")
    @ApiResponses( value = {
            @ApiResponse(code = 201, message = "Product created")
    })
    public ResponseEntity<Boolean> createUser(@Valid @RequestBody ProductDTO productDTO) throws URISyntaxException {
        log.debug("REST request to save product : {}", productDTO);
        if (productDTO.getId() != null) {
            throw new BadRequestAlertException("A new product cannot already have an ID", "products", "idexists");
        }

        Boolean newUser = productService.createProduct(productDTO);

        return ResponseEntity.created(new URI("/api/products/" + productDTO.getProduct().replace(" ", "_")))
                .headers(HeaderUtil.createAlert( "productManagement.created", productDTO.getProduct()))
                .body(newUser);
    }

    /**
     * PUT /products : Updates an existing Product.
     *
     * @param productDTO the product to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated product
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing product")
    @PutMapping("/products")
    public ResponseEntity<Boolean> updateUser(@Valid @RequestBody ProductDTO productDTO) throws URISyntaxException {
        log.debug("REST request to update product : {}", productDTO);
        if (productDTO.getId() == null){
            throw new BadRequestAlertException("Invalid id", "products", "idnull");
        }

        Boolean updateProduct = productService.updateProduct(productDTO);

        return ResponseEntity.created(new URI("/api/users/" + productDTO.getProduct().replace(" ", "_")))
                .headers(HeaderUtil.createAlert( "productManagement.created", productDTO.getProduct().replace(" ", "_")))
                .body(updateProduct);
    }

    /**
     * GET /products : get all products.
     *
     * @param page the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all products
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all products")
    @GetMapping("/products")
    public ResponseEntity<ResultPageDTO> getAllUsers(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                        @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                        @RequestParam(value = "sortBy", required = false) String sortBy,
                                                        @RequestParam(value = "direction", required = false) String direction,
                                                        @RequestParam(value = "productCode", required = false) String productCode,
                                                        @RequestParam(value = "productName", required = false) String productName) {
        Map<String, Object> pageMap = productService.getAllProduct(page, limit, sortBy, direction, productCode, productName);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }

    /**
     * GET /products/:id : get the "id" product.
     *
     * @param id the id of the product to find
     * @return the ResponseEntity with status 200 (OK) and with body the "id" product, or with status 404 (Not Found)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get user base on id")
    @GetMapping("/products/{id}")
    public ResponseEntity<ProductDetailDTO> getUser(@PathVariable Long id) {
        log.debug("REST request to get product : {}", id);
        return ResponseUtil.wrapOrNotFound(
                productService.getProduct(id));
    }

    /**
     * DELETE /products/:id : delete the "id" Product.
     *
     * @param id the id of the product to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Delete product base on id")
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        log.debug("REST request to delete product: {}", id);
        productService.deleteProduct(id);
        return ResponseEntity.ok().headers(HeaderUtil.createAlert( "productManagement.deleted", id.toString())).build();
    }

}
