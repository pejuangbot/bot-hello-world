package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.PqNumberService;
import com.nostratech.nurindo.service.dto.PqNumberDTO;
import com.nostratech.nurindo.service.dto.PqNumberRecordDTO;
import com.nostratech.nurindo.service.dto.ResultPageDTO;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("api/v1")
public class PqNumberResource {
    public static final String ENTITY_NAME = "pq_number_record";

    private final PqNumberService pqNumberService;

    public PqNumberResource(PqNumberService pqNumberService) {
        this.pqNumberService = pqNumberService;
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Generate Price Quotation Number")
    @GetMapping("/number/{customerId}/{actionType}")
    public ResponseEntity<PqNumberDTO> genertaePriceQuotationNumber(@PathVariable Long customerId,
                                                                    @PathVariable String actionType) {
        return ResponseUtil.wrapOrNotFound(
                pqNumberService.getPqNumber(customerId, actionType)
        );
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "update Price Quotation Number")
    @PutMapping("/number")
    public ResponseEntity<Boolean> updateNumber(@Valid @RequestBody PqNumberRecordDTO pqNumberRecordDTO) throws URISyntaxException {
        log.debug("REST request to update Number : {}", pqNumberRecordDTO);
        if (pqNumberRecordDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid ID to Update", ENTITY_NAME, "idnotexists");
        }

        Boolean result = pqNumberService.updateNumber(pqNumberRecordDTO);
        return ResponseEntity.created(new URI("/api/number/" + pqNumberRecordDTO.getId()))
                .headers(HeaderUtil.createAlert( "number.updated", pqNumberRecordDTO.getMonth()))
                .body(result);
    }


    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all Numbers")
    @GetMapping("/list/number")
    public ResponseEntity<ResultPageDTO> getAllNumber(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                    @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                    @RequestParam(value = "sortBy", required = false) String sortBy,
                                                    @RequestParam(value = "direction", required = false) String direction,
                                                    @RequestParam(value = "month", required = false) String month,
                                                    @RequestParam(value = "numberType", required = false) String numberType) {
        Map<String, Object> pageMap = pqNumberService.getAllNumber(page, limit, sortBy, direction, numberType, month);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "delete city base on id")
    @DeleteMapping("/number/{id}")
    public ResponseEntity<Boolean> deleteCity(@PathVariable Long id) {
        log.debug("REST request to delete Roles : {}", id);
        Boolean result = pqNumberService.deleteNumber(id);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createAlert( "Number.deleted", id.toString()))
                .body(result);
    }
}
