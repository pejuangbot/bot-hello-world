package com.nostratech.nurindo.web;

import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.exception.EmailAlreadyUsedException;
import com.nostratech.nurindo.exception.UsernameAlreadyUsedException;
import com.nostratech.nurindo.service.SalesOrderService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.util.HeaderUtil;
import com.nostratech.nurindo.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class SalesOrderResource {

    @Autowired
    private SalesOrderService salesOrderService;

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/sales-orders")
    @ApiOperation(value = "Creates a new sales order")
    public ResponseEntity<Boolean> create(@Valid @RequestBody SalesOrderDTO salesOrderDto) throws URISyntaxException {
        log.debug("REST request to save User : {}", salesOrderDto);
        if (salesOrderDto.getId() != null) {
            throw new BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists");
        } else {
            Boolean result = salesOrderService.create(salesOrderDto);

            return ResponseEntity.created(new URI("/api/users/" + salesOrderDto.getSoNumber()))
                    .headers(HeaderUtil.createAlert( "userManagement.created", salesOrderDto.getSoNumber()))
                    .body(result);
        }
    }

    /**
     * PUT /users : Updates an existing sales order.
     *
     * @param salesOrderDTO the sales order to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sales order
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Updates an existing sales order")
    @PutMapping("/sales-orders")
    public ResponseEntity<Boolean> update(@Valid @RequestBody SalesOrderDTO salesOrderDTO) throws URISyntaxException {
        log.debug("REST request to update price quotation : {}", salesOrderDTO);

        if (salesOrderDTO.getId() == null){
            throw new BadRequestAlertException("Invalid id", "sales_order", "idnull");
        }

        Boolean result = salesOrderService.update(salesOrderDTO);

        return ResponseEntity.created(new URI("/api/users/" + salesOrderDTO.getSoNumber()))
                .headers(HeaderUtil.createAlert( "userManagement.created", salesOrderDTO.getSoNumber()))
                .body(result);
    }

    /**
     * GET /price-quotation : get all price quotations.
     *
     * @param page the pagination information
     * @return the ResponseEntity with status 200 (OK) and with body all price quotations.
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get all sales orders")
    @GetMapping("/sales-orders")
    public ResponseEntity<ResultPageDTO> getAllUsers(@RequestParam(value = "pages", required = true, defaultValue = "0") Integer page,
                                                     @RequestParam(value = "limit", required = true, defaultValue = "10") Integer limit,
                                                     @RequestParam(value = "sortBy", required = false) String sortBy,
                                                     @RequestParam(value = "direction", required = false) String direction,
                                                     @RequestParam(value = "pqNumber", required = false, defaultValue = "") String pqNumber ){
        Map<String, Object> pageMap = salesOrderService.getAll(page, limit, sortBy, direction, pqNumber);
        return new ResponseEntity<>(AbstractRequestHandler.constructListResult(pageMap), HttpStatus.OK);
    }

    /**
     * GET /sales-orders/:id : get the "id" sales-orders.
     *
     * @param id the id of the sales-orders to find
     * @return the ResponseEntity with status 200 (OK) and with body the "id" price quotation, or with status 404 (Not Found)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Get price sales order on id")
    @GetMapping("/sales-orders/{id}")
    public ResponseEntity<SalesOrderDetailDTO> getSalesOrder(@PathVariable Long id) {
        log.debug("REST request to get sales order : {}", id);

        return ResponseUtil.wrapOrNotFound(
                salesOrderService.getSalesOrder(id));
    }

    /**
     * DELETE /sales-orders/:id : delete the "id" sales orders.
     *
     * @param id the id of the sales orders to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @PreAuthorize("isAuthenticated()")
    @ApiOperation(value = "Delete sales order base on id")
    @DeleteMapping("/sales-orders/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable Long id) throws URISyntaxException {
        log.debug("REST request to delete sales orders: {}", id);
        Boolean result = salesOrderService.delete(id);

        return ResponseEntity.created(new URI("/api/sales-orders/" + id.toString()))
                .headers(HeaderUtil.createAlert( "salesOrdersManagement.deleted", id.toString()))
                .body(result);
    }

}
