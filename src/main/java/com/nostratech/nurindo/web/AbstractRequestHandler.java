package com.nostratech.nurindo.web;

import com.nostratech.nurindo.service.dto.ResultPageDTO;
import com.nostratech.nurindo.util.Constants;

import java.util.Collection;
import java.util.Map;

public class AbstractRequestHandler {

    public static ResultPageDTO constructListResult(Map<String, Object> pageMap) {
        ResultPageDTO result = new ResultPageDTO();
        try {
            Collection list = constructPageResult(pageMap, result);
            result.setResult(list);
        } catch (Exception e) {

        }
        return result;
    }

    public static Collection constructPageResult(Map<String, Object> map, ResultPageDTO result) {
        if (map == null) {
            result.setPages("0");
            result.setElements("0");
            return null;
        } else {
            Collection vos = (Collection) map.get(Constants.PageParameter.LIST_DATA);
            result.setPages(String.valueOf(map.get(Constants.PageParameter.TOTAL_PAGES)));
            result.setElements(String.valueOf(map.get(Constants.PageParameter.TOTAL_ELEMENTS)));
            return vos;
        }
    }

}
