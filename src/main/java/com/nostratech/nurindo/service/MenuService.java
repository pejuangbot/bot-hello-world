package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.MenuDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface MenuService {
    /**
     * Save a menu
     *
     * @param menuDTO the entity to save
     * @return the persisted id
     */
    MenuDTO save(MenuDTO menuDTO);

    /**
     * Get all of menus
     *
     * @return list of entities
     */
    List<MenuDTO> findAll();

    /**
     * Get specific menu
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<MenuDTO> findOne(Long id);

    /**
     * Delete specific menu
     *
     * @param id the id of entity
     */
    void delete(Long id);

    /**
     * Get all of menus by page
     *
     * @param pageable
     * @return list of the entities
     */
    Page<MenuDTO> findAll(Pageable pageable);
}
