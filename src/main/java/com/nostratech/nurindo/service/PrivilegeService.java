package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.PrivilegeDTO;

public interface PrivilegeService extends EntityService<PrivilegeDTO> {
    
}
