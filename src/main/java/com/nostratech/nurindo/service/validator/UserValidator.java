package com.nostratech.nurindo.service.validator;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.RoleRepository;
import com.nostratech.nurindo.repository.UserRepository;
import com.nostratech.nurindo.service.dto.ChangePasswordDTO;
import com.nostratech.nurindo.service.dto.RoleBaseDTO;
import com.nostratech.nurindo.util.ErrorConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserValidator {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    public List<Role> checkRoles(List<RoleBaseDTO> roleBaseDTOS){
        List<Role> roleList = roleBaseDTOS == null ? null : roleBaseDTOS.stream()
                .map(role -> {
                    Role roleFound = roleRepository.findById(role.getId())
                            .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Role not found!", "userManagement", "userexists"));
                    return roleFound;
                }).collect(Collectors.toCollection(LinkedList::new));
        return roleList;
    }

    public User changePasswordValidation(String username, ChangePasswordDTO changePasswordDTO){

        User userFound = userRepository.findOneByUsername(username)
                .orElseThrow(()-> new BadCredentialsException("Bad credentials"));

        if(!userFound.getUsername().equalsIgnoreCase(username))
            throw new BadCredentialsException("Bad credentials");

        if(!changePasswordDTO.getPassword().equalsIgnoreCase(changePasswordDTO.getConfirmPassword()))
            throw new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Password doesn't match!", "userManagement", "userexists");

        return userFound;
    }



}
