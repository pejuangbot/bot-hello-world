package com.nostratech.nurindo.service.validator;

import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.ProductRepository;
import com.nostratech.nurindo.repository.RoleRepository;
import com.nostratech.nurindo.service.dto.ProductDTO;
import com.nostratech.nurindo.service.dto.ProductRoleDTO;
import com.nostratech.nurindo.util.Constants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductValidator {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    ProductRepository productRepository;

    public void checkRoles(List<ProductRoleDTO> productRoleDTOS){
        productRoleDTOS.stream()
                .map(role -> {
                    Role roleFound = roleRepository.findById(role.getId())
                            .orElseThrow(()-> new BadRequestAlertException("Role not found!", "products", "idnull"));
                    return roleFound;
                }).collect(Collectors.toCollection(LinkedList::new));
    }

    public void createValidation(ProductDTO dto){
        fieldValidation(dto);

        Product product = productRepository.findByProductCode(dto.getProductCode());
        if(null != product) throw new BadRequestAlertException("Product code already used!", "productManagement", "idnull");
    }

    public void updateValidation(ProductDTO dto, Product entity){
        fieldValidation(dto);

        if(!dto.getProductCode().equalsIgnoreCase(entity.getProductCode()))
            throw new BadRequestAlertException("The product code is not the same!", "productManagement", "idnull");
    }

    public void fieldValidation(ProductDTO dto){
        if(!(dto.getType().equalsIgnoreCase(Constants.ProductType.PRODUCT) || dto.getType().equalsIgnoreCase(Constants.ProductType.SERVICE)))
            throw new BadRequestAlertException("Type is not valid!", "productManagement", "idnull");

        if (dto.getType().equalsIgnoreCase(Constants.ProductType.PRODUCT)){
            if (null == dto.getListOfMaterial())
                throw new BadRequestAlertException("List of material must not be null", "productManagement", "idnull");

            if (null == dto.getListOfPrinter())
                throw new BadRequestAlertException("List of printer must not be null", "productManagement", "idnull");

            if (null == dto.getHeight())
                throw new BadRequestAlertException("Height must not be null", "productManagement", "idnull");

            if (null == dto.getWidth())
                throw new BadRequestAlertException("Width must not be null", "productManagement", "idnull");
        }
    }


}
