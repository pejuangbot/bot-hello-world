package com.nostratech.nurindo.service.validator;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.SalesOrder;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.SalesOrderRepository;
import com.nostratech.nurindo.service.dto.SalesOrderDTO;
import com.nostratech.nurindo.service.dto.SalesOrderDetailTrxDTO;
import com.nostratech.nurindo.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class SalesOrderValidator {

    @Autowired
    SalesOrderRepository salesOrderRepository;

    public void salesOrderValidation(SalesOrderDTO salesOrderDTO, PriceQuotation priceQuotation){
        if(priceQuotation.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.DRAFT)){
            throw new BadRequestAlertException("Unable to make a sales order, price quotation status is a draft", "salesOrderManagement", "");
        } else if (priceQuotation.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.DELETED)){
            throw new BadRequestAlertException("Unable to make a sales order, price quotation status is a deleted", "salesOrderManagement", "");
        } else if (priceQuotation.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEED_APPROVAL)){
            throw new BadRequestAlertException("Unable to make a sales order, price quotation status is a need approval", "salesOrderManagement", "");
        } else if (priceQuotation.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.SO_CREATED)){
            throw new BadRequestAlertException("Sales order was created", "salesOrderManagement", "");
        } else if (priceQuotation.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEW)){

        } else {
            throw new BadRequestAlertException("Status price quotation is not valid", "salesOrderManagement", "");
        }

        SalesOrder salesOrder = salesOrderRepository.findByPriceQuotation(priceQuotation);
        if(null != salesOrder) throw new BadRequestAlertException("Price quotation is already used", "salesOrderManagement", "");

        statusValidation(salesOrderDTO);
    }

    public void statusValidation(SalesOrderDTO salesOrderDTO){
        if(!(salesOrderDTO.getStatus().equalsIgnoreCase(Constants.SalesOrderStatus.NEW)
                || salesOrderDTO.getStatus().equalsIgnoreCase(Constants.SalesOrderStatus.DELETED)
                || salesOrderDTO.getStatus().equalsIgnoreCase(Constants.SalesOrderStatus.JO_CREATED))){
            throw new BadRequestAlertException("Status sales order is not valid", "salesOrderManagement", "");
        }
    }

}
