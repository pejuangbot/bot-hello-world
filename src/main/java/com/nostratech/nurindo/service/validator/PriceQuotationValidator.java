package com.nostratech.nurindo.service.validator;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.service.dto.PriceQuotationDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailTrxDTO;
import com.nostratech.nurindo.util.Constants;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class PriceQuotationValidator {

    public void calculationValidation(PriceQuotationDTO dtos){
        BigDecimal subTotal = new BigDecimal(0);
        BigDecimal finalTotal = new BigDecimal((0 ));
        for(PriceQuotationDetailTrxDTO dto : dtos.getDetailTrxDTOS()){
            BigDecimal totalPrice = dto.getPrice().multiply(new BigDecimal(dto.getQuantity()));
            if(totalPrice.compareTo(dto.getTotalPrice()) != 0)
                throw new BadRequestAlertException("Total price is not valid", "price-quotation","");
            subTotal = subTotal.add(totalPrice);
        }

        if (subTotal.compareTo(dtos.getSubTotal()) != 0)
            throw new BadRequestAlertException("Sub total is not valid", "price-quotation","");

        BigDecimal discountPrice = new BigDecimal(0);
        if(null != dtos.getDiscount()){
            discountPrice = new BigDecimal(dtos.getDiscount()).divide(new BigDecimal(100)).multiply(subTotal);
            if (discountPrice.compareTo(dtos.getDiscountPrice()) != 0)
                throw new BadRequestAlertException("Discount is not valid", "price-quotation","");
        }

        finalTotal = subTotal.subtract(discountPrice);

        BigDecimal taxPrice = new BigDecimal(0);
        if (dtos.getTax().equalsIgnoreCase(Constants.Tax.PPN_10)) {
            taxPrice = new BigDecimal(10).divide(new BigDecimal(100)).multiply(finalTotal);
        } else {
            throw new BadRequestAlertException("Tax is not valid", "price-quotation","");
        }

        if (taxPrice.compareTo(dtos.getTaxPrice()) != 0)
            throw new BadRequestAlertException("Tax price is not valid", "price-quotation","");

        BigDecimal finalPrice = finalTotal.add(taxPrice);
        if (finalPrice.compareTo(dtos.getFinalPrice()) != 0)
            throw new BadRequestAlertException("Final price is not valid", "price-quotation","");
    }

    public void createValidation(PriceQuotationDTO dto){

        if(!(dto.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEW)
                || dto.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEED_APPROVAL)
                || dto.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.DELETED)
                || dto.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.SO_CREATED)
                || dto.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.DRAFT))){
            throw new BadRequestAlertException("Status is not valid", "priceQuotationManagement","");
        }

        if(!(dto.getType().equalsIgnoreCase(Constants.ProductType.PRODUCT) || dto.getType().equalsIgnoreCase(Constants.ProductType.SERVICE)))
            throw new BadRequestAlertException("Type is not valid!", "productManagement", "idexists");

    }

}
