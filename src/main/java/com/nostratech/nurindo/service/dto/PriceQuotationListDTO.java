package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;

public class PriceQuotationListDTO {

    private Long id;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pq_number")
    private String pqNumber;

    @NotBlank
    @JsonProperty(value = "account_executive")
    private String accountExecutive;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "project_name")
    private String projectName;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "customer_name")
    private String customerName;

    @JsonProperty(value = "created_date")
    private Long createdDate;

    @JsonProperty(value = "price_quotation_status")
    private String status;

    @JsonProperty(value = "need_approval_from")
    private String needApprovalFrom;

    public PriceQuotationListDTO() {
    }

    public PriceQuotationListDTO(Long id, @NotBlank @Size(max = 20) String pqNumber, @NotBlank String accountExecutive, @NotBlank @Size(max = 100) String projectName, @NotBlank @Size(max = 100) String customerName, Long createdDate, String status, String needApprovalFrom) {
        this.id = id;
        this.pqNumber = pqNumber;
        this.accountExecutive = accountExecutive;
        this.projectName = projectName;
        this.customerName = customerName;
        this.createdDate = createdDate;
        this.status = status;
        this.needApprovalFrom = needApprovalFrom;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPqNumber() {
        return pqNumber;
    }

    public void setPqNumber(String pqNumber) {
        this.pqNumber = pqNumber;
    }

    public String getAccountExecutive() {
        return accountExecutive;
    }

    public void setAccountExecutive(String accountExecutive) {
        this.accountExecutive = accountExecutive;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNeedApprovalFrom() {
        return needApprovalFrom;
    }

    public void setNeedApprovalFrom(String needApprovalFrom) {
        this.needApprovalFrom = needApprovalFrom;
    }

    @Override
    public String toString() {
        return "PriceQuotationListDTO{" +
                "id=" + id +
                ", pqNumber='" + pqNumber + '\'' +
                ", accountExecutive='" + accountExecutive + '\'' +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", createdDate=" + createdDate +
                ", status=" + status +
                ", needApprovalFrom=" + needApprovalFrom +
                '}';
    }
}
