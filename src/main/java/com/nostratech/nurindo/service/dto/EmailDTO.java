package com.nostratech.nurindo.service.dto;

import java.io.Serializable;

public class EmailDTO implements Serializable{
    private String subject;
    private String recepients;
    private String mailText;

    public EmailDTO() {
    }

    public EmailDTO( String cc, String subject, String recepients, String mailText) {
        this.subject = subject;
        this.recepients = recepients;
        this.mailText = mailText;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRecepients() {
        return recepients;
    }

    public void setRecepients(String recepients) {
        this.recepients = recepients;
    }

    public String getMailText() {
        return mailText;
    }

    public void setMailText(String mailText) {
        this.mailText = mailText;
    }

    @Override
    public String toString() {
        return "EmailDTO{" +
                ", subject='" + subject + '\'' +
                ", recepients='" + recepients + '\'' +
                ", mailText='" + mailText + '\'' +
                '}';
    }
}
