package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.List;

public class CustomerProductRoleDTO {

    @JsonProperty(value = "customer_id")
    private Long customerId;

    @JsonProperty(value = "product_id")
    private Long productId;

    @JsonProperty(value = "product_name")
    private String productName;

    @JsonProperty(value = "product_height")
    private Integer productHeight;

    @JsonProperty(value = "product_width")
    private Integer productWidth;

    @JsonProperty(value = "product_type")
    private String productType;

    @JsonProperty(value = "customer_product_id")
    private Long customerProductId;

    @JsonProperty(value = "msrp")
    private BigDecimal msrp;

    @JsonProperty(value = "bottom_price")
    private BigDecimal bottomPrice;

    @JsonProperty(value = "agreed_price")
    private BigDecimal agreedPrice;

    @JsonProperty(value = "unit_measure")
    private String unitMeasure;

    @JsonProperty(value = "list_of_material")
    private List<MaterialDTO> listOfMaterial;

    @JsonProperty(value = "list_of_printer")
    private List<PrinterDTO> listOfPrinter;

    public CustomerProductRoleDTO() {
    }

    public CustomerProductRoleDTO(Long customerId, Long productId, String productName, Integer productHeight, Integer productWidth, String productType, Long customerProductId, BigDecimal msrp, BigDecimal bottomPrice, BigDecimal agreedPrice, String unitMeasure, List<MaterialDTO> listOfMaterial, List<PrinterDTO> listOfPrinter) {
        this.customerId = customerId;
        this.productId = productId;
        this.productName = productName;
        this.productHeight = productHeight;
        this.productWidth = productWidth;
        this.productType = productType;
        this.customerProductId = customerProductId;
        this.msrp = msrp;
        this.bottomPrice = bottomPrice;
        this.agreedPrice = agreedPrice;
        this.unitMeasure = unitMeasure;
        this.listOfMaterial = listOfMaterial;
        this.listOfPrinter = listOfPrinter;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getProductHeight() {
        return productHeight;
    }

    public void setProductHeight(Integer productHeight) {
        this.productHeight = productHeight;
    }

    public Integer getProductWidth() {
        return productWidth;
    }

    public void setProductWidth(Integer productWidth) {
        this.productWidth = productWidth;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Long getCustomerProductId() {
        return customerProductId;
    }

    public void setCustomerProductId(Long customerProductId) {
        this.customerProductId = customerProductId;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public BigDecimal getBottomPrice() {
        return bottomPrice;
    }

    public void setBottomPrice(BigDecimal bottomPrice) {
        this.bottomPrice = bottomPrice;
    }

    public BigDecimal getAgreedPrice() {
        return agreedPrice;
    }

    public void setAgreedPrice(BigDecimal agreedPrice) {
        this.agreedPrice = agreedPrice;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public List<MaterialDTO> getListOfMaterial() {
        return listOfMaterial;
    }

    public void setListOfMaterial(List<MaterialDTO> listOfMaterial) {
        this.listOfMaterial = listOfMaterial;
    }

    public List<PrinterDTO> getListOfPrinter() {
        return listOfPrinter;
    }

    public void setListOfPrinter(List<PrinterDTO> listOfPrinter) {
        this.listOfPrinter = listOfPrinter;
    }

    @Override
    public String toString() {
        return "CustomerProductRoleDTO{" +
                "customerId=" + customerId +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", productHeight=" + productHeight +
                ", productWidth=" + productWidth +
                ", productType='" + productType + '\'' +
                ", customerProductId=" + customerProductId +
                ", msrp=" + msrp +
                ", bottomPrice=" + bottomPrice +
                ", agreedPrice=" + agreedPrice +
                ", unitMeasure='" + unitMeasure + '\'' +
                ", listOfMaterial=" + listOfMaterial +
                ", listOfPrinter=" + listOfPrinter +
                '}';
    }
}
