package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

public class ProductDTO extends ProductBaseDTO {

    @Valid
    @JsonProperty(value = "list_of_printer")
    private List<PrinterDTO> listOfPrinter;

    @Valid
    @JsonProperty(value = "list_of_material")
    private List<MaterialDTO> listOfMaterial;

    public ProductDTO() {
    }

    public List<PrinterDTO> getListOfPrinter() {
        return listOfPrinter;
    }

    public void setListOfPrinter(List<PrinterDTO> listOfPrinter) {
        this.listOfPrinter = listOfPrinter;
    }

    public List<MaterialDTO> getListOfMaterial() {
        return listOfMaterial;
    }

    public void setListOfMaterial(List<MaterialDTO> listOfMaterial) {
        this.listOfMaterial = listOfMaterial;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + getId() +
                ", productCode='" + getProductCode() + '\'' +
                ", product='" + getProduct() + '\'' +
                ", size='" + getHeight() + '\'' +
                ", stock=" + getStock() +
                ", listOfPrinter='" + listOfPrinter + '\'' +
                ", listOfMaterial='" + listOfMaterial + '\'' +
                '}';
    }
}
