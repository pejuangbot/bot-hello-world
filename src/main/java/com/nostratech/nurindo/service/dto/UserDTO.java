package com.nostratech.nurindo.service.dto;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.util.Constants;

import javax.validation.constraints.*;
import java.util.List;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.User.USERNAME_REGEX)
    @Size(min = 1, max = 50)
    private String username;

    public UserDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                '}';
    }
}
