package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;

public class SalesOrderDTO {

    private Long id;

    @NotNull
    @JsonProperty(value = "price_quotation_id")
    private Long priceQuotation;

    @NotBlank
    @Size(max = 50)
    @JsonProperty(value = "so_number")
    private String soNumber;

    @Size(max = 50)
    @JsonProperty(value = "pr_origin")
    private String prOrigin;

    @Size(max = 256)
    @JsonProperty(value = "sales_order_notes")
    private String notes;

    @NotBlank
    @Size(max = 50)
    @JsonProperty(value = "shipment_type")
    private String shipmentType;

    @NotNull
    @JsonProperty(value = "shipment_date")
    private Long shipmentDate;

    @NotBlank
    @Size(max = 10)
    @JsonProperty(value = "sales_order_status")
    private String status;

    public SalesOrderDTO() {
    }

    public SalesOrderDTO(Long id, @NotNull Long priceQuotation, @NotBlank @Size(max = 20) String soNumber, @NotBlank @Size(max = 20) String prOrigin, @NotBlank @Size(max = 8) String notes, @NotBlank @Size(max = 3) String currency, @NotBlank @Size(max = 50) String shipmentType, @NotNull Long shipmentDate, @NotBlank @Size(max = 10) String status) {
        this.id = id;
        this.priceQuotation = priceQuotation;
        this.soNumber = soNumber;
        this.prOrigin = prOrigin;
        this.notes = notes;
        this.shipmentType = shipmentType;
        this.shipmentDate = shipmentDate;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPriceQuotation() {
        return priceQuotation;
    }

    public void setPriceQuotation(Long priceQuotation) {
        this.priceQuotation = priceQuotation;
    }

    public String getSoNumber() {
        return soNumber;
    }

    public void setSoNumber(String soNumber) {
        this.soNumber = soNumber;
    }

    public String getPrOrigin() {
        return prOrigin;
    }

    public void setPrOrigin(String prOrigin) {
        this.prOrigin = prOrigin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public Long getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(Long shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SalesOrderDTO{" +
                "id=" + id +
                ", priceQuotation=" + priceQuotation +
                ", soNumber='" + soNumber + '\'' +
                ", prOrigin='" + prOrigin + '\'' +
                ", notes='" + notes + '\'' +
                ", shipmentType='" + shipmentType + '\'' +
                ", shipmentDate=" + shipmentDate +
                ", status='" + status + '\'' +
                '}';
    }
}
