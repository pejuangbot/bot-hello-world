package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.domain.User;

import javax.persistence.Column;
import javax.validation.constraints.*;
import java.util.List;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserCreateDTO {

    private Long id;

    @NotBlank
    @Pattern(regexp = Constants.User.USERNAME_REGEX)
    @Size(min = 1, max = 50)
    private String username;

    @Email
    @Size(min = 5, max = 254)
    @NotBlank
    private String email;

    @NotNull
    private List<RoleBaseDTO> roles;

    public UserCreateDTO() {
    }

    public UserCreateDTO(User user) {
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<RoleBaseDTO> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleBaseDTO> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return "UserCreateDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                '}';
    }
}
