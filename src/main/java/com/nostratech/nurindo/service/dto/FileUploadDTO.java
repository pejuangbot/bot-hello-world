package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;
import java.io.Serializable;

public class FileUploadDTO implements Serializable {

    @JsonProperty(value = "file_name")
    private String fileName;

    @JsonProperty(value = "file_url")
    private String fileUrl;

    @JsonProperty(value = "file_description")
    private String fileDescription;

    public FileUploadDTO() {
    }

    public FileUploadDTO(String fileName, String fileUrl, String fileDescription) {
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.fileDescription = fileDescription;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    @Override
    public String toString() {
        return "FileUploadDTO{" +
                "fileName='" + fileName + '\'' +
                ", fileUrl=" + fileUrl +
                ", fileDescription='" + fileDescription + '\'' +
                '}';
    }
}
