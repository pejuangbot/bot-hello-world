package com.nostratech.nurindo.service.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

public class StatusDTO {

    @NotNull
    @Column(nullable = false)
    private Boolean activated;

    public StatusDTO() {
    }

    public StatusDTO(StatusDTO dto) {
        this.activated = dto.getActivated();
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    @Override
    public String toString() {
        return "StatusDTO{" +
                "activated=" + activated +
                '}';
    }
}
