package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.List;

public class ProductDetailDTO extends ProductBaseDTO {


    @NotNull
    @JsonProperty(value = "list_of_printer")
    private List<PrinterDTO> listOfPrinter;

    @NotNull
    @JsonProperty(value = "list_of_material")
    private List<MaterialDTO> listOfMaterial;

    @JsonProperty(value = "created_date")
    private Long createdDate;

    @JsonProperty(value = "last_modified_date")
    private Long lastModifiedDate;

    public ProductDetailDTO() {
    }

    public ProductDetailDTO(Long id, @NotBlank @Size(min = 3, max = 50) String productCode, @NotBlank @Size(min = 3, max = 100) String product, Integer height, Integer width, @NotNull @Min(value = 0) Integer stock, String unitMeasure, String type, @NotNull @Min(value = 0) BigDecimal msrp, @NotNull @Min(value = 0) BigDecimal bottomPriceAE, @NotNull @Min(value = 0) BigDecimal bottomPriceSM, @NotNull @Min(value = 0) BigDecimal bottomPriceSA, @NotNull List<PrinterDTO> listOfPrinter, @NotNull List<MaterialDTO> listOfMaterial, Long createdDate, Long lastModifiedDate) {
        super(id, productCode, product, height, width, stock, unitMeasure, type, msrp, bottomPriceAE, bottomPriceSM, bottomPriceSA);
        this.listOfPrinter = listOfPrinter;
        this.listOfMaterial = listOfMaterial;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
    }

    public List<PrinterDTO> getListOfPrinter() {
        return listOfPrinter;
    }

    public void setListOfPrinter(List<PrinterDTO> listOfPrinter) {
        this.listOfPrinter = listOfPrinter;
    }

    public List<MaterialDTO> getListOfMaterial() {
        return listOfMaterial;
    }

    public void setListOfMaterial(List<MaterialDTO> listOfMaterial) {
        this.listOfMaterial = listOfMaterial;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String toString() {
        return "ProductDetailDTO{" +
                "listOfPrinter=" + listOfPrinter +
                ", listOfMaterial=" + listOfMaterial +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
