package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

public class SalesOrderDetailDTO {

    private Long id;

    @NotNull
    @JsonProperty(value = "price_quotation_id")
    private Long priceQuotation;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "so_number")
    private String soNumber;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pq_number")
    private String pqNumber;

    @JsonProperty(value = "created_date")
    private Long createdDate;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pr_number")
    private String prNumber;

    @NotNull
    @JsonProperty(value = "sales_person")
    private UserDTO salesPerson;

    @NotNull
    @JsonProperty(value = "sales_admin")
    private UserDTO salesAdmin;

    @NotNull
    @JsonProperty(value = "customer")
    private CustomerListDTO customer;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "project_name")
    private String projectName;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "customer_name")
    private String customerName;

    @NotBlank
    @Size(max = 32)
    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @NotBlank
    @Size(max = 32)
    @JsonProperty(value = "fax_number")
    private String faxNumber;

    @NotBlank
    @Size(max = 254)
    private String email;

    @NotBlank
    @Size(max = 254)
    private String notes;

    @NotBlank
    @Size(max = 254)
    @JsonProperty(value = "sales_order_notes")
    private String salesOrderNotes;

    @NotBlank
    @Size(max = 10)
    private String type;

    @JsonProperty(value = "status")
    private String status;

    @NotBlank
    private String tax;

    @NotNull
    @Min(0)
    @Max(100)
    private Double discount;

    @NotNull
    @JsonProperty(value = "sub_total")
    private BigDecimal subTotal;

    @NotNull
    @JsonProperty(value = "discount_price")
    private BigDecimal discountPrice;

    @NotNull
    @JsonProperty(value = "tax_price")
    private BigDecimal taxPrice;

    @NotNull
    @JsonProperty(value = "final_price")
    private BigDecimal finalPrice;

    @NotNull
    @JsonProperty(value = "detail_sales_order")
    private List<PriceQuotationDetailTrxListDTO> detailSalesOrder;

    @NotNull
    @JsonProperty(value = "list_of_attach")
    private List<FileUploadDTO> listOfAttach;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pr_origin")
    private String prOrigin;

    @NotBlank
    @Size(max = 3)
    @JsonProperty(value = "currency")
    private String currency;

    @NotBlank
    @Size(max = 50)
    @JsonProperty(value = "shipment_type")
    private String shipmentType;

    @NotNull
    @JsonProperty(value = "shipment_date")
    private Long shipmentDate;

    @Size(max = 50)
    @JsonProperty(value = "po")
    private String po;

    public SalesOrderDetailDTO() {
    }

    public SalesOrderDetailDTO(Long id, @NotNull Long priceQuotation, @NotBlank @Size(max = 20) String soNumber, @NotBlank @Size(max = 20) String pqNumber, Long createdDate, @NotBlank @Size(max = 20) String prNumber, @NotNull UserDTO salesPerson, @NotNull UserDTO salesAdmin, @NotNull CustomerListDTO customer, @NotBlank @Size(max = 100) String projectName, @NotBlank @Size(max = 100) String customerName, @NotBlank @Size(max = 32) String phoneNumber, @NotBlank @Size(max = 32) String faxNumber, @NotBlank @Size(max = 254) String email, @NotBlank @Size(max = 254) String notes, @NotBlank @Size(max = 254) String salesOrderNotes, @NotBlank @Size(max = 10) String type, String status, @NotBlank String tax, @NotNull @Min(0) @Max(100) Double discount, @NotNull BigDecimal subTotal, @NotNull BigDecimal discountPrice, @NotNull BigDecimal taxPrice, @NotNull BigDecimal finalPrice, @NotNull List<PriceQuotationDetailTrxListDTO> detailSalesOrder, @NotNull List<FileUploadDTO> listOfAttach, @NotBlank @Size(max = 20) String prOrigin, @NotBlank @Size(max = 3) String currency, @NotBlank @Size(max = 50) String shipmentType, @NotNull Long shipmentDate, @Size(max = 50) String po) {
        this.id = id;
        this.priceQuotation = priceQuotation;
        this.soNumber = soNumber;
        this.pqNumber = pqNumber;
        this.createdDate = createdDate;
        this.prNumber = prNumber;
        this.salesPerson = salesPerson;
        this.salesAdmin = salesAdmin;
        this.customer = customer;
        this.projectName = projectName;
        this.customerName = customerName;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.email = email;
        this.notes = notes;
        this.salesOrderNotes = salesOrderNotes;
        this.type = type;
        this.status = status;
        this.tax = tax;
        this.discount = discount;
        this.subTotal = subTotal;
        this.discountPrice = discountPrice;
        this.taxPrice = taxPrice;
        this.finalPrice = finalPrice;
        this.detailSalesOrder = detailSalesOrder;
        this.listOfAttach = listOfAttach;
        this.prOrigin = prOrigin;
        this.currency = currency;
        this.shipmentType = shipmentType;
        this.shipmentDate = shipmentDate;
        this.po = po;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSoNumber() {
        return soNumber;
    }

    public void setSoNumber(String soNumber) {
        this.soNumber = soNumber;
    }

    public String getPqNumber() {
        return pqNumber;
    }

    public void setPqNumber(String pqNumber) {
        this.pqNumber = pqNumber;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public UserDTO getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(UserDTO salesPerson) {
        this.salesPerson = salesPerson;
    }

    public UserDTO getSalesAdmin() {
        return salesAdmin;
    }

    public void setSalesAdmin(UserDTO salesAdmin) {
        this.salesAdmin = salesAdmin;
    }

    public CustomerListDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerListDTO customer) {
        this.customer = customer;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(BigDecimal taxPrice) {
        this.taxPrice = taxPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public List<PriceQuotationDetailTrxListDTO> getDetailSalesOrder() {
        return detailSalesOrder;
    }

    public void setDetailSalesOrder(List<PriceQuotationDetailTrxListDTO> detailSalesOrder) {
        this.detailSalesOrder = detailSalesOrder;
    }

    public List<FileUploadDTO> getListOfAttach() {
        return listOfAttach;
    }

    public void setListOfAttach(List<FileUploadDTO> listOfAttach) {
        this.listOfAttach = listOfAttach;
    }

    public String getPrOrigin() {
        return prOrigin;
    }

    public void setPrOrigin(String prOrigin) {
        this.prOrigin = prOrigin;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public Long getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(Long shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public Long getPriceQuotation() {
        return priceQuotation;
    }

    public void setPriceQuotation(Long priceQuotation) {
        this.priceQuotation = priceQuotation;
    }

    public String getSalesOrderNotes() {
        return salesOrderNotes;
    }

    public void setSalesOrderNotes(String salesOrderNotes) {
        this.salesOrderNotes = salesOrderNotes;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    @Override
    public String toString() {
        return "SalesOrderDetailDTO{" +
                "id=" + id +
                ", priceQuotation=" + priceQuotation +
                ", soNumber='" + soNumber + '\'' +
                ", pqNumber='" + pqNumber + '\'' +
                ", createdDate=" + createdDate +
                ", prNumber='" + prNumber + '\'' +
                ", salesPerson=" + salesPerson +
                ", salesAdmin=" + salesAdmin +
                ", customer=" + customer +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", notes='" + notes + '\'' +
                ", salesOrderNotes='" + salesOrderNotes + '\'' +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", tax='" + tax + '\'' +
                ", discount=" + discount +
                ", subTotal=" + subTotal +
                ", discountPrice=" + discountPrice +
                ", taxPrice=" + taxPrice +
                ", finalPrice=" + finalPrice +
                ", detailSalesOrder=" + detailSalesOrder +
                ", listOfAttach=" + listOfAttach +
                ", prOrigin='" + prOrigin + '\'' +
                ", currency='" + currency + '\'' +
                ", shipmentType='" + shipmentType + '\'' +
                ", shipmentDate=" + shipmentDate +
                ", po='" + po + '\'' +
                '}';
    }
}
