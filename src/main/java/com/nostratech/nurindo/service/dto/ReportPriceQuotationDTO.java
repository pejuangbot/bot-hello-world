package com.nostratech.nurindo.service.dto;

import java.math.BigDecimal;

public class ReportPriceQuotationDTO {

    private String name;
    private String size;
    private String measure;
    private BigDecimal hargaMeter;
    private BigDecimal price;
    private Integer quantity;
    private Integer totalLuas;
    private BigDecimal subTotal;

    public ReportPriceQuotationDTO() {
    }

    public ReportPriceQuotationDTO(String name, String size, String measure, BigDecimal hargaMeter, BigDecimal price, Integer quantity, Integer totalLuas, BigDecimal subTotal) {
        this.name = name;
        this.size = size;
        this.measure = measure;
        this.hargaMeter = hargaMeter;
        this.price = price;
        this.quantity = quantity;
        this.totalLuas = totalLuas;
        this.subTotal = subTotal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public BigDecimal getHargaMeter() {
        return hargaMeter;
    }

    public void setHargaMeter(BigDecimal hargaMeter) {
        this.hargaMeter = hargaMeter;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getTotalLuas() {
        return totalLuas;
    }

    public void setTotalLuas(Integer totalLuas) {
        this.totalLuas = totalLuas;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    @Override
    public String toString() {
        return "ReportPriceQuotationDTO{" +
                "name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", measure='" + measure + '\'' +
                ", hargaMeter='" + hargaMeter + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", totalLuas='" + totalLuas + '\'' +
                ", subTotal=" + subTotal +
                '}';
    }
}
