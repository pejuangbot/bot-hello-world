package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class CustomerContactPersonDTO implements Serializable{
    private Long id;

    @Size(max = 48)
    @JsonProperty(value = "full_name")
    private String fullName;

    @JsonProperty(value = "title")
    private String title;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @Size(max = 32)
    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @Size(max = 32)
    @JsonProperty(value = "fax_number")
    private String faxNumber;

    @Size(max = 32)
    @JsonProperty(value = "mobile_number")
    private String mobileNumber;

    @JsonProperty(value = "status")
    private String status;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "customer_id")
    private Long customerId;

    public CustomerContactPersonDTO() {
    }

    public CustomerContactPersonDTO(Long id, @Size(max = 48) String fullName, String title, @Email @Size(min = 5, max = 254) String email, @Size(max = 32) String phoneNumber, @Size(max = 32) String faxNumber, @Size(max = 32) String mobileNumber, String status, String description, Long customerId) {
        this.id = id;
        this.fullName = fullName;
        this.title = title;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.mobileNumber = mobileNumber;
        this.status = status;
        this.description = description;
        this.customerId = customerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "CustomerContactPersonDTO{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", title='" + title + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", mobileNumber='" + mobileNumber + '\'' +
                ", status='" + status + '\'' +
                ", description='" + description + '\'' +
                ", customerId=" + customerId +
                '}';
    }
}
