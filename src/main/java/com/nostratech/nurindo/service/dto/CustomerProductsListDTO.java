package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

public class CustomerProductsListDTO {
    @JsonProperty(value = "customer_product_id")
    private Long id;

    @JsonProperty(value = "product_id")
    private Long productId;

    @JsonProperty(value = "product_name")
    private String productName;

    @NotNull
    @JsonProperty(value = "agreed_price")
    private BigDecimal agreedPrice;

    @JsonProperty(value = "list_of_material")
    private List<MaterialDTO> listOfMaterial;

    @JsonProperty(value = "list_of_printer")
    private List<PrinterDTO> listOfPrinter;

    public CustomerProductsListDTO() {
    }

    public CustomerProductsListDTO(Long id, Long productId, String productName, @NotNull BigDecimal agreedPrice, List<MaterialDTO> listOfMaterial, List<PrinterDTO> listOfPrinter) {
        this.id = id;
        this.productId = productId;
        this.productName = productName;
        this.agreedPrice = agreedPrice;
        this.listOfMaterial = listOfMaterial;
        this.listOfPrinter = listOfPrinter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getAgreedPrice() {
        return agreedPrice;
    }

    public void setAgreedPrice(BigDecimal agreedPrice) {
        this.agreedPrice = agreedPrice;
    }

    public List<MaterialDTO> getListOfMaterial() {
        return listOfMaterial;
    }

    public void setListOfMaterial(List<MaterialDTO> listOfMaterial) {
        this.listOfMaterial = listOfMaterial;
    }

    public List<PrinterDTO> getListOfPrinter() {
        return listOfPrinter;
    }

    public void setListOfPrinter(List<PrinterDTO> listOfPrinter) {
        this.listOfPrinter = listOfPrinter;
    }

    @Override
    public String toString() {
        return "CustomerProductsListDTO{" +
                "id=" + id +
                ", productId=" + productId +
                ", productName='" + productName + '\'' +
                ", agreedPrice=" + agreedPrice +
                ", listOfMaterial=" + listOfMaterial +
                ", listOfPrinter=" + listOfPrinter +
                '}';
    }
}
