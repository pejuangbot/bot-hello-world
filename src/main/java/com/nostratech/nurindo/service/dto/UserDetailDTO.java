package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostratech.nurindo.util.Constants;
import io.swagger.models.auth.In;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Date;
import java.util.List;

public class UserDetailDTO extends UserListDTO {

    @JsonProperty(value = "created_date")
    private Long createdDate;

    public UserDetailDTO() {
    }

    public UserDetailDTO(UserListDTO userListDTO, Long createdDate) {
        super(userListDTO);
        this.createdDate = createdDate;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public String toString() {
        return "UserDetailDTO{" +
                "createdDate=" + createdDate +
                '}';
    }
}
