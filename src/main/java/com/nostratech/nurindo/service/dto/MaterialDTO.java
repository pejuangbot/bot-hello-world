package com.nostratech.nurindo.service.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class MaterialDTO {

    private Long id;

    @NotBlank
    @Size(min = 5, max = 50)
    private String name;

    public MaterialDTO() {
    }

    public MaterialDTO(Long id, @NotBlank @Size(min = 5, max = 50) String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MaterialDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
