package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class SalesOrderListDTO {

    private Long id;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "so_number")
    private String soNumber;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pq_number")
    private String pqNumber;

    @NotBlank
    @JsonProperty(value = "account_executive")
    private String accountExecutive;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "project_name")
    private String projectName;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "customer_name")
    private String customerName;

    @JsonProperty(value = "created_date")
    private Long createdDate;

    @JsonProperty(value = "status")
    private String status;

    public SalesOrderListDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPqNumber() {
        return pqNumber;
    }

    public void setPqNumber(String pqNumber) {
        this.pqNumber = pqNumber;
    }

    public String getAccountExecutive() {
        return accountExecutive;
    }

    public void setAccountExecutive(String accountExecutive) {
        this.accountExecutive = accountExecutive;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSoNumber() {
        return soNumber;
    }

    public void setSoNumber(String soNumber) {
        this.soNumber = soNumber;
    }

    @Override
    public String toString() {
        return "SalesOrderListDTO{" +
                "id=" + id +
                ", soNumber='" + soNumber + '\'' +
                ", pqNumber='" + pqNumber + '\'' +
                ", accountExecutive='" + accountExecutive + '\'' +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", createdDate=" + createdDate +
                ", status='" + status + '\'' +
                '}';
    }
}
