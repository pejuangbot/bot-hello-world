package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class ProductRoleDTO extends RoleBaseDTO {

    @NotNull
    @JsonProperty(value = "bottom_price")
    private BigDecimal bottomPrice;

    @NotNull
    private BigDecimal msrp;

    public ProductRoleDTO() {
    }

    public ProductRoleDTO(Long id, String name, ProductRoleDTO productRoleDTO) {
        super(id, name);
        this.bottomPrice = productRoleDTO.getBottomPrice();
        this.msrp = productRoleDTO.getMsrp();
    }

    public BigDecimal getBottomPrice() {
        return bottomPrice;
    }

    public void setBottomPrice(BigDecimal bottomPrice) {
        this.bottomPrice = bottomPrice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }


    @Override
    public String toString() {
        return "ProductRoleDTO{" +
                "id="+ getId()+
                "bottomPrice=" + bottomPrice +
                ", msrp=" + msrp +
                '}';
    }
}
