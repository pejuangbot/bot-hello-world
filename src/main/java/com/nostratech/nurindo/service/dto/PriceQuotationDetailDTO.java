package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.User;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

public class PriceQuotationDetailDTO {

    private Long id;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pq_number")
    private String pqNumber;

    @JsonProperty(value = "created_date")
    private Long createdDate;

    @NotBlank
    @Size(max = 20)
    @JsonProperty(value = "pr_number")
    private String prNumber;

    @NotNull
    @JsonProperty(value = "sales_person")
    private UserDTO salesPerson;

    @NotNull
    @JsonProperty(value = "sales_admin")
    private UserDTO salesAdmin;

    @NotNull
    @JsonProperty(value = "customer")
    private CustomerListDTO customer;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "project_name")
    private String projectName;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "customer_name")
    private String customerName;

    @NotBlank
    @Size(max = 32)
    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @NotBlank
    @Size(max = 32)
    @JsonProperty(value = "fax_number")
    private String faxNumber;

    @NotBlank
    @Size(max = 254)
    private String email;

    @NotBlank
    @Size(max = 254)
    private String notes;

    @NotBlank
    @Size(max = 10)
    private String type;

    @NotBlank
    private String tax;

    @NotNull
    @Min(0)
    @Max(100)
    private Double discount;

    @NotNull
    @JsonProperty(value = "sub_total")
    private BigDecimal subTotal;

    @NotNull
    @JsonProperty(value = "discount_price")
    private BigDecimal discountPrice;

    @NotNull
    @JsonProperty(value = "tax_price")
    private BigDecimal taxPrice;

    @NotNull
    @JsonProperty(value = "final_price")
    private BigDecimal finalPrice;

    @NotNull
    @JsonProperty(value = "detail_price_quotation")
    private List<PriceQuotationDetailTrxListDTO> detailTrxDTOS;

    @NotNull
    @JsonProperty(value = "list_of_attach")
    private List<FileUploadDTO> listOfAttach;

    @JsonProperty(value = "price_quotation_status")
    private String status;

    @JsonProperty(value = "need_approval_from")
    private String needApprovalFrom;

    @Size(max = 50)
    @JsonProperty(value = "po")
    private String po;

    @Size(max = 50)
    @JsonProperty(value = "attn")
    private String attn;

    @Size(max = 50)
    @JsonProperty(value = "cc")
    private String cc;


    public PriceQuotationDetailDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPqNumber() {
        return pqNumber;
    }

    public void setPqNumber(String pqNumber) {
        this.pqNumber = pqNumber;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public UserDTO getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(UserDTO salesPerson) {
        this.salesPerson = salesPerson;
    }

    public UserDTO getSalesAdmin() {
        return salesAdmin;
    }

    public void setSalesAdmin(UserDTO salesAdmin) {
        this.salesAdmin = salesAdmin;
    }

    public CustomerListDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerListDTO customer) {
        this.customer = customer;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(BigDecimal taxPrice) {
        this.taxPrice = taxPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public List<PriceQuotationDetailTrxListDTO> getDetailTrxDTOS() {
        return detailTrxDTOS;
    }

    public void setDetailTrxDTOS(List<PriceQuotationDetailTrxListDTO> detailTrxDTOS) {
        this.detailTrxDTOS = detailTrxDTOS;
    }

    public List<FileUploadDTO> getListOfAttach() {
        return listOfAttach;
    }

    public void setListOfAttach(List<FileUploadDTO> listOfAttach) {
        this.listOfAttach = listOfAttach;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNeedApprovalFrom() {
        return needApprovalFrom;
    }

    public void setNeedApprovalFrom(String needApprovalFrom) {
        this.needApprovalFrom = needApprovalFrom;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public String getAttn() {
        return attn;
    }

    public void setAttn(String attn) {
        this.attn = attn;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Override
    public String toString() {
        return "PriceQuotationDetailDTO{" +
                "id=" + id +
                ", pqNumber='" + pqNumber + '\'' +
                ", createdDate=" + createdDate +
                ", prNumber='" + prNumber + '\'' +
                ", salesPerson=" + salesPerson +
                ", salesAdmin=" + salesAdmin +
                ", customer=" + customer +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", notes='" + notes + '\'' +
                ", type='" + type + '\'' +
                ", tax='" + tax + '\'' +
                ", discount=" + discount +
                ", subTotal=" + subTotal +
                ", discountPrice=" + discountPrice +
                ", taxPrice=" + taxPrice +
                ", finalPrice=" + finalPrice +
                ", detailTrxDTOS=" + detailTrxDTOS +
                ", listOfAttach=" + listOfAttach +
                ", status='" + status + '\'' +
                ", needApprovalFrom='" + needApprovalFrom + '\'' +
                ", po='" + po + '\'' +
                ", attn='" + attn + '\'' +
                ", cc='" + cc + '\'' +
                '}';
    }
}
