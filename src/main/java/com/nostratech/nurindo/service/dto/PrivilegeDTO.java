package com.nostratech.nurindo.service.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class PrivilegeDTO implements Serializable {

    private Long id;

    @NotNull
    private Long menu;

    @NotNull
    private Boolean create;

    @NotNull
    private Boolean read;

    @NotNull
    private Boolean update;

    @NotNull
    private Boolean delete;

    public PrivilegeDTO() {
    }

    public PrivilegeDTO(Long id, Long menu, Boolean create, Boolean read,
        Boolean update, Boolean delete) {
        this.id = id;
        this.menu = menu;
        this.create = create;
        this.read = read;
        this.update = update;
        this.delete = delete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMenu() {
        return menu;
    }

    public void setMenu(Long menu) {
        this.menu = menu;
    }

    public Boolean getCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(Boolean update) {
        this.update = update;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    @Override
    public String toString() {
        return "PrivilegeDTO{" +
            "id=" + id +
            ", menu=" + menu +
            ", create=" + create +
            ", read=" + read +
            ", update=" + update +
            ", delete=" + delete +
            '}';
    }
}
