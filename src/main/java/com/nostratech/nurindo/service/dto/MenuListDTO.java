package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostratech.nurindo.domain.Menu;

import java.io.Serializable;

public class MenuListDTO implements Serializable {

    private Long id;

    private String name;

    @JsonProperty(value = "menu_name")
    private String menuName;

    private String description;

    private Long parent;

    private Boolean activated;

    private Boolean create;

    private Boolean read;

    private Boolean update;

    private Boolean delete;

    public MenuListDTO() {
    }

    public MenuListDTO(Long id, String name, String menuName, String description, Long parent, Boolean activated, Boolean create, Boolean read, Boolean update, Boolean delete) {
        this.id = id;
        this.name = name;
        this.menuName = menuName;
        this.description = description;
        this.parent = parent;
        this.activated = activated;
        this.create = create;
        this.read = read;
        this.update = update;
        this.delete = delete;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Boolean getCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(Boolean update) {
        this.update = update;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    @Override
    public String toString() {
        return "MenuListDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", menuName='" + menuName + '\'' +
                ", description='" + description + '\'' +
                ", parent=" + parent +
                ", activated=" + activated +
                ", create=" + create +
                ", read=" + read +
                ", update=" + update +
                ", delete=" + delete +
                '}';
    }
}
