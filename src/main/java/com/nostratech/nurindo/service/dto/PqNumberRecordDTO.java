package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PqNumberRecordDTO implements Serializable {
    private Long id;

    @JsonProperty(value = "number_generated")
    private String numberGenerated;

    @JsonProperty(value = "month")
    private String month;

    @JsonProperty(value = "year")
    private String year;

    @JsonProperty(value = "number_type")
    private String numberType;

    public PqNumberRecordDTO() {
    }

    public PqNumberRecordDTO(Long id, String numberGenerated, String month, String year, String numberType) {
        this.id = id;
        this.numberGenerated = numberGenerated;
        this.month = month;
        this.year = year;
        this.numberType = numberType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumberGenerated() {
        return numberGenerated;
    }

    public void setNumberGenerated(String numberGenerated) {
        this.numberGenerated = numberGenerated;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    @Override
    public String toString() {
        return "PqNumberRecordDTO{" +
                "id=" + id +
                ", numberGenerated='" + numberGenerated + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", numberType='" + numberType + '\'' +
                '}';
    }
}
