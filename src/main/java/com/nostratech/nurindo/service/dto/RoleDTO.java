package com.nostratech.nurindo.service.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class RoleDTO extends RoleBaseDTO implements Serializable {

    @NotBlank
    @Size(max = 100)
    private String description;

    @NotNull
    private Boolean activated;

    @NotNull
    @JsonProperty(value = "menus")
    private List<PrivilegeDTO> privilegeList;

    public RoleDTO() {
    }

    public RoleDTO(
        @Size(max = 100) String description, Boolean activated) {
        this.description = description;
        this.activated = activated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public List<PrivilegeDTO> getPrivilegeList() {
        return privilegeList;
    }

    public void setPrivilegeList(List<PrivilegeDTO> privilegeList) {
        this.privilegeList = privilegeList;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
            ", description='" + description + '\'' +
            ", activated=" + activated +
            '}';
    }
}
