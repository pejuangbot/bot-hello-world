package com.nostratech.nurindo.service.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class MenuDTO implements Serializable {

    private Long id;

    @NotBlank
    @Size(min = 5, max = 50)
    private String name;

    @NotBlank
    @Size(max = 100)
    private String description;

    @Size(max = 100)
    private String url;
    
    @NotNull
    private Long parent;

    private boolean activated = false;

    public MenuDTO() {
    }

    public MenuDTO(Long id, @NotBlank @Size(min = 5, max = 50) String name, @NotBlank @Size(max = 100) String description, @Size(max = 100) String url, @NotNull Long parent, boolean activated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.url = url;
        this.parent = parent;
        this.activated = activated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    @Override
    public String toString() {
        return "MenuDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", parent=" + parent +
                ", activated=" + activated +
                '}';
    }
}
