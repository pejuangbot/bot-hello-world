package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class SalesOrderDetailTrxDTO {

    @NotBlank
    @Size(max = 100)
    private String name;

    private String size;

    @Size(max = 100)
    private String resolution;

    @Size(max = 100)
    private String material;

    @NotNull
    private BigDecimal msrp;

    @NotNull
    @JsonProperty(value = "bottom_price")
    private BigDecimal bottomPrice;

    @NotNull
    private Integer quantity;

    @NotNull
    private BigDecimal price;

    @NotNull
    @JsonProperty(value = "total_price")
    private BigDecimal totalPrice;

    public SalesOrderDetailTrxDTO() {
    }

    public SalesOrderDetailTrxDTO(@NotBlank @Size(max = 100) String name, String size, String resolution, String material, @NotNull BigDecimal msrp, @NotNull BigDecimal bottomPrice, @NotNull Integer quantity, @NotNull BigDecimal price, @NotNull BigDecimal totalPrice) {
        this.name = name;
        this.size = size;
        this.resolution = resolution;
        this.material = material;
        this.msrp = msrp;
        this.bottomPrice = bottomPrice;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public BigDecimal getBottomPrice() {
        return bottomPrice;
    }

    public void setBottomPrice(BigDecimal bottomPrice) {
        this.bottomPrice = bottomPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "SalesOrderDetailTrxDTO{" +
                "name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", resolution='" + resolution + '\'' +
                ", material='" + material + '\'' +
                ", msrp=" + msrp +
                ", bottomPrice=" + bottomPrice +
                ", quantity=" + quantity +
                ", price=" + price +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
