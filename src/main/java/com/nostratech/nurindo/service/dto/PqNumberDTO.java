package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PqNumberDTO implements Serializable {

    @JsonProperty(value = "kind_of_message")
    private String messageKind;


    @JsonProperty(value = "value")
    private String value;

    public PqNumberDTO() {
    }

    public PqNumberDTO(String messageKind, String value) {
        this.messageKind = messageKind;
        this.value = value;
    }

    public String getMessageKind() {
        return messageKind;
    }

    public void setMessageKind(String messageKind) {
        this.messageKind = messageKind;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "PqNumberDTO{" +
                "messageKind='" + messageKind + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
