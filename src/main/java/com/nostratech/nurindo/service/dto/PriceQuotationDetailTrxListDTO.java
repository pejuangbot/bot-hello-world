package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostratech.nurindo.domain.Material;
import com.nostratech.nurindo.domain.Printer;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class PriceQuotationDetailTrxListDTO {

    @NotBlank
    @Size(max = 100)
    private String name;

    private Integer height;

    private Integer weight;

    private MaterialDTO material;

    private PrinterDTO printer;

    private String resolution;

    @NotNull
    private BigDecimal msrp;

    @NotNull
    @JsonProperty(value = "bottom_price")
    private BigDecimal bottomPrice;

    @NotNull
    private Integer quantity;

    @NotNull
    private BigDecimal price;

    @NotNull
    @JsonProperty(value = "total_price")
    private BigDecimal totalPrice;

    public PriceQuotationDetailTrxListDTO() {
    }

    public PriceQuotationDetailTrxListDTO(@NotBlank @Size(max = 100) String name, Integer height, Integer weight, MaterialDTO material, PrinterDTO printer, String resolution, @NotNull BigDecimal msrp, @NotNull BigDecimal bottomPrice, @NotNull Integer quantity, @NotNull BigDecimal price, @NotNull BigDecimal totalPrice) {
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.material = material;
        this.printer = printer;
        this.resolution = resolution;
        this.msrp = msrp;
        this.bottomPrice = bottomPrice;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public BigDecimal getBottomPrice() {
        return bottomPrice;
    }

    public void setBottomPrice(BigDecimal bottomPrice) {
        this.bottomPrice = bottomPrice;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public MaterialDTO getMaterial() {
        return material;
    }

    public void setMaterial(MaterialDTO material) {
        this.material = material;
    }

    public PrinterDTO getPrinter() {
        return printer;
    }

    public void setPrinter(PrinterDTO printer) {
        this.printer = printer;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    @Override
    public String toString() {
        return "PriceQuotationDetailTrxDTO{" +
                "name='" + name + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", material='" + material + '\'' +
                ", printer='" + printer + '\'' +
                ", resolution='" + resolution + '\'' +
                ", msrp=" + msrp +
                ", bottomPrice=" + bottomPrice +
                ", quantity=" + quantity +
                ", price=" + price +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
