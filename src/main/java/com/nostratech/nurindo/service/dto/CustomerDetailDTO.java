package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;

public class CustomerDetailDTO implements Serializable {

    private Long id;

    @Size(max = 32)
    @JsonProperty(value = "last_updated_process")
    private String lastUpdateProcess;

    @JsonProperty(value = "is_deleted")
    private Integer isDeleted;

    @Size(max = 32)
    @JsonProperty(value = "customer_code")
    private String customerCode;

    @Size(max = 64)
    @JsonProperty(value = "customer_name")
    private String customerName;

    @Size(max = 128)
    private String address;

    @Size(max = 16)
    @JsonProperty(value = "city_code")
    private String cityCode;

    @Size(max = 5)
    @JsonProperty(value = "zip_code")
    private String zipCode;

    @Size(max = 20)
    @JsonProperty(value = "npwp_id")
    private String npwpId;

    @Size(max = 128)
    @JsonProperty(value = "npwp_address")
    private String npwpAddress;

    @Size(max = 20)
    @JsonProperty(value = "npwp_city_code")
    private String npwpCityCode;

    @Size(max = 5)
    @JsonProperty(value = "npwp_zip_code")
    private String npwpZipCode;

    @Size(max = 64)
    @JsonProperty(value = "npwp_name")
    private String npwpName;

    @Size(max = 24)
    @JsonProperty(value = "quotation_format")
    private String quotationFormat;

    @Size(max = 16)
    @JsonProperty(value = "group_code")
    private String groupCode;

    @Size(max = 32)
    private String status;

    @JsonProperty(value = "need_numbering")
    private Integer needNumbering;

    @Size(max = 16)
    private String top;

    @Size(max = 32)
    @JsonProperty(value = "top_um")
    private String topUm;

    @Size(max = 128)
    @JsonProperty(value = "address_detail")
    private String addressDetail;

    @Size(max = 16)
    @JsonProperty(value = "sales_code")
    private String salesCode;

    @Size(max = 32)
    @JsonProperty(value = "customer_type")
    private String customerType;

    @JsonProperty(value = "created_date")
    private Instant createdDate;

    @JsonProperty(value = "last_update")
    private Instant lastModifiedDate;

    @JsonProperty(value = "last_update_by")
    private String lastModifiedBy;

    @JsonProperty(value = "contact_persons")
    List<CustomerContactPersonDTO> customerContactPersonDTOS;

    public CustomerDetailDTO() {
    }

    public CustomerDetailDTO(Long id, @Size(max = 32) String lastUpdateProcess, Integer isDeleted, @Size(max = 32) String customerCode, @Size(max = 64) String customerName, @Size(max = 128) String address, @Size(max = 16) String cityCode, @Size(max = 5) String zipCode, @Size(max = 20) String npwpId, @Size(max = 128) String npwpAddress, @Size(max = 20) String npwpCityCode, @Size(max = 5) String npwpZipCode, @Size(max = 64) String npwpName, @Size(max = 24) String quotationFormat, @Size(max = 16) String groupCode, @Size(max = 32) String status, Integer needNumbering, @Size(max = 16) String top, @Size(max = 32) String topUm, @Size(max = 128) String addressDetail, @Size(max = 16) String salesCode, @Size(max = 32) String customerType, Instant createdDate, Instant lastModifiedDate, String lastModifiedBy, List<CustomerContactPersonDTO> customerContactPersonDTOS) {
        this.id = id;
        this.lastUpdateProcess = lastUpdateProcess;
        this.isDeleted = isDeleted;
        this.customerCode = customerCode;
        this.customerName = customerName;
        this.address = address;
        this.cityCode = cityCode;
        this.zipCode = zipCode;
        this.npwpId = npwpId;
        this.npwpAddress = npwpAddress;
        this.npwpCityCode = npwpCityCode;
        this.npwpZipCode = npwpZipCode;
        this.npwpName = npwpName;
        this.quotationFormat = quotationFormat;
        this.groupCode = groupCode;
        this.status = status;
        this.needNumbering = needNumbering;
        this.top = top;
        this.topUm = topUm;
        this.addressDetail = addressDetail;
        this.salesCode = salesCode;
        this.customerType = customerType;
        this.createdDate = createdDate;
        this.lastModifiedDate = lastModifiedDate;
        this.lastModifiedBy = lastModifiedBy;
        this.customerContactPersonDTOS = customerContactPersonDTOS;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastUpdateProcess() {
        return lastUpdateProcess;
    }

    public void setLastUpdateProcess(String lastUpdateProcess) {
        this.lastUpdateProcess = lastUpdateProcess;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNpwpId() {
        return npwpId;
    }

    public void setNpwpId(String npwpId) {
        this.npwpId = npwpId;
    }

    public String getNpwpAddress() {
        return npwpAddress;
    }

    public void setNpwpAddress(String npwpAddress) {
        this.npwpAddress = npwpAddress;
    }

    public String getNpwpCityCode() {
        return npwpCityCode;
    }

    public void setNpwpCityCode(String npwpCityCode) {
        this.npwpCityCode = npwpCityCode;
    }

    public String getNpwpZipCode() {
        return npwpZipCode;
    }

    public void setNpwpZipCode(String npwpZipCode) {
        this.npwpZipCode = npwpZipCode;
    }

    public String getNpwpName() {
        return npwpName;
    }

    public void setNpwpName(String npwpName) {
        this.npwpName = npwpName;
    }

    public String getQuotationFormat() {
        return quotationFormat;
    }

    public void setQuotationFormat(String quotationFormat) {
        this.quotationFormat = quotationFormat;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNeedNumbering() {
        return needNumbering;
    }

    public void setNeedNumbering(Integer needNumbering) {
        this.needNumbering = needNumbering;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getTopUm() {
        return topUm;
    }

    public void setTopUm(String topUm) {
        this.topUm = topUm;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getSalesCode() {
        return salesCode;
    }

    public void setSalesCode(String salesCode) {
        this.salesCode = salesCode;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public List<CustomerContactPersonDTO> getCustomerContactPersonDTOS() {
        return customerContactPersonDTOS;
    }

    public void setCustomerContactPersonDTOS(List<CustomerContactPersonDTO> customerContactPersonDTOS) {
        this.customerContactPersonDTOS = customerContactPersonDTOS;
    }

    @Override
    public String toString() {
        return "CustomerDetailDTO{" +
                "id=" + id +
                ", lastUpdateProcess='" + lastUpdateProcess + '\'' +
                ", isDeleted=" + isDeleted +
                ", customerCode='" + customerCode + '\'' +
                ", customerName='" + customerName + '\'' +
                ", address='" + address + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", npwpId='" + npwpId + '\'' +
                ", npwpAddress='" + npwpAddress + '\'' +
                ", npwpCityCode='" + npwpCityCode + '\'' +
                ", npwpZipCode='" + npwpZipCode + '\'' +
                ", npwpName='" + npwpName + '\'' +
                ", quotationFormat='" + quotationFormat + '\'' +
                ", groupCode='" + groupCode + '\'' +
                ", status='" + status + '\'' +
                ", needNumbering=" + needNumbering +
                ", top='" + top + '\'' +
                ", topUm='" + topUm + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", salesCode='" + salesCode + '\'' +
                ", customerType='" + customerType + '\'' +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", customerContactPersonDTOS=" + customerContactPersonDTOS +
                '}';
    }
}
