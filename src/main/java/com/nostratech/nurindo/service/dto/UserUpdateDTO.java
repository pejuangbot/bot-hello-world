package com.nostratech.nurindo.service.dto;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.util.Constants;

import javax.validation.constraints.*;
import java.util.List;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserUpdateDTO extends UserCreateDTO {

    @NotNull
    private Boolean enabled;

    public UserUpdateDTO() {
    }

    public UserUpdateDTO(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "UserUpdateDTO{" +
                "enabled=" + enabled +
                '}';
    }
}
