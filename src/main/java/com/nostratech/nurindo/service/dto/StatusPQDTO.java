package com.nostratech.nurindo.service.dto;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class StatusPQDTO {

    @NotBlank
    private String status;

    public StatusPQDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "StatusPQDTO{" +
                "status='" + status + '\'' +
                '}';
    }
}
