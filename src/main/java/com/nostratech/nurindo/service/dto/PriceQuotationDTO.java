package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

public class PriceQuotationDTO {

    private Long id;

    @NotBlank
    @Size(max = 50)
    @JsonProperty(value = "pr_number")
    private String prNumber;

    @NotNull
    @JsonProperty(value = "account_executive")
    private Long accountExecutive;

    @NotNull
    @JsonProperty(value = "sales_admin")
    private Long salesAdmin;

    @NotNull
    private Long customer;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "project_name")
    private String projectName;

    @NotBlank
    @Size(max = 100)
    @JsonProperty(value = "customer_name")
    private String customerName;

    @Size(max = 32)
    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @Size(max = 32)
    @JsonProperty(value = "fax_number")
    private String faxNumber;

    @Size(max = 254)
    private String email;

    @Size(max = 254)
    private String notes;

    @NotBlank
    @Size(max = 10)
    private String type;

    @NotBlank
    private String tax;

    @NotBlank
    private String status;

    @NotNull
    @Min(0)
    @Max(100)
    private Double discount;

    @Valid
    @NotNull
    @JsonProperty(value = "detail_price_quotation")
    private List<PriceQuotationDetailTrxDTO> detailTrxDTOS;

    @NotNull
    @JsonProperty(value = "sub_total")
    private BigDecimal subTotal;

    @NotNull
    @JsonProperty(value = "discount_price")
    private BigDecimal discountPrice;

    @NotNull
    @JsonProperty(value = "tax_price")
    private BigDecimal taxPrice;

    @NotNull
    @JsonProperty(value = "final_price")
    private BigDecimal finalPrice;

    @NotNull
    @JsonProperty(value = "role_id")
    private Long roleId;

    @JsonProperty(value = "list_of_attach")
    private List<FileUploadDTO> listOfAttach;

    @Size(max = 50)
    @JsonProperty(value = "po")
    private String po;

    @Size(max = 50)
    @JsonProperty(value = "attn")
    private String attn;

    @Size(max = 50)
    @JsonProperty(value = "cc")
    private String cc;

    @NotNull
    public PriceQuotationDTO() {
    }

    public PriceQuotationDTO(Long id, @NotBlank @Size(max = 20) String prNumber, @NotNull Long accountExecutive, @NotNull Long salesAdmin, @NotNull Long customer, @NotBlank @Size(max = 100) String projectName, @NotBlank @Size(max = 100) String customerName, @NotBlank @Size(max = 32) String phoneNumber, @NotBlank @Size(max = 32) String faxNumber, @NotBlank @Size(max = 254) String email, @NotBlank @Size(max = 254) String notes, @NotBlank @Size(max = 10) String type, @NotBlank String tax, @NotBlank String status, @NotNull @Min(0) @Max(100) Double discount, @Valid @NotNull List<PriceQuotationDetailTrxDTO> detailTrxDTOS, @NotNull BigDecimal subTotal, @NotNull BigDecimal discountPrice, @NotNull BigDecimal taxPrice, @NotNull BigDecimal finalPrice, Long roleId, List<FileUploadDTO> listOfAttach, @Size(max = 50) String po, @Size(max = 50) String attn, @Size(max = 50) String cc) {
        this.id = id;
        this.prNumber = prNumber;
        this.accountExecutive = accountExecutive;
        this.salesAdmin = salesAdmin;
        this.customer = customer;
        this.projectName = projectName;
        this.customerName = customerName;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.email = email;
        this.notes = notes;
        this.type = type;
        this.tax = tax;
        this.status = status;
        this.discount = discount;
        this.detailTrxDTOS = detailTrxDTOS;
        this.subTotal = subTotal;
        this.discountPrice = discountPrice;
        this.taxPrice = taxPrice;
        this.finalPrice = finalPrice;
        this.roleId = roleId;
        this.listOfAttach = listOfAttach;
        this.po = po;
        this.attn = attn;
        this.cc = cc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public Long getAccountExecutive() {
        return accountExecutive;
    }

    public void setAccountExecutive(Long accountExecutive) {
        this.accountExecutive = accountExecutive;
    }

    public Long getSalesAdmin() {
        return salesAdmin;
    }

    public void setSalesAdmin(Long salesAdmin) {
        this.salesAdmin = salesAdmin;
    }

    public Long getCustomer() {
        return customer;
    }

    public void setCustomer(Long customer) {
        this.customer = customer;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public List<PriceQuotationDetailTrxDTO> getDetailTrxDTOS() {
        return detailTrxDTOS;
    }

    public void setDetailTrxDTOS(List<PriceQuotationDetailTrxDTO> detailTrxDTOS) {
        this.detailTrxDTOS = detailTrxDTOS;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(BigDecimal taxPrice) {
        this.taxPrice = taxPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public List<FileUploadDTO> getListOfAttach() {
        return listOfAttach;
    }

    public void setListOfAttach(List<FileUploadDTO> listOfAttach) {
        this.listOfAttach = listOfAttach;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public String getAttn() {
        return attn;
    }

    public void setAttn(String attn) {
        this.attn = attn;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Override
    public String toString() {
        return "PriceQuotationDTO{" +
                "id=" + id +
                ", prNumber='" + prNumber + '\'' +
                ", accountExecutive=" + accountExecutive +
                ", salesAdmin=" + salesAdmin +
                ", customer=" + customer +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", notes='" + notes + '\'' +
                ", type='" + type + '\'' +
                ", tax='" + tax + '\'' +
                ", status='" + status + '\'' +
                ", discount=" + discount +
                ", detailTrxDTOS=" + detailTrxDTOS +
                ", subTotal=" + subTotal +
                ", discountPrice=" + discountPrice +
                ", taxPrice=" + taxPrice +
                ", finalPrice=" + finalPrice +
                ", roleId=" + roleId +
                ", listOfAttach=" + listOfAttach +
                ", po='" + po + '\'' +
                ", attn='" + attn + '\'' +
                ", cc='" + cc + '\'' +
                '}';
    }
}
