package com.nostratech.nurindo.service.dto;

import org.codehaus.jackson.annotate.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class RoleBaseDTO implements Serializable {

    private Long id;

    @Size(min = 3, max = 50)
    private String name;

    public RoleBaseDTO() {
    }

    public RoleBaseDTO(Long id,
                       @NotBlank @Size(min = 3, max = 50) String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }
}
