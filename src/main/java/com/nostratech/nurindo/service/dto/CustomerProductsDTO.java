package com.nostratech.nurindo.service.dto;



import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CustomerProductsDTO{

    @JsonProperty(value = "customer_product_id")
    private Long id;

    @JsonProperty(value = "product_id")
    private Long productId;

    @NotNull
    @JsonProperty(value = "agreed_price")
    private BigDecimal agreedPrice;

    public CustomerProductsDTO() {
    }

    public CustomerProductsDTO(Long id, Long productId, @NotNull BigDecimal agreedPrice) {
        this.id = id;
        this.productId = productId;
        this.agreedPrice = agreedPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public BigDecimal getAgreedPrice() {
        return agreedPrice;
    }

    public void setAgreedPrice(BigDecimal agreedPrice) {
        this.agreedPrice = agreedPrice;
    }

    @Override
    public String toString() {
        return "CustomerProductsDTO{" +
                "id=" + id +
                ", productId=" + productId +
                ", agreedPrice=" + agreedPrice +
                '}';
    }
}
