package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.nostratech.nurindo.domain.User;

public class UserListDTO extends UserCreateDTO {

    @JsonProperty(value = "last_modified_date")
    private Long lastModifiedDate;

    private Boolean enabled;

    public UserListDTO() {
    }

    public UserListDTO(UserListDTO userListDTO) {
        this.lastModifiedDate = userListDTO.getLastModifiedDate();
        this.enabled = userListDTO.getEnabled();
    }

    public Long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "UserListDTO{" +
                ", lastModifiedDate=" + lastModifiedDate +
                ", enabled=" + enabled +
                '}';
    }

    public static UserListDTO map(User user) {
        UserListDTO userListDTO = new UserListDTO();
        userListDTO.setEnabled(user.isEnabled());
        userListDTO.setLastModifiedDate(user.getLastModifiedDate().toEpochMilli());
        return userListDTO;
    }
}
