package com.nostratech.nurindo.service.dto;


import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class RoleListDTO implements Serializable {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 50)
    private String name;

    @Size(max = 100)
    private String description;

    private Boolean activated;

    @JsonProperty(value = "menus")
    private List<MenuListDTO> menuList;

    public RoleListDTO() {
    }

    public RoleListDTO(Long id,
                       @NotBlank @Size(min = 3, max = 50) String name,
                       @Size(max = 100) String description, Boolean activated) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.activated = activated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public List<MenuListDTO> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuListDTO> menuList) {
        this.menuList = menuList;
    }

    @Override
    public String toString() {
        return "RoleListDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", activated=" + activated +
                ", menuList=" + menuList +
                '}';
    }
}
