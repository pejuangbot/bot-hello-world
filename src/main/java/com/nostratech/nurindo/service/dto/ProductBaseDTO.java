package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class ProductBaseDTO {

    private Long id;

    @NotBlank
    @Size(min = 3, max = 50)
    @JsonProperty(value = "product_code")
    private String productCode;

    @NotBlank
    @Size(min = 3, max = 100)
    private String product;

    private Integer height;

    private Integer width;

    @NotNull
    @Min(value = 0)
    private Integer stock;

    @JsonProperty(value = "unit_measure")
    private String unitMeasure;

    private String type;

    @NotNull
    @Min(value = 0)
    private BigDecimal msrp;

    @NotNull
    @Min(value = 0)
    @JsonProperty(value = "bottom_price_ae")
    private BigDecimal bottomPriceAE;

    @NotNull
    @Min(value = 0)
    @JsonProperty(value = "bottom_price_sm")
    private BigDecimal bottomPriceSM;

    @NotNull
    @Min(value = 0)
    @JsonProperty(value = "bottom_price_sa")
    private BigDecimal bottomPriceSA;

    public ProductBaseDTO() {
    }

    public ProductBaseDTO(Long id, @NotBlank @Size(min = 3, max = 50) String productCode, @NotBlank @Size(min = 3, max = 100) String product, Integer height, Integer width, @NotNull @Min(value = 0) Integer stock, String unitMeasure, String type, @NotNull @Min(value = 0) BigDecimal msrp, @NotNull @Min(value = 0) BigDecimal bottomPriceAE, @NotNull @Min(value = 0) BigDecimal bottomPriceSM, @NotNull @Min(value = 0) BigDecimal bottomPriceSA) {
        this.id = id;
        this.productCode = productCode;
        this.product = product;
        this.height = height;
        this.width = width;
        this.stock = stock;
        this.unitMeasure = unitMeasure;
        this.type = type;
        this.msrp = msrp;
        this.bottomPriceAE = bottomPriceAE;
        this.bottomPriceSM = bottomPriceSM;
        this.bottomPriceSA = bottomPriceSA;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public BigDecimal getBottomPriceAE() {
        return bottomPriceAE;
    }

    public void setBottomPriceAE(BigDecimal bottomPriceAE) {
        this.bottomPriceAE = bottomPriceAE;
    }

    public BigDecimal getBottomPriceSM() {
        return bottomPriceSM;
    }

    public void setBottomPriceSM(BigDecimal bottomPriceSM) {
        this.bottomPriceSM = bottomPriceSM;
    }

    public BigDecimal getBottomPriceSA() {
        return bottomPriceSA;
    }

    public void setBottomPriceSA(BigDecimal bottomPriceSA) {
        this.bottomPriceSA = bottomPriceSA;
    }

    @Override
    public String toString() {
        return "ProductBaseDTO{" +
                "id=" + id +
                ", productCode='" + productCode + '\'' +
                ", product='" + product + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", stock=" + stock +
                ", unitMeasure='" + unitMeasure + '\'' +
                ", type='" + type + '\'' +
                ", msrp=" + msrp +
                ", bottomPriceAE=" + bottomPriceAE +
                ", bottomPriceSM=" + bottomPriceSM +
                ", bottomPriceSA=" + bottomPriceSA +
                '}';
    }
}
