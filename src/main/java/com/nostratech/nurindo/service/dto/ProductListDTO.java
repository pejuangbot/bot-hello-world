package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class ProductListDTO extends ProductBaseDTO {

    @JsonProperty(value = "created_date")
    private Long createdDate;

    @JsonProperty(value = "last_modified_date")
    private Long lastModifiedDate;

    public ProductListDTO() {
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + getId() +
                ", productCode='" + getProductCode() + '\'' +
                ", product='" + getProduct() + '\'' +
                ", size='" + getHeight() + '\'' +
                ", stock=" + getStock() +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                '}';
    }
}
