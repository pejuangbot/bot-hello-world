package com.nostratech.nurindo.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class CityDTO implements Serializable {

    private Long id;

    @JsonProperty(value = "city_code")
    private String cityCode;

    @JsonProperty(value = "city_name")
    private String cityName;

    @JsonProperty(value = "island")
    private String island;

    @JsonProperty(value = "description")
    private String description;

    public CityDTO() {
    }

    public CityDTO(Long id, String cityCode, String cityName, String island, String description) {
        this.id = id;
        this.cityCode = cityCode;
        this.cityName = cityName;
        this.island = island;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getIsland() {
        return island;
    }

    public void setIsland(String island) {
        this.island = island;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CityDTO{" +
                "id=" + id +
                ", cityCode=" + cityCode +
                ", cityName='" + cityName + '\'' +
                ", island='" + island + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
