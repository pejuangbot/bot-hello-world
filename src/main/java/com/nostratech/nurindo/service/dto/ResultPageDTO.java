package com.nostratech.nurindo.service.dto;

public class ResultPageDTO {

    private Object result;
    private String pages;
    private String elements;

    public ResultPageDTO() {
    }

    public ResultPageDTO(Object result, String pages, String elements) {
        this.result = result;
        this.pages = pages;
        this.elements = elements;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getElements() {
        return elements;
    }

    public void setElements(String elements) {
        this.elements = elements;
    }

}
