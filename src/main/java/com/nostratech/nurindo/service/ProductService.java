package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.ProductDTO;
import com.nostratech.nurindo.service.dto.ProductDetailDTO;
import org.springframework.data.domain.Page;

import java.util.Map;
import java.util.Optional;

public interface ProductService{

    /**
     * Save a user
     *
     * @param productDTO the entity to save
     * @return the persisted id
     */
    Boolean createProduct(ProductDTO productDTO);

    /**
     * Update a product
     *
     * @param productDTO the entity to save
     * @return the persisted id
     */
    Boolean updateProduct(ProductDTO productDTO);

    /**
     * Get all of product
     *
     * @return list of entities
     */
    Map<String, Object> getAllProduct(Integer page, Integer limit, String sortBy, String direction, String productCode, String productName);

    /**
     * Get specific product
     *
     * @param id the id of entity
     * @return the entity
     */
    Optional<ProductDetailDTO> getProduct(Long id);

    /**
     * Delete specific product
     *
     * @param id the id of entity
     */
    void deleteProduct(Long id);
    
}
