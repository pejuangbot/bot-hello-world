package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Optional;


public interface UserService {

    /**
     * Save a user
     *
     * @param roleDTO the entity to save
     * @return the persisted id
     */
    Boolean createUser(UserCreateDTO roleDTO);

    /**
     * Update a role
     *
     * @param roleDTO the entity to save
     * @return the persisted id
     */
    Boolean updateUser(UserUpdateDTO roleDTO);

    /**
     * Get all of user
     *
     * @return list of entities
     */
    Map<String, Object> getAllUsers(Integer page, Integer limit, String sortBy, String direction, String username, String role);

    /**
     * Get specific user
     *
     * @param username the id of entity
     * @return the entity
     */
    Optional<UserDetailDTO> getUser(String username);

    /**
     * Change password user
     *
     * @param id the id of entity
     * @param changePasswordDTO the dto to save
     *
     * @return boolean
     */
    Boolean changePassword(String id, ChangePasswordDTO changePasswordDTO);

    /**
     * Delete specific user
     *
     * @param username the id of entity
     */
    void deleteUser(String username);

    /**
     * Change status user
     *
     * @param id the id of user
     * @param statusDTO the entity to save
     * @return the boolean true
     */
    Boolean changeStatus(Long id, StatusDTO statusDTO);

    /**
     * Get all user by roles
     *
     * @return list of entities
     */
    List<UserDTO> findAllByRoles(String roleName);
}
