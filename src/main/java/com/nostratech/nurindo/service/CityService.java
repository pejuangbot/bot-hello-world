package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.CityDTO;

import java.util.Map;

public interface CityService {

    Boolean createCity(CityDTO cityDTO);

    Boolean updateCity(CityDTO cityDTO);

    Boolean deleteCity(Long id);

    Map<String, Object> getAllCityByName(Integer page, Integer limit, String sortBy, String direction, String cityName, String island, String cityCode);


}
