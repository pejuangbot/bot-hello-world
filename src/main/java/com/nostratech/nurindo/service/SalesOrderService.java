package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.SalesOrderDTO;
import com.nostratech.nurindo.service.dto.SalesOrderDetailDTO;

import java.util.Map;
import java.util.Optional;


public interface SalesOrderService {

    /**
     * Save sales order
     *
     * @param salesOrderDTO the entity to save
     * @return the persisted id
     */
    Boolean create(SalesOrderDTO salesOrderDTO);

    /**
     * Update sales order
     *
     * @param salesOrderDTO the entity to save
     * @return the persisted id
     */
    Boolean update(SalesOrderDTO salesOrderDTO);

    /**
     * Get all sales order
     *
     * @return list of entities
     */
    Map<String, Object> getAll(Integer page, Integer limit, String sortBy, String direction, String pqNumber);

    /**
     * Get specific sales order
     *
     * @param id the id of entity
     * @return the entity
     */
    Optional<SalesOrderDetailDTO> getSalesOrder(Long id);

    /**
     * Delete specific sales order
     *
     * @param id the id of entity
     */
    Boolean delete(Long id);

}
