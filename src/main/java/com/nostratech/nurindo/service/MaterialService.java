package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.MaterialDTO;
import com.nostratech.nurindo.service.dto.MenuDTO;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface MaterialService {
    /**
     * Save a material
     *
     * @param materialDTO the entity to save
     * @return boolean
     */
    Boolean save(MaterialDTO materialDTO);

    /**
     * Get all of materials
     *
     * @return list of entities
     */
    List<MaterialDTO> findAll();

    /**
     * Get specific material
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<MaterialDTO> findOne(Long id);

    /**
     * Delete specific material
     *
     * @param id the id of entity
     */
    void delete(Long id);

}
