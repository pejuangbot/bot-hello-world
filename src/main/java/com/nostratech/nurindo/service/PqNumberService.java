package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.PqNumberDTO;
import com.nostratech.nurindo.service.dto.PqNumberRecordDTO;

import java.util.Map;
import java.util.Optional;

public interface PqNumberService {

    Optional<PqNumberDTO> getPqNumber(Long customerId, String actionType);

    String generatePriceQuotationNumber(Long customerId, String actionType);

    Boolean updateNumber(PqNumberRecordDTO pqNumberRecordDTO);

    Boolean deleteNumber(Long id);

    Map<String, Object> getAllNumber(Integer page, Integer limit, String sortBy, String direction, String numberType, String bulan);
}
