package com.nostratech.nurindo.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface EntityService<D> {
    /**
     * Save a role
     *
     * @param DTO the entity to save
     * @return the persisted id
     */
    D save(D dto);

    /**
     * Get all of entity
     *
     * @return list of entities
     */
    List<D> findAll();

    /**
     * Get specific entity
     *
     * @param id the id of entity
     * @return the entity
     */
    Optional<D> findOne(Long id);

    /**
     * Delete specific entity
     *
     * @param id the id of entity
     */
    void delete(Long id);

    /**
     * Get entities by page
     *
     * @param pageable
     * @return list of entities
     */
    Page<D> findAll(Pageable pageable);
}
