package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.EmailDTO;

public interface EmailService{
    /**
     * Save a role
     *
     * @param emailDTO the entity to save
     * @return the persisted id
     */
    Boolean sendEmailConfirmPassword(EmailDTO emailDTO);

}
