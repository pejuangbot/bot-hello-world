package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.FileUploadDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

public interface FileAttachService {
    Optional<FileUploadDTO> uploadFile(MultipartFile file, String fileType);
}
