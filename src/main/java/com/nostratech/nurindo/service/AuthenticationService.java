package com.nostratech.nurindo.service;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.repository.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

/**
 * 
 * @author Tommy
 *
 */
@Service(value = "userDetailsService")
public class AuthenticationService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String input) {
		User user = null;

		if (input.contains("@"))
			user = userRepository.findOneByEmailIgnoreCase(input)
					.orElseThrow(() -> new BadCredentialsException("Bad credentials"));
		else
			user = userRepository.findOneByUsername(input)
					.orElseThrow(() -> new BadCredentialsException("Bad credentials"));

		new AccountStatusUserDetailsChecker().check(user);

		return user;
	}

}
