package com.nostratech.nurindo.service;

import com.amazonaws.services.dynamodbv2.xspec.L;
import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.service.dto.*;

import java.util.Map;
import java.util.Optional;


public interface PriceQuotationService {

    /**
     * Save a price quotation
     *
     * @param priceQuotationDTO the entity to save
     * @return the persisted id
     */
    PqNumberDTO create(PriceQuotationDTO priceQuotationDTO);

    /**
     * Update a price quotation
     *
     * @param priceQuotationDTO the entity to save
     * @return the persisted id
     */
    PqNumberDTO update(PriceQuotationDTO priceQuotationDTO);

    /**
     * Get all of price quotation
     *
     * @return list of entities
     */
    Map<String, Object> getAll(Integer page, Integer limit, String sortBy, String direction, String pqNumber, Long roleId, Long customerId);

    /**
     * Get specific price quotation
     *
     * @param id the id of entity
     * @return the entity
     */
    Optional<PriceQuotationDetailDTO> getPriceQuotation(Long id);

    /**
     * Delete specific price quotation
     *
     * @param id the id of entity
     */
    Boolean delete(Long id);

    /**
     * Update status price quotation
     *
     * @param id the id of entity
     */
    Boolean statusUpdate(Long id, StatusPQDTO statusPQDTO);

}
