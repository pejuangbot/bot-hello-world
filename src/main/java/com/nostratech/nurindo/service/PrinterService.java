package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.MaterialDTO;
import com.nostratech.nurindo.service.dto.PrinterDTO;

import java.util.List;
import java.util.Optional;

public interface PrinterService {
    /**
     * Save a material
     *
     * @param printerDTO the entity to save
     * @return boolean
     */
    Boolean save(PrinterDTO printerDTO);

    /**
     * Get all of printers
     *
     * @return list of entities
     */
    List<PrinterDTO> findAll();

    /**
     * Get specific printer
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PrinterDTO> findOne(Long id);

    /**
     * Delete specific printer
     *
     * @param id the id of entity
     */
    void delete(Long id);

}
