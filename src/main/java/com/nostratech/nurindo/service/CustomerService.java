package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.*;

import java.util.Map;
import java.util.Optional;

public interface CustomerService {

    /**
     * Save a customer
     *
     * @param customerDTO the entity to save
     * @return the persisted id
     */
    Boolean createCustomer(CustomerDTO customerDTO);

    Boolean updateCustomer(CustomerDTO customerDTO);

    /**
     * Get specific customer
     *
     * @param customerId the id of entity
     * @return list of entities
     */

    Optional<CustomerListDTO> getCustomersById(Long customerId);

    Map<String, Object> getDetailPriceByCustomerId(Integer page, Integer limit, String sortBy, String direction, Long customerId, Long roleId, String productName, String productType);

//    Map<String, Object> getAllCustomer(Integer page, Integer limit, String sortBy, String direction);

    Map<String, Object> getAllCustomerByName(Integer page, Integer limit, String sortBy, String direction, String customerName, Long roleId);
}
