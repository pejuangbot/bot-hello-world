package com.nostratech.nurindo.service;

import com.nostratech.nurindo.service.dto.RoleDTO;
import com.nostratech.nurindo.service.dto.RoleListDTO;
import com.nostratech.nurindo.service.dto.StatusDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    /**
     * Save a role
     *
     * @param roleDTO the entity to save
     * @return the persisted id
     */
    Boolean save(RoleDTO roleDTO);

    /**
     * Get all of roles
     *
     * @return list of entities
     */
    List<RoleListDTO> findAll(String roleName);

    /**
     * Get specific role
     *
     * @param id the id of entity
     * @return the entity
     */
    Optional<RoleDTO> findOne(Long id);

    /**
     * Delete specific role
     *
     * @param id the id of entity
     */
    void delete(Long id);

    /**
     * Get roles by page
     *
     * @param pageable
     * @return list of entities
     */
    Page<RoleDTO> findAll(Pageable pageable);

    /**
     * Save a status role
     *
     * @param id the id of role
     * @param statusDTO the entity to save
     * @return boolean
     */
    Boolean changeStatus(Long id, StatusDTO statusDTO);
}
