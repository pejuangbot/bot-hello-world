package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Material;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.MaterialRepository;
import com.nostratech.nurindo.service.MaterialService;
import com.nostratech.nurindo.service.dto.MaterialDTO;
import com.nostratech.nurindo.service.mapper.MaterialMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class MaterialServiceImpl implements MaterialService {

    @Autowired
    private MaterialRepository materialRepository;

    /**
     * Save a menu
     *
     * @param materialDTO the entity to save
     * @return boolean true
     */
    @Override
    public Boolean save(MaterialDTO materialDTO) {
        log.debug("Request to save material : {}", materialDTO);
        Material material = new Material();
        if(null != materialDTO.getId()) {
            material = materialRepository.findById(materialDTO.getId())
                    .orElseThrow(() -> new BadRequestAlertException("Material not found", "material", "idnull"));
        }
        material = MaterialMapper.INSTANCE.toEntity(materialDTO, material);
        materialRepository.save(material);
        return Boolean.TRUE;
    }

    /**
     * Get all of Material
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<MaterialDTO> findAll() {
        log.debug("Request to get all material");
        return materialRepository.findAll().stream()
                .map(material -> {
                    MaterialDTO dto = new MaterialDTO();
                    dto = MaterialMapper.INSTANCE.toDto(material, dto);
                    return dto;
                })
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get specific menu
     *
     * @param id the id of entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialDTO> findOne(Long id) {
        log.debug("Request get material : {}", id);
        return materialRepository.findById(id)
                .map(material -> {
                    MaterialDTO dto = new MaterialDTO();
                    dto = MaterialMapper.INSTANCE.toDto(material, dto);
                    return dto;
                });
    }

    /**
     * Delete specific material
     *
     * @param id the id of entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Menu : {}", id);
        materialRepository.deleteById(id);
    }
}
