package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.*;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.*;
import com.nostratech.nurindo.service.CustomerContactPersonService;
import com.nostratech.nurindo.service.CustomerService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.service.mapper.*;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.ErrorConstants;
import com.nostratech.nurindo.util.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService{

    ModelMapper modelMapper = new ModelMapper();

    @Autowired
    private CustomerContactPersonRepository customerContactPersonRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private CustomerProductsRepository customerProductsRepository;

    @Autowired
    private CustomerContactPersonService customerContactPersonService;

    @Autowired
    private CustomerRepository customerRepository;

    @Transactional
    @Override
    public Boolean createCustomer(CustomerDTO customerDTO) {
        List<CustomerContactPerson> customerContactPersonList = new ArrayList<>();
        Customer customer = CustomerMapper.INSTANCE.toEntity(customerDTO);
        customerRepository.save(customer);
        Customer customerId = customerRepository.findById(customer.getId())
                .orElseThrow(()-> new BadRequestAlertException("customer id not found", "customer", "idnull"));

        customerDTO.getCustomerContactPersonDTOS().stream()
                .forEach(customerContactPersonDTO -> {
                    customerContactPersonService.createCustomerContactPerson(customerContactPersonDTO, customerId);
                });

        customerDTO.getCustomerProductsDTOS().stream()
                .forEach(customerProductsDTO -> {
                    CustomerProducts customerProducts = CustomerProductsMapper.INSTANCE.toEntity(customerProductsDTO);
                    Product product = productRepository.findById(customerProductsDTO.getProductId())
                            .orElseThrow(()-> new BadRequestAlertException("Product id not found", "product", "idnull"));
                    customerProducts.setCustomer(customerId);
                    customerProducts.setProduct(product);
                    customerProducts.setAgreedPrice(customerProductsDTO.getAgreedPrice());
                    customerProductsRepository.save(customerProducts);
                });
        return Boolean.TRUE;
    }


    @Transactional
    @Override
    public Boolean updateCustomer(CustomerDTO customerDTO) {
        Customer customer  = customerRepository.findById(customerDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "customer not found!", "customerManagement", "customerexists"));
        modelMapper.map(customerDTO,customer);

        customerDTO.getCustomerContactPersonDTOS().stream()
                .forEach(customerContactPersonDTO -> {
                    if (customerContactPersonDTO.getId()==null){
                        customerContactPersonService.createCustomerContactPerson(customerContactPersonDTO, customer);
                    }
                    else {
                        customerContactPersonService.updateCustomerContactPerson(customerContactPersonDTO, customer.getId());
                    }
                });

        customerDTO.getCustomerProductsDTOS().stream()
                .forEach(customerProductsDTO -> {
                    if(customerProductsDTO.getId()==null){
                        CustomerProducts customerProducts = CustomerProductsMapper.INSTANCE.toEntity(customerProductsDTO);
                        Product product = productRepository.findById(customerProductsDTO.getProductId())
                                .orElseThrow(()-> new BadRequestAlertException("Product id not found", "product", "idnull"));
                        customerProducts.setCustomer(customer);
                        customerProducts.setProduct(product);
                        customerProducts.setAgreedPrice(customerProductsDTO.getAgreedPrice());
                        customerProductsRepository.save(customerProducts);
                    }else{
                        CustomerProducts customerProducts = customerProductsRepository.findById(customerProductsDTO.getId())
                                .orElseThrow(()-> new BadRequestAlertException("Customer Products id not found", "product", "idnull"));
                        customerProducts.setProduct(productRepository.findById(customerProductsDTO.getProductId())
                                .orElseThrow(() -> new BadRequestAlertException("Product id not found", "product", "idnull")));
                        customerProducts.setCustomer(customer);
                        customerProducts.setAgreedPrice(customerProductsDTO.getAgreedPrice());
                        customerProductsRepository.save(customerProducts);
                    }

                });
        return Boolean.TRUE;
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<CustomerListDTO> getCustomersById(Long customerId) {
        Optional<CustomerListDTO> customerListDTO = customerRepository.findById(customerId)
//        Optional<CustomerListDTO> customerListDTO = optionalCustomer
                .map(customer -> {
                    CustomerListDTO customerDTO = CustomerListMapper.INSTANCE.toDto(customer);
                    String cityName="", npwpCityName="";
                    if (!StringUtils.isEmpty(customer.getCityCode())){
                        Optional<City> city = cityRepository.findByCityCodeIgnoreCase(customer.getCityCode());
                        if (city.isPresent()){
                            cityName = city.get().getCityName();
                        }
                    }
                    if(!StringUtils.isEmpty(customer.getNpwpCityCode())){
                        Optional<City> npwpCityNames = cityRepository.findByCityCodeIgnoreCase(customer.getNpwpCityCode());
                        if (npwpCityNames.isPresent()){
                            npwpCityName = npwpCityNames.get().getCityName();
                        }
                    }
                    List<CustomerContactPersonDTO> customerContactPersonDTOS = customerContactPersonRepository.findByCustomerId(customer.getId()).stream()
                            .map(customerContactPerson -> {
                                CustomerContactPersonDTO customerContactPersonDTO = CustomerContactPersonMapper.INSTANCE.toDto(customerContactPerson);
                                customerContactPersonDTO.setCustomerId(customerDTO.getId());
                                return customerContactPersonDTO;
                            }).collect(Collectors.toCollection(LinkedList::new));

                    List<CustomerProductsListDTO> customerProductsDTOS = customerProductsRepository.findByCustomerId(customer.getId()).stream()
                            .map(customerProducts -> {
                                CustomerProductsListDTO customerProductsDTO = CustomerProductsListMapper.INSTANCE.toDto(customerProducts);
                                customerProductsDTO.setProductId(customerProducts.getProduct().getId());
                                customerProductsDTO.setProductName(customerProducts.getProduct().getProduct());
                                customerProductsDTO.setAgreedPrice(customerProducts.getAgreedPrice());
                                if(customerProducts.getProduct().getMaterials() != null) customerProductsDTO.setListOfMaterial(MaterialMapper.INSTANCE.toDto(customerProducts.getProduct().getMaterials(),new ArrayList<MaterialDTO>()));
                                if(customerProducts.getProduct().getPrinters() != null)customerProductsDTO.setListOfPrinter(PrinterMapper.INSTANCE.toDto(customerProducts.getProduct().getPrinters(),new ArrayList<PrinterDTO>()));
                                return customerProductsDTO;
                            }).collect(Collectors.toCollection(LinkedList::new));

                    customerDTO.setCityName(cityName);
                    customerDTO.setNpwpCityName(npwpCityName);
                    customerDTO.setCustomerContactPersonDTOS(customerContactPersonDTOS);
                    customerDTO.setCustomerProductsListDTOList(customerProductsDTOS);

                    return customerDTO;
                });
        return customerListDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> getDetailPriceByCustomerId(Integer page, Integer limit, String sortBy, String direction, Long customerId, Long roleId, String productName, String productType) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        productName = StringUtils.isBlank(productName) ? "%" : "%" + productName + "%";
        productType = StringUtils.isBlank(productType) ? "%" : "%" + productType + "%";
        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);


        Page<CustomerProducts> customerProductsList = customerProductsRepository.findByCustomerIdAndProductProductLikeIgnoreCaseAndProductTypeLikeIgnoreCase(customerId, productName, productType, pageable);
        Collection<CustomerProductRoleDTO> vos = customerProductsList.getContent().stream()
                .map(customerProducts -> {
                    CustomerProductRoleDTO customerProductRoleDTO = CustomerProductRoleMapper.INSTANCE.toDto(customerProducts);
                    Role role = roleRepository.findById(roleId)
                            .orElseThrow(()-> new BadRequestAlertException("Role id not found", "role", "idnull"));

                    customerProductRoleDTO.setCustomerId(customerId);
                    customerProductRoleDTO.setProductId(customerProducts.getProduct().getId());
                    customerProductRoleDTO.setCustomerProductId(customerProducts.getId());
                    customerProductRoleDTO.setProductName(customerProducts.getProduct().getProduct());
                    customerProductRoleDTO.setProductHeight(customerProducts.getProduct().getHeight());
                    customerProductRoleDTO.setProductWidth(customerProducts.getProduct().getWidth());
                    customerProductRoleDTO.setMsrp(customerProducts.getProduct().getMsrp());
                    customerProductRoleDTO.setProductType(customerProducts.getProduct().getType());
                    customerProductRoleDTO.setAgreedPrice(customerProducts.getAgreedPrice());
                    customerProductRoleDTO.setUnitMeasure(customerProducts.getProduct().getUnitMeasure());
                    if (role.getName().equalsIgnoreCase(Constants.Role.ACCOUNT_EXCUTIVE) || role.getName().equalsIgnoreCase(Constants.Role.SALES_ADMIN)){
                        customerProductRoleDTO.setBottomPrice(customerProducts.getProduct().getBottomPriceAE());
                    } else if (role.getName().equalsIgnoreCase(Constants.Role.SALES_MANAGER)){
                        customerProductRoleDTO.setBottomPrice(customerProducts.getProduct().getBottomPriceSA());
                    } else if (role.getName().equalsIgnoreCase(Constants.Role.SUPERVISOR_ADMINISTRATOR)){
                        customerProductRoleDTO.setBottomPrice(customerProducts.getProduct().getBottomPriceSA());
                    }
                    if(customerProducts.getProduct().getMaterials() != null) customerProductRoleDTO.setListOfMaterial(MaterialMapper.INSTANCE.toDto(customerProducts.getProduct().getMaterials(),new ArrayList<MaterialDTO>()));
                    if(customerProducts.getProduct().getPrinters() != null) customerProductRoleDTO.setListOfPrinter(PrinterMapper.INSTANCE.toDto(customerProducts.getProduct().getPrinters(),new ArrayList<PrinterDTO>()));
                    return  customerProductRoleDTO;
                }).collect(Collectors.toCollection(LinkedList::new));

        return PaginationUtil.constructMapReturn(vos, customerProductsList.getTotalElements(), customerProductsList.getTotalPages());
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> getAllCustomerByName(Integer page, Integer limit, String sortBy, String direction, String customerName, Long roleId) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        customerName = StringUtils.isBlank(customerName) ? "%" : "%" + customerName + "%";

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);
        Page<Customer> customerList = null;
        Role role = roleRepository.findById(roleId)
                .orElseThrow(()-> new BadRequestAlertException("Role id not found", "role", "idnull"));
        if (role.getName().equalsIgnoreCase("Account Executive")){
            customerList = customerRepository.findByCustomerNameLikeIgnoreCaseAndCustomerTypeIgnoreCase(customerName, "Account Executive", pageable);
        }else{
            customerList = customerRepository.findByCustomerNameLikeIgnoreCase(customerName, pageable);
        }
//        Page<Customer> customerList = customerRepository.findByCustomerNameLikeIgnoreCase(customerName, pageable);
        Collection<CustomerListDTO> vos = customerList.getContent().stream()
                .map(customer -> {
                    String cityName="", npwpCityName="";
                    if (!StringUtils.isEmpty(customer.getCityCode())){
                        Optional<City> city = cityRepository.findByCityCodeIgnoreCase(customer.getCityCode());
                        if (city.isPresent()) cityName = city.get().getCityName();
                    }
                    if(!StringUtils.isEmpty(customer.getNpwpCityCode())){
                        Optional<City> npwpCityNames = cityRepository.findByCityCodeIgnoreCase(customer.getNpwpCityCode());
                        if(npwpCityNames.isPresent()) npwpCityName = npwpCityNames.get().getCityName();
                    }

                    CustomerListDTO dto = CustomerListMapper.INSTANCE.toDto(customer);
                    dto.setCityName(cityName);
                    dto.setNpwpCityName(npwpCityName);
                    return dto;
                }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, customerList.getTotalElements(), customerList.getTotalPages());
    }


}
