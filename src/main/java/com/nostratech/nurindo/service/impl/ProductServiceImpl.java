package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.*;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.*;
import com.nostratech.nurindo.service.ProductService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.service.mapper.*;
import com.nostratech.nurindo.service.validator.ProductValidator;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductRoleRepository productRoleRepository;

    @Autowired
    ProductValidator productValidator;

    @Autowired
    PrinterRepository printerRepository;

    @Autowired
    MaterialRepository materialRepository;

    @Autowired
    CustomerProductsRepository customerProductsRepository;

    /**
     * Create product with all information, and return the product.
     *
     * @param productDTO product to create
     * @return boolean true
     *
     */
    @Transactional
    @Override
    public Boolean createProduct(ProductDTO productDTO) {
        productValidator.createValidation(productDTO);

        Product product = new Product();
        product = ProductMapper.INSTANCE.toEntity(productDTO, product);

        if(productDTO.getType().equalsIgnoreCase(Constants.ProductType.PRODUCT)){
            product.setPrinters(productDTO.getListOfPrinter().stream().map(printerDTO -> {
                Printer printer = printerRepository.findById(printerDTO.getId())
                        .orElseThrow(()->new BadRequestAlertException("Printer not found!", "productManagement", "idnull"));
                return printer;
            }).collect(Collectors.toCollection(LinkedList::new)));
            product.setMaterials(productDTO.getListOfMaterial().stream().map(materialDTO -> {
                Material material = materialRepository.findById(materialDTO.getId())
                        .orElseThrow(()->new BadRequestAlertException("Material not found!", "productManagement", "idnull"));
                return material;
            }).collect(Collectors.toCollection(LinkedList::new)));
        }

        productRepository.save(product);

        return Boolean.TRUE;
    }

    /**
     * Update all information for a specific product, and return the modified product.
     *
     * @param productDTO product to update
     * @return boolean true
     */
    @Transactional
    @Override
    public Boolean updateProduct(ProductDTO productDTO) {
        Product product = productRepository.findById(productDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException("Product not found!", "productManagement", "idnull"));
        productValidator.updateValidation(productDTO, product);

        syncAgreedPrice(productDTO, product);

        product = ProductMapper.INSTANCE.toEntity(productDTO, product);

        if(productDTO.getType().equalsIgnoreCase(Constants.ProductType.PRODUCT)){
            product.setPrinters(productDTO.getListOfPrinter().stream().map(printerDTO -> {
                Printer printer = printerRepository.findById(printerDTO.getId())
                        .orElseThrow(()->new BadRequestAlertException("Printer not found!", "productManagement", "idnull"));
                return printer;
            }).collect(Collectors.toCollection(LinkedList::new)));
            product.setMaterials(productDTO.getListOfMaterial().stream().map(materialDTO -> {
                Material material = materialRepository.findById(materialDTO.getId())
                        .orElseThrow(()->new BadRequestAlertException("Material not found!", "productManagement", "idnull"));
                return material;
            }).collect(Collectors.toCollection(LinkedList::new)));
        }
        productRepository.save(product);

        return Boolean.TRUE;
    }

    /**
     * Get all products.
     *
     * @return all products
     */
    @Transactional
    @Override
    public Map<String, Object> getAllProduct(Integer page, Integer limit, String sortBy, String direction, String productCode, String productName) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        productCode = StringUtils.isBlank(productCode) ? "%" : "%" + productCode + "%";
        productName = StringUtils.isBlank(productName) ? "%" : "%" + productName + "%";

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);
        Page<Product> resultPage = productRepository.findByProductCodeLikeIgnoreCaseAndProductLikeIgnoreCase(productCode, productName, pageable);

        Collection<ProductListDTO> vos = resultPage.getContent().stream().map(product -> {
            ProductListDTO dto = new ProductListDTO();
            dto = ProductListMapper.INSTANCE.toDto(product, dto);
            return dto;
        }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    /**
     * GET /products/:id : get the "id" product.
     *
     * @param id the id of the product to find
     * @return the "id" product or null if not found
     */
    @Override
    public Optional<ProductDetailDTO> getProduct(Long id) {
        return productRepository.findById(id).map(product -> {
            ProductDetailDTO dto = new ProductDetailDTO();
            dto = ProductDetailMapper.INSTANCE.toDto(product, dto);
            dto.setListOfPrinter(PrinterMapper.INSTANCE.toDto(product.getPrinters(),new ArrayList<PrinterDTO>()));
            dto.setListOfMaterial(MaterialMapper.INSTANCE.toDto(product.getMaterials(),new ArrayList<MaterialDTO>()));
            return dto;
        });
    }

    /**
     * Delete product base on id.
     *
     * @param id product to delete
     */
    @Transactional
    @Override
    public void deleteProduct(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(()-> new BadRequestAlertException("Product not found!", "productManagement", "idnull"));
        productRepository.delete(product);
    }

    public void syncAgreedPrice(ProductDTO productDTO, Product product){
        List<CustomerProducts> customerProductsList = customerProductsRepository.findByProduct(product);
        for(CustomerProducts customerProducts : customerProductsList){
            customerProducts.setAgreedPrice(productDTO.getMsrp().divide(product.getMsrp()).multiply(customerProducts.getAgreedPrice()));
            customerProductsRepository.saveAndFlush(customerProducts);
        }
    }

}
