package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.PriceQuotationDetailTrx;
import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.PriceQuotationDetailTrxRepository;
import com.nostratech.nurindo.repository.PriceQuotationRepository;
import com.nostratech.nurindo.repository.ProductRepository;
import com.nostratech.nurindo.repository.UserRepository;
import com.nostratech.nurindo.service.ReportService;
import com.nostratech.nurindo.service.dto.ReportPriceQuotationDTO;
import com.nostratech.nurindo.util.ErrorConstants;
import com.nostratech.nurindo.util.PdfUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReportSeviceImpl implements ReportService{

    @Autowired
    PriceQuotationRepository priceQuotationRepository;

    @Autowired
    PriceQuotationDetailTrxRepository priceQuotationDetailTrxRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PdfUtil pdfUtil;

    final private String PRICE_QUOTATION_REPORT_TEMPLATE = "PriceQuotationReport.jasper";
    final private String PRICE_QUOTATION_REPORT_TITLE = "PENAWARAN HARGA";

    public Map<String, Object> exportReportPriceQuotation(String documentType, Long idPq){
        String methodName = "exportReportPriceQuotation";
        log.info("BEGIN:{}", methodName);

        PriceQuotation priceQuotations = priceQuotationRepository.findById(idPq)
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Price Quotation not found!", "priceQuotation", "priceQuotationexists"));

        HashMap title = new HashMap<>();
        title.put("report_title",PRICE_QUOTATION_REPORT_TITLE);
        title.put("pq_number", priceQuotations.getPqNumber());
        title.put("customer_name", priceQuotations.getCustomerName());
        title.put("cust_addr", priceQuotations.getCustomer().getAddress());
        title.put("cust_city", priceQuotations.getCustomer().getAddressDetail());
        title.put("phone_number", priceQuotations.getPhoneNumber());
        title.put("fax_number", priceQuotations.getFaxNumber());
        title.put("atm", priceQuotations.getAttn());
        title.put("cc", priceQuotations.getCc());
        title.put("po", priceQuotations.getPo());
        title.put("pr_number", priceQuotations.getPrNumber());
        title.put("project_name", priceQuotations.getProjectName());
        title.put("allSubTotal", priceQuotations.getSubTotal());
        title.put("taxType", priceQuotations.getTax());
        title.put("taxPrice", priceQuotations.getTaxPrice());
        title.put("totalCount", priceQuotations.getSubTotal().add(priceQuotations.getTaxPrice()));
        title.put("salesPersonName", priceQuotations.getAccountExecutive().getFirstName());
        List<ReportPriceQuotationDTO> reportPriceQuotationDTOS = priceQuotationDetailTrxRepository.findByPriceQuotation(priceQuotations).stream()
                .map(priceQuotationDetailTrx -> {

                    ReportPriceQuotationDTO priceQuotationDTO = new ReportPriceQuotationDTO();
                    log.info("METERAN : " + priceQuotationDetailTrx.getHeight());
//                    String meteran = priceQuotationDetailTrx.getHeight().toString();
                    if (priceQuotationDetailTrx.getHeight()!=null){
                        priceQuotationDTO.setName(priceQuotationDetailTrx.getName());
                        Integer Luas = (priceQuotationDetailTrx.getHeight()/100) * (priceQuotationDetailTrx.getWeight()/100);
                        priceQuotationDTO.setSize(priceQuotationDetailTrx.getHeight()/100 +" x "+ priceQuotationDetailTrx.getWeight()/100);
                        priceQuotationDTO.setMeasure("meteran");
                        priceQuotationDTO.setHargaMeter(priceQuotationDetailTrx.getPrice());
                        priceQuotationDTO.setPrice(new BigDecimal(Luas).multiply(priceQuotationDetailTrx.getPrice()));
                        priceQuotationDTO.setQuantity(priceQuotationDetailTrx.getQuantity());
                        priceQuotationDTO.setTotalLuas(Luas);
                        BigDecimal hargaUnit = new BigDecimal(Luas).multiply(priceQuotationDetailTrx.getPrice());
                        priceQuotationDTO.setSubTotal(hargaUnit.multiply(new BigDecimal(priceQuotationDetailTrx.getQuantity())));
                    }else {
                        priceQuotationDTO.setName(priceQuotationDetailTrx.getName());
                        priceQuotationDTO.setSize("-");
                        priceQuotationDTO.setMeasure("satuan");
                        priceQuotationDTO.setHargaMeter(new BigDecimal(0));
                        priceQuotationDTO.setPrice(priceQuotationDetailTrx.getPrice());
                        priceQuotationDTO.setQuantity(priceQuotationDetailTrx.getQuantity());
                        priceQuotationDTO.setTotalLuas(0);
                        priceQuotationDTO.setSubTotal(priceQuotationDetailTrx.getPrice().multiply(new BigDecimal(priceQuotationDetailTrx.getQuantity())));
                    }

                    return priceQuotationDTO;
                }).collect(Collectors.toCollection(LinkedList::new));
        List<Object> detail = new ArrayList<>(reportPriceQuotationDTOS);



        File file;
        if (documentType.equalsIgnoreCase("pdf")) {
            String fileName = Instant.now() + "PRICE_QUOTATION_REPORT_TEMPLATE" + ".pdf";
            file = new File(pdfUtil.generatePdfFileJasperFromListObject(title, detail,
                    fileName, PRICE_QUOTATION_REPORT_TEMPLATE));

        } else {
            throw new BadRequestAlertException("Filetype " , documentType , " not supported.");
        }

        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (Exception e) {
                throw new BadRequestAlertException("fail", documentType ,"not Supported");
            }
        }

        String contentType = "application/octet-stream";
        Map<String, Object> resultMap = new HashedMap();
        resultMap.put("file", file);
        resultMap.put("contentType", contentType);

        log.info("DONE:{}", methodName);
        return resultMap;
    }
}
