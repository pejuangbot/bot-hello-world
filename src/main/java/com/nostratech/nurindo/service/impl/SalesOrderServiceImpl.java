package com.nostratech.nurindo.service.impl;


import com.nostratech.nurindo.domain.*;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.*;
import com.nostratech.nurindo.service.SalesOrderService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.service.mapper.*;
import com.nostratech.nurindo.service.validator.SalesOrderValidator;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class SalesOrderServiceImpl implements SalesOrderService {

    @Autowired
    SalesOrderRepository salesOrderRepository;

    @Autowired
    SalesOrderValidator salesOrderValidator;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    FileAttachRepository fileAttachRepository;

    @Autowired
    PriceQuotationRepository priceQuotationRepository;

    @Autowired
    PriceQuotationDetailTrxRepository priceQuotationDetailTrxRepository;

    @Override
    public Boolean create(SalesOrderDTO salesOrderDTO) {
        PriceQuotation priceQuotation = priceQuotationRepository.findById(salesOrderDTO.getPriceQuotation())
                .orElseThrow(()-> new BadRequestAlertException("Price quotation not found", "priceQuotation", "idnull"));
        salesOrderValidator.salesOrderValidation(salesOrderDTO, priceQuotation);
        priceQuotation.setStatus(Constants.PriceQuotationStatus.SO_CREATED);
        PriceQuotation saved = priceQuotationRepository.saveAndFlush(priceQuotation);

        SalesOrder salesOrder = new SalesOrder();
        salesOrder = SalesOrderMapper.INSTANCE.toEntity(salesOrderDTO, salesOrder);
        salesOrder.setPriceQuotation(saved);
        salesOrder.setId(null);
        salesOrderRepository.save(salesOrder);

        return Boolean.TRUE;
    }

    @Override
    public Boolean update(SalesOrderDTO salesOrderDTO) {
        salesOrderValidator.statusValidation(salesOrderDTO);

        PriceQuotation priceQuotation = priceQuotationRepository.findById(salesOrderDTO.getPriceQuotation())
                .orElseThrow(()-> new BadRequestAlertException("Price quotation not found", "priceQuotation", "idnull"));

        SalesOrder salesOrder = salesOrderRepository.findById(salesOrderDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException("Sales order not found", "priceQuotation", "idnull"));
        salesOrder = SalesOrderMapper.INSTANCE.toEntity(salesOrderDTO, salesOrder);
        salesOrder.setPriceQuotation(priceQuotation);
        salesOrderRepository.save(salesOrder);

        return Boolean.TRUE;
    }

    @Override
    public Map<String, Object> getAll(Integer page, Integer limit, String sortBy, String direction, String pqNumber) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        pqNumber = StringUtils.isBlank(pqNumber) ? "%" : "%" + pqNumber + "%";

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);

        Page<SalesOrder> resultPage = salesOrderRepository.findByPriceQuotationPqNumberLikeAndStatusNot(pqNumber, Constants.SalesOrderStatus.DELETED, pageable);
        Collection<SalesOrderListDTO> vos = resultPage.getContent().stream().map(entity -> {
            SalesOrderListDTO dto = new SalesOrderListDTO();
            dto = SalesOrderListMapper.INSTANCE.toDto(entity.getPriceQuotation(), dto);
            dto = SalesOrderMapper.INSTANCE.toDto(entity, dto);
            return dto;
        }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    @Override
    public Optional<SalesOrderDetailDTO> getSalesOrder(Long id) {
        Optional<SalesOrderDetailDTO> detailDTO = salesOrderRepository.findById(id).map(salesOrder -> {
            SalesOrderDetailDTO dto = new SalesOrderDetailDTO();
            dto = SalesOrderDetailMapper.INSTANCE.toDto(salesOrder.getPriceQuotation(), dto);
            dto.setSalesPerson(UserMapper.INSTANCE.toDto(salesOrder.getPriceQuotation().getAccountExecutive()));
            dto.setSalesAdmin(UserMapper.INSTANCE.toDto(salesOrder.getPriceQuotation().getSalesAdmin()));
            dto.setCustomer(CustomerListMapper.INSTANCE.toDto(salesOrder.getPriceQuotation().getCustomer()));
            dto = SalesOrderMapper.INSTANCE.toDto(salesOrder, dto);
            dto.setPriceQuotation(salesOrder.getPriceQuotation().getId());
            dto.setDetailSalesOrder(priceQuotationDetailTrxRepository.findByPriceQuotation(salesOrder.getPriceQuotation())
                    .stream()
                    .map(priceQuotationDetailTrx -> {
                        PriceQuotationDetailTrxListDTO listDTO = PriceQuotationDetailTrxListMapper.INSTANCE.toDto(priceQuotationDetailTrx);
                        if(priceQuotationDetailTrx.getMaterial() != null) listDTO.setMaterial(MaterialMapper.INSTANCE.toDto(priceQuotationDetailTrx.getMaterial(), new MaterialDTO()));
                        if(priceQuotationDetailTrx.getPrinter() != null) listDTO.setPrinter(PrinterMapper.INSTANCE.toDto(priceQuotationDetailTrx.getPrinter(), new PrinterDTO()));
                        return listDTO;
                    }).collect(Collectors.toCollection(LinkedList::new)));dto.setListOfAttach(FileUploadMapper.INSTANCE.toDto(salesOrder.getPriceQuotation().getFileAttaches()));
            return dto;
        });
        return detailDTO;
    }

    @Override
    public Boolean delete(Long id) {
        SalesOrder salesOrder = salesOrderRepository.findById(id)
                .orElseThrow(() -> new BadRequestAlertException("Sales order not found", "salesOrderManagement", "idnull"));
        salesOrder.setStatus(Constants.SalesOrderStatus.DELETED);
        salesOrderRepository.save(salesOrder);
        return Boolean.TRUE;
    }

}
