package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.domain.Privilege;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.MenuRepository;
import com.nostratech.nurindo.repository.PrivilegeRepository;
import com.nostratech.nurindo.repository.RoleRepository;
import com.nostratech.nurindo.service.RoleService;
import com.nostratech.nurindo.service.dto.MenuListDTO;
import com.nostratech.nurindo.service.dto.RoleDTO;
import com.nostratech.nurindo.service.dto.RoleListDTO;
import com.nostratech.nurindo.service.dto.StatusDTO;
import com.nostratech.nurindo.service.mapper.MenuListMapper;
import com.nostratech.nurindo.service.mapper.PrivilegeMapper;
import com.nostratech.nurindo.service.mapper.RoleListMapper;
import com.nostratech.nurindo.service.mapper.RoleMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private PrivilegeRepository privilegeRepository;

    /**
     * Save a role
     *
     * @param roleDTO the entity to save
     * @return the boolean
     */
    @Transactional
    @Override
    public Boolean save(RoleDTO roleDTO) {
        List<Privilege> privileges = roleDTO.getPrivilegeList() == null ? null : roleDTO.getPrivilegeList().stream().map(privilegeDTO -> {
                    Menu menu = menuRepository.findById(privilegeDTO.getMenu())
                            .orElseThrow(()-> new BadRequestAlertException("Menu id " + privilegeDTO.getMenu() + " not found", "role", "idnull"));
                    Privilege privilege = PrivilegeMapper.INSTANCE.toEntity(privilegeDTO);
                    privilege.setMenu(menu);
                    privilegeRepository.saveAndFlush(privilege);
                    return privilege;
                }).collect(Collectors.toCollection(LinkedList::new));

        log.debug("Request to save Role : {}", roleDTO);

        Role role = new Role();
        if(null != roleDTO.getId()){
            role = roleRepository.findById(roleDTO.getId())
                    .orElseThrow(() -> new BadRequestAlertException("Role id " + roleDTO.getId() + " not found", "role", "idnull"));
            //delete
            List<Privilege> privilegeList = role.getPrivileges();
            role.setPrivileges(null);
            roleRepository.save(role);
            privilegeList.stream()
                    .forEach(privilege -> privilegeRepository.delete(privilege));
        }
        role.setDescription(roleDTO.getDescription());
        role.setName(roleDTO.getName());
        role.setActivated(roleDTO.getActivated());
        role.setPrivileges(privileges);
        roleRepository.save(role);
        return Boolean.TRUE;
    }

    /**
     * Get all of roles
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<RoleListDTO> findAll(String name) {
        name = StringUtils.isBlank(name) ? "%" : "%" + name + "%";

        log.debug("Request to get all Roles");
        List<RoleListDTO> roleList = roleRepository.findByNameLikeOrderByLastModifiedDateDesc(name).stream()
                .map(role -> {
                    RoleListDTO roleListDTO = RoleListMapper.INSTANCE.toDto(role);
                    List<MenuListDTO> menuListDTOS = role.getPrivileges().stream()
                            .map(privilege -> {
                                MenuListDTO menuListDTO = MenuListMapper.INSTANCE.toDto(privilege.getMenu());
                                menuListDTO.setCreate(privilege.getCreate());
                                menuListDTO.setDelete(privilege.getDelete());
                                menuListDTO.setRead(privilege.getRead());
                                menuListDTO.setUpdate(privilege.getUpdate());
                                return menuListDTO;
                            }).collect(Collectors.toCollection(LinkedList::new));
                    roleListDTO.setMenuList(menuListDTOS);
                    return roleListDTO;
                }).collect(Collectors.toCollection(LinkedList::new));
        return roleList;
    }

    /**
     * Get specific role
     *
     * @param id the id of entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RoleDTO> findOne(Long id) {
        log.debug("Request get Role : {}", id);
        return roleRepository.findById(id)
                .map(RoleMapper.INSTANCE::toDto);
    }

    /**
     * Delete specific role
     *
     * @param id the id of entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Role : {}", id);
        roleRepository.deleteById(id);
    }

    /**
     * Get all of roles by page
     * @param pageable
     * @return
     */
    @Override
    @Transactional(readOnly = true)
    public Page<RoleDTO> findAll(Pageable pageable) {
        log.debug("pageable");
        return roleRepository.findAll(pageable).map(RoleMapper.INSTANCE::toDto);
    }


    /**
     * Change status a role
     *
     * @param id the id of role
     * @param statusDTO the entity to save
     * @return the boolean
     */
    @Override
    public Boolean changeStatus(Long id, StatusDTO statusDTO) {
        Role role = roleRepository.findById(id)
                .orElseThrow(()-> new BadRequestAlertException("Role not found", "role", "idnull"));
        role.setActivated(statusDTO.getActivated());
        roleRepository.save(role);
        return Boolean.TRUE;
    }
}
