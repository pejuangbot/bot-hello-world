package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Printer;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.PrinterRepository;
import com.nostratech.nurindo.service.PrinterService;
import com.nostratech.nurindo.service.dto.PrinterDTO;
import com.nostratech.nurindo.service.mapper.PrinterMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class PrinterServiceImpl implements PrinterService {

    @Autowired
    private PrinterRepository printerRepository;

    /**
     * Save a printer
     *
     * @param printerDTO the entity to save
     * @return boolean true
     */
    @Override
    public Boolean save(PrinterDTO printerDTO) {
        log.debug("Request to save printer : {}", printerDTO);
        Printer printer = new Printer();
        if(null != printerDTO.getId()) {
            printer = printerRepository.findById(printerDTO.getId())
                    .orElseThrow(() -> new BadRequestAlertException("Printer not found", "printer", "idnull"));
        }
        printer = PrinterMapper.INSTANCE.toEntity(printerDTO, printer);
        printerRepository.save(printer);
        return Boolean.TRUE;
    }

    /**
     * Get all of printer
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PrinterDTO> findAll() {
        log.debug("Request to get all printer");
        return printerRepository.findAll().stream()
                .map(printer -> {
                    PrinterDTO dto = new PrinterDTO();
                    dto = PrinterMapper.INSTANCE.toDto(printer, dto);
                    return dto;
                })
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get specific printer
     *
     * @param id the id of entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PrinterDTO> findOne(Long id) {
        log.debug("Request get printer : {}", id);
        return printerRepository.findById(id)
                .map(printer -> {
                    PrinterDTO dto = new PrinterDTO();
                    dto = PrinterMapper.INSTANCE.toDto(printer, dto);
                    return dto;
                });
    }

    /**
     * Delete specific printer
     *
     * @param id the id of entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete printer : {}", id);
        printerRepository.deleteById(id);
    }
}
