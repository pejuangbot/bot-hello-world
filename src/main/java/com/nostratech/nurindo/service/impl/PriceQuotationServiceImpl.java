package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.*;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.*;
import com.nostratech.nurindo.service.PriceQuotationService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.service.mapper.*;
import com.nostratech.nurindo.service.validator.PriceQuotationValidator;
import com.nostratech.nurindo.util.Constants;
import com.nostratech.nurindo.util.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional
public class PriceQuotationServiceImpl implements PriceQuotationService {

    @Autowired
    PriceQuotationRepository priceQuotationRepository;

    @Autowired
    PriceQuotationHistoryRepository priceQuotationHistoryRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PriceQuotationValidator priceQuotationValidator;

    @Autowired
    PriceQuotationDetailTrxRepository priceQuotationDetailTrxRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    FileAttachRepository fileAttachRepository;

    @Autowired
    PqNumberServiceImpl pqNumberService;

    @Autowired
    MaterialRepository materialRepository;

    @Autowired
    PrinterRepository printerRepository;

    @Override
    public PqNumberDTO create(PriceQuotationDTO priceQuotationDTO) {
        priceQuotationValidator.createValidation(priceQuotationDTO);
        //priceQuotationValidator.calculationValidation(priceQuotationDTO);

        PriceQuotation priceQuotation = new PriceQuotation();
        priceQuotation = PriceQuotationMapper.INSTANCE.toEntity(priceQuotationDTO, priceQuotation);
        priceQuotation.setCustomer(
                customerRepository.findById(priceQuotationDTO.getCustomer()).orElseThrow(() -> new BadRequestAlertException("Customer not found", "customerManagement", "idnull"))
        );
        priceQuotation.setSalesAdmin(
                userRepository.findById(priceQuotationDTO.getSalesAdmin()).orElseThrow(() -> new BadRequestAlertException("Sales admin not found", "userManagement", "idnull"))
        );
        priceQuotation.setAccountExecutive(
                userRepository.findById(priceQuotationDTO.getAccountExecutive()).orElseThrow(() -> new BadRequestAlertException("Account Executive not found", "userManagement", "idnull"))
        );

//        String pqNumberDTO = "PQ/DUMMY/2018";//pqNumberService.getPqNumber("others").get();
        String priceQuotationNumber = pqNumberService.generatePriceQuotationNumber(priceQuotationDTO.getCustomer(), "create");
        priceQuotation.setPqNumber(priceQuotationNumber);

        if (priceQuotationDTO.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEED_APPROVAL)){
            Role role = roleRepository.findById(priceQuotationDTO.getRoleId()).orElseThrow(() -> new BadRequestAlertException("Role not found","roleManagement",""));
            if(role.getName().equalsIgnoreCase(Constants.Role.ACCOUNT_EXCUTIVE) || role.getName().equalsIgnoreCase(Constants.Role.SALES_ADMIN)){
                Role approvalRole = roleRepository.findOneByName(Constants.Role.SALES_MANAGER).orElseThrow(() -> new BadRequestAlertException("Role sales maneger not found","roleManagement",""));
                priceQuotation.setApproval(approvalRole);
            } else {
                throw new BadRequestAlertException("This role cannot create price quotation","roleManagement","");
            }
        } else {
            priceQuotation.setApproval(null);
        }

        priceQuotation.setFileAttaches(priceQuotationDTO.getListOfAttach() == null ? null :
                priceQuotationDTO.getListOfAttach().stream()
                        .map(dto -> {
                            FileAttach fileAttach = FileUploadMapper.INSTANCE.toEntity(dto);
                            fileAttachRepository.saveAndFlush(fileAttach);
                            return fileAttach;
                        }).collect(Collectors.toCollection(LinkedList::new))
        );

        PriceQuotation saved = priceQuotationRepository.save(priceQuotation);

        priceQuotationDTO.getDetailTrxDTOS().stream().map(detailTrxDTO -> {
            PriceQuotationDetailTrx detailTrx = new PriceQuotationDetailTrx();
            detailTrx = PriceQuotationDetailTrxMapper.INSTANCE.toEntity(detailTrxDTO, detailTrx);
            if(detailTrxDTO.getMaterial() != null) detailTrx.setMaterial(materialRepository.findById(detailTrxDTO.getMaterial()).orElseThrow(() -> new BadRequestAlertException("Material not found","materialManagement","")));
            if(detailTrxDTO.getPrinter() != null) detailTrx.setPrinter(printerRepository.findById(detailTrxDTO.getPrinter()).orElseThrow(() -> new BadRequestAlertException("Printer not found","printerManagement","")));
            detailTrx.setPriceQuotation(saved);
            priceQuotationDetailTrxRepository.save(detailTrx);
            return detailTrx;
        }).collect(Collectors.toCollection(LinkedList::new));

        createHistory(saved);

        PqNumberDTO pqNumberDTO = new PqNumberDTO();
        pqNumberDTO.setMessageKind("PQ Number");
        pqNumberDTO.setValue(priceQuotationNumber);
        return pqNumberDTO;
    }

    @Override
    public PqNumberDTO update(PriceQuotationDTO priceQuotationDTO) {
        priceQuotationValidator.createValidation(priceQuotationDTO);
        //priceQuotationValidator.calculationValidation(priceQuotationDTO);

        PriceQuotation priceQuotation = priceQuotationRepository.findById(priceQuotationDTO.getId())
                .orElseThrow(() -> new BadRequestAlertException("Price quotation not found", "price-quotation", "idnull"));
        priceQuotation = PriceQuotationMapper.INSTANCE.toEntity(priceQuotationDTO, priceQuotation);
        priceQuotation.setCustomer(
                customerRepository.findById(priceQuotationDTO.getCustomer()).orElseThrow(() -> new BadRequestAlertException("Customer not found", "customerManagement", "idnull"))
        );
        priceQuotation.setSalesAdmin(
                userRepository.findById(priceQuotationDTO.getSalesAdmin()).orElseThrow(() -> new BadRequestAlertException("Sales admin not found", "userManagement", "idnull"))
        );
        priceQuotation.setAccountExecutive(
                userRepository.findById(priceQuotationDTO.getAccountExecutive()).orElseThrow(() -> new BadRequestAlertException("Sales person not found", "userManagement", "idnull"))
        );

//        String pqNumberDTO = "UPDATE/DUMMY/2018";
        String priceQuotationNumber = pqNumberService.generatePriceQuotationNumber(priceQuotationDTO.getCustomer(), "update");
        priceQuotation.setPqNumber(priceQuotationNumber);

        if (priceQuotationDTO.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEED_APPROVAL)){
            Role role = roleRepository.findById(priceQuotationDTO.getRoleId()).orElseThrow(() -> new BadRequestAlertException("Role not found","roleManagement",""));
            if(role.getName().equalsIgnoreCase(Constants.Role.ACCOUNT_EXCUTIVE)){
                Role approvalRole = roleRepository.findOneByName(Constants.Role.SALES_MANAGER).orElseThrow(() -> new BadRequestAlertException("Role sales maneger not found","roleManagement",""));
                priceQuotation.setApproval(approvalRole);
            } else if(role.getName().equalsIgnoreCase(Constants.Role.SALES_MANAGER)){
                Role approvalRole = roleRepository.findOneByName(Constants.Role.SUPERVISOR_ADMINISTRATOR).orElseThrow(() -> new BadRequestAlertException("Role supervisor administrator not found","roleManagement",""));
                priceQuotation.setApproval(approvalRole);
            } else {
                throw new BadRequestAlertException("This role cannot update price quotation","roleManagement","");
            }
        } else {
            priceQuotation.setApproval(null);
        }

        priceQuotation.setFileAttaches(priceQuotationDTO.getListOfAttach() == null ? null :
                priceQuotationDTO.getListOfAttach().stream()
                        .map(dto -> {
                            FileAttach fileAttach = FileUploadMapper.INSTANCE.toEntity(dto);
                            fileAttachRepository.saveAndFlush(fileAttach);
                            return fileAttach;
                        }).collect(Collectors.toCollection(LinkedList::new))
        );

        PriceQuotation saved = priceQuotationRepository.save(priceQuotation);

        priceQuotationDetailTrxRepository.findByPriceQuotation(saved)
                .forEach(priceQuotationDetailTrx -> priceQuotationDetailTrxRepository.delete(priceQuotationDetailTrx));

        priceQuotationDTO.getDetailTrxDTOS().stream().map(detailTrxDTO -> {
            PriceQuotationDetailTrx detailTrx = new PriceQuotationDetailTrx();
            detailTrx = PriceQuotationDetailTrxMapper.INSTANCE.toEntity(detailTrxDTO, detailTrx);
            if(detailTrxDTO.getMaterial() != null) detailTrx.setMaterial(materialRepository.findById(detailTrxDTO.getMaterial()).orElseThrow(() -> new BadRequestAlertException("Material not found","materialManagement","")));
            if(detailTrxDTO.getPrinter() != null) detailTrx.setPrinter(printerRepository.findById(detailTrxDTO.getPrinter()).orElseThrow(() -> new BadRequestAlertException("Printer not found","printerManagement","")));
            detailTrx.setPriceQuotation(saved);
            priceQuotationDetailTrxRepository.save(detailTrx);
            return detailTrx;
        }).collect(Collectors.toCollection(LinkedList::new));

        createHistory(saved);
        PqNumberDTO pqNumberDTO = new PqNumberDTO();
        pqNumberDTO.setMessageKind("PQ Number");
        pqNumberDTO.setValue(priceQuotationNumber);
        return pqNumberDTO;
    }

    @Override
    public Map<String, Object> getAll(Integer page, Integer limit, String sortBy, String direction, String pqNumber, Long roleId, Long customerId) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        pqNumber = StringUtils.isBlank(pqNumber) ? "%" : "%" + pqNumber + "%";

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);

        Page<PriceQuotation> resultPage = null;
        if(roleId != null) {
            Role role = roleRepository.findById(roleId)
                    .orElseThrow(()-> new BadRequestAlertException("Role not found", "roleManagement", "idnull"));
            if(role.getName().equalsIgnoreCase(Constants.Role.SALES_MANAGER) || role.getName().equalsIgnoreCase(Constants.Role.SUPERVISOR_ADMINISTRATOR)){
                resultPage = priceQuotationRepository.findByPqNumberLikeIgnoreCaseAndApprovalAndStatusNot(pqNumber, role, Constants.PriceQuotationStatus.DELETED, pageable);
            } else if (role.getName().equalsIgnoreCase(Constants.Role.ACCOUNT_EXCUTIVE)){
                resultPage = priceQuotationRepository.findByPqNumberLikeIgnoreCaseAndStatusNotAndAccountExecutiveIsNotNull(pqNumber, Constants.PriceQuotationStatus.DELETED, pageable);
            } else if (role.getName().equalsIgnoreCase(Constants.Role.SALES_ADMIN)){
                resultPage = priceQuotationRepository.findByPqNumberLikeIgnoreCaseAndStatusNot(pqNumber, Constants.PriceQuotationStatus.DELETED, pageable);
            }
        } else if (customerId != null){
            Customer customer = customerRepository.findById(customerId)
                    .orElseThrow(()-> new BadRequestAlertException("Customer not found", "customerManagement", "idnull"));
            resultPage = priceQuotationRepository.findByPqNumberLikeIgnoreCaseAndCustomerAndStatusNot(pqNumber, customer, Constants.PriceQuotationStatus.DELETED, pageable);
        } else {
            resultPage = priceQuotationRepository.findByPqNumberLikeIgnoreCaseAndStatusNot(pqNumber, Constants.PriceQuotationStatus.DELETED, pageable);
        }

        Collection<PriceQuotationListDTO> vos = resultPage.getContent().stream().map(entity -> {
            PriceQuotationListDTO dto = new PriceQuotationListDTO();
            dto = PriceQuotationListMapper.INSTANCE.toDto(entity, dto);
            return dto;
        }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    @Override
    public Optional<PriceQuotationDetailDTO> getPriceQuotation(Long id) {
        return priceQuotationRepository.findById(id).map(priceQuotation -> {
            PriceQuotationDetailDTO dto = new PriceQuotationDetailDTO();
            dto = PriceQuotationDetailMapper.INSTANCE.toDto(priceQuotation, dto);
            dto.setSalesPerson(UserMapper.INSTANCE.toDto(priceQuotation.getAccountExecutive()));
            dto.setSalesAdmin(UserMapper.INSTANCE.toDto(priceQuotation.getSalesAdmin()));
            dto.setCustomer(CustomerListMapper.INSTANCE.toDto(priceQuotation.getCustomer()));
            dto.setDetailTrxDTOS(priceQuotationDetailTrxRepository.findByPriceQuotation(priceQuotation)
                    .stream()
                    .map(priceQuotationDetailTrx -> {
                        PriceQuotationDetailTrxListDTO listDTO = PriceQuotationDetailTrxListMapper.INSTANCE.toDto(priceQuotationDetailTrx);
                        listDTO.setMaterial(MaterialMapper.INSTANCE.toDto(priceQuotationDetailTrx.getMaterial(), new MaterialDTO()));
                        listDTO.setPrinter(PrinterMapper.INSTANCE.toDto(priceQuotationDetailTrx.getPrinter(), new PrinterDTO()));
                        return listDTO;
                    }).collect(Collectors.toCollection(LinkedList::new)));
            dto.setListOfAttach(FileUploadMapper.INSTANCE.toDto(priceQuotation.getFileAttaches()));
            return dto;
        });
    }

    @Override
    public Boolean delete(Long id) {
        PriceQuotation priceQuotation = priceQuotationRepository.findById(id)
                .orElseThrow(() -> new BadRequestAlertException("Price quotation not found","priceQuotationManagement","idnull"));
        priceQuotation.setStatus(Constants.PriceQuotationStatus.DELETED);
        priceQuotationRepository.save(priceQuotation);
        return Boolean.TRUE;
    }

    @Override
    public Boolean statusUpdate(Long id, StatusPQDTO statusPQDTO) {
        PriceQuotation priceQuotation = priceQuotationRepository.findById(id)
                .orElseThrow(() -> new BadRequestAlertException("Price quotation not found","priceQuotationManagement","idnull"));
        priceQuotation.setStatus(statusPQDTO.getStatus());
        if(!statusPQDTO.getStatus().equalsIgnoreCase(Constants.PriceQuotationStatus.NEED_APPROVAL)){
            priceQuotation.setApproval(null);
        }
        priceQuotationRepository.save(priceQuotation);
        return Boolean.TRUE;
    }

    public void createHistory(PriceQuotation priceQuotation){
        PriceQuotationHistory priceQuotationHistory = new PriceQuotationHistory();
        priceQuotationHistory = PriceQuotationHistoryMapper.INSTANCE.toEntity(priceQuotation, priceQuotationHistory);
        priceQuotationHistory.setId(null);
        priceQuotationHistory.setPriceQuotation(priceQuotation);
        priceQuotationHistoryRepository.save(priceQuotationHistory);
    }


}
