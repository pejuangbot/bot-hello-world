package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.repository.UserRepository;
import com.nostratech.nurindo.service.EmailService;
import com.nostratech.nurindo.service.dto.EmailDTO;

import java.util.Base64;

import com.nostratech.nurindo.util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;


@Service
@PropertySource("classpath:configuration/email.properties")
public class EmailServiceImpl implements EmailService{
    @Autowired
    private EmailUtil emailUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SpringTemplateEngine templateEngine;

    @Value(value = "${url.master}")
    String urlMaster;

    @Value(value = "${url.path}")
    String urlPathr;

    @Override
    public Boolean sendEmailConfirmPassword(EmailDTO emailDTO) {
        Boolean result = true;
        User user = userRepository.findOneByEmailIgnoreCase(emailDTO.getRecepients())
                .orElseThrow(() -> new BadCredentialsException("Bad credentials (email tidak terdaftar)"));
        Context context = new Context();
        String username = user.getUsername();
        String pathEncode = Base64.getUrlEncoder().withoutPadding().encodeToString(username.getBytes());
        String url = urlMaster+urlPathr+"/"+pathEncode;
        System.out.println(url);
        context.setVariable("url", url);
        String html=null;
        if (emailDTO.getSubject().toUpperCase().equalsIgnoreCase("CREATE PASSWORD")){
            html = templateEngine.process("email-template", context);
        }else {
            html = templateEngine.process("email-template2", context);
        }
        emailUtil.sendEmail(emailDTO.getRecepients(), emailDTO.getSubject(), html);
        return result;
    }

}
