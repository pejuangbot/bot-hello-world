package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.RoleRepository;
import com.nostratech.nurindo.repository.UserRepository;
import com.nostratech.nurindo.service.EmailService;
import com.nostratech.nurindo.service.UserService;
import com.nostratech.nurindo.service.dto.*;
import com.nostratech.nurindo.service.mapper.UserCreateMapper;
import com.nostratech.nurindo.service.mapper.UserDetailMapper;
import com.nostratech.nurindo.service.mapper.UserListMapper;
import com.nostratech.nurindo.service.mapper.UserMapper;
import com.nostratech.nurindo.service.validator.UserValidator;
import com.nostratech.nurindo.util.ErrorConstants;
import com.nostratech.nurindo.util.PaginationUtil;
import com.nostratech.nurindo.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    UserValidator userValidator;
    @Autowired
    EmailService emailService;

    /**
     * Create user with all information, and return the create user.
     *
     * @param userDTO user to create
     * @return create user
     *
     */
    @Transactional
    @Override
    public Boolean createUser(UserCreateDTO userDTO) {
        List<Role> roleList = userValidator.checkRoles(userDTO.getRoles());

        User user = UserCreateMapper.INSTANCE.toEntity(userDTO);
        user.setFirstName(userDTO.getUsername());
        String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        user.setPassword(encryptedPassword);
        user.setRoles(roleList != null ? roleList : null);
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setSubject("Create Password");
        emailDTO.setRecepients(userDTO.getEmail());
        emailService.sendEmailConfirmPassword(emailDTO);

        return Boolean.TRUE;
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update
     * @return updated user
     */
    @Transactional
    @Override
    public Boolean updateUser(UserUpdateDTO userDTO) {
        List<Role> roleList = userValidator.checkRoles(userDTO.getRoles());

        User userFound = userRepository.findById(userDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "User not found!", "userManagement", "userexists"));
        userFound.setRoles(roleList != null ? roleList : null);
        userFound.setEnabled(userDTO.getEnabled());
        userFound.setEmail(userDTO.getEmail());
        userFound.setFirstName(userDTO.getUsername());
        userFound.setUsername(userDTO.getUsername());
        userRepository.save(userFound);
        log.debug("Created Information for User: {}", userFound);

        return Boolean.TRUE;
    }

    /**
     * Delete User base on username.
     *
     * @param username user to delete
     */
    @Override
    public void deleteUser(String username) {
        userRepository.findOneByUsername(username).ifPresent(user -> {
            userRepository.delete(user);
            log.debug("Deleted User: {}", user);
        });
    }

    @Override
    public Boolean changeStatus(Long id, StatusDTO statusDTO) {
        User user = userRepository.findById(id)
                .orElseThrow(()-> new BadRequestAlertException("User not found!", "userManagement", "idnull"));
        user.setEnabled(statusDTO.getActivated());
        userRepository.save(user);
        return Boolean.TRUE;
    }

    /**
     * Get all user by role.
     *
     * @return all users
     */
    @Override
    public List<UserDTO> findAllByRoles(String roleName) {
        List<UserDTO> userListDTOS = userRepository.findByRolesNameIgnoreCase(roleName)
                .stream()
                .distinct()
                .map(user -> UserMapper.INSTANCE.toDto(user))
                .collect(Collectors.toCollection(LinkedList::new));
        return userListDTOS;
    }

    /**
     * Get all users.
     *
     * @return all users
     */
    @Transactional(readOnly = true)
    @Override
    public Map<String, Object> getAllUsers(Integer page, Integer limit, String sortBy, String direction, String username, String role) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);
        Page<User> resultPage = null;
        if(StringUtils.isNotBlank(role)){
            role = StringUtils.isBlank(role) ? "%" : "%" + role + "%";
            resultPage = userRepository.findByRolesNameLikeIgnoreCase(role, pageable);
        } else {
            username = StringUtils.isBlank(username) ? "%" : "%" + username + "%";
            resultPage = userRepository.findByUsernameLikeIgnoreCase(username, pageable);
        }
        Collection<UserListDTO> vos = resultPage.getContent().stream().distinct().map(user -> {
            UserListDTO userListDTO = UserListMapper.INSTANCE.toDto(user);
            return userListDTO;
        }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, resultPage.getTotalElements(), resultPage.getTotalPages());
    }

    /**
     * GET /users/:username : get the "username" user.
     *
     * @param username the username of the user to find
     * @return the "username" user or null if not found
     */
    @Transactional(readOnly = true)
    @Override
    public Optional<UserDetailDTO> getUser(String username) {
        return userRepository.findOneByUsername(username).map(UserDetailMapper.INSTANCE::toDto);
    }

    /**
     * Update password user
     *
     * @param id the id of the user to find
     * @param changePasswordDTO new password to save
     *
     * @return boolean true or message error if not found
     */
    @Transactional
    @Override
    public Boolean changePassword(String id, ChangePasswordDTO changePasswordDTO) {
        byte[] decodedBytes = Base64.getDecoder().decode(id);
        String decodeResult = new String(decodedBytes);

        User user = userValidator.changePasswordValidation(decodeResult, changePasswordDTO);
        user.setPassword(passwordEncoder.encode(changePasswordDTO.getPassword()));
        user.setEnabled(Boolean.TRUE);
        userRepository.save(user);
        return Boolean.TRUE;
    }
}
