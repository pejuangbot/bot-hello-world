package com.nostratech.nurindo.service.impl;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.nostratech.nurindo.service.FileAttachService;
import com.nostratech.nurindo.service.dto.FileUploadDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
@PropertySource("classpath:configuration/attachment.properties")
public class FileAttachServiceImpl implements FileAttachService {

    @Value("${nurindo.aws.url}")
    String awsUrl;

    @Value("${nurindo.aws.url.view}")
    String awsUrlView;

    @Value("${nurindo.aws.bucket}")
    String awsBucket;

    @Value("${nurindo.aws.folder}")
    String awsFolder;

    @Value("${nurindo.aws.access.key}")
    String awsAccessKey;

    @Value("${nurindo.aws.secret.key}")
    String awsSecretKey;

    @Override
    public Optional<FileUploadDTO> uploadFile(MultipartFile file, String fileType) {

        String storageFolder = null;
        String realFileName = null;
        if (fileType.toLowerCase().equalsIgnoreCase("thumbnail")){
            storageFolder=fileType.toLowerCase();
        }
        else {
            storageFolder=awsFolder;
        }
        AWSCredentials credentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
        AmazonS3 s3Client = AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();

        if (!s3Client.doesBucketExistV2(awsBucket)){
            s3Client.createBucket(awsBucket);
        }

        UUID uuid = UUID.randomUUID();
        String fileName = uuid.toString();
        try {
            File convFile = convertMultiPartFile(file);
            s3Client.putObject(new PutObjectRequest(awsBucket + "/" + storageFolder, fileName, convFile)
                    .withCannedAcl(CannedAccessControlList.PublicRead));
            realFileName = convFile.getName();
            deleteLocalFile(convFile);
        } catch (IOException e){
//            throw new BadRequestAlertException("Uploading file is failed");
//            return "error";
        }

        String finalUrl = awsUrlView + awsBucket + "/" + storageFolder + "/" + fileName;
        FileUploadDTO fileUploadDTO = new FileUploadDTO();
        fileUploadDTO.setFileName(realFileName);
        fileUploadDTO.setFileUrl(finalUrl);
        fileUploadDTO.setFileDescription("Upload : "+ Instant.now());
        return Optional.ofNullable(fileUploadDTO);
    }

    private Boolean deleteLocalFile (File file){
        try{
            return Files.deleteIfExists(file.toPath());
        } catch (IOException e){
            return false;
        }
    }

    private File convertMultiPartFile(MultipartFile file) throws IOException{
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }
}
