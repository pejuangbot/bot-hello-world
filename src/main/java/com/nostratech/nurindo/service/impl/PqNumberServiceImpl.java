package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.PqNumberRecord;
import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.CustomerRepository;
import com.nostratech.nurindo.repository.PqNumberRecordRepository;
import com.nostratech.nurindo.repository.PriceQuotationRepository;
import com.nostratech.nurindo.service.PqNumberService;
import com.nostratech.nurindo.service.dto.PqNumberDTO;
import com.nostratech.nurindo.service.dto.PqNumberRecordDTO;
import com.nostratech.nurindo.service.mapper.PqNumberMapper;
import com.nostratech.nurindo.util.ErrorConstants;
import com.nostratech.nurindo.util.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class PqNumberServiceImpl implements PqNumberService {

    ModelMapper modelMapper = new ModelMapper();
    @Autowired
    private PqNumberRecordRepository pqNumberRecordRepository;

    @Autowired
    private PriceQuotationRepository priceQuotationRepository;

    @Autowired
    private CustomerRepository customerRepository;



    @Override
    public Optional<PqNumberDTO> getPqNumber(Long customerId, String actionType) {
        PqNumberDTO pqNumberDTO = new PqNumberDTO();
        String pqNumber=null;
        if (actionType.equalsIgnoreCase("create")){
            pqNumber = pqNumberRecordRepository.createPqNumber(customerId.toString());
        }
        else{
            pqNumber = pqNumberRecordRepository.updatePqNumber(customerId.toString());
        }
//        String val = pqNumberRecordRepository.createPqNumber(customerId.toString());
        log.info("nilai sekarang : "+pqNumber);
        pqNumberDTO.setMessageKind("Generate Number");
        pqNumberDTO.setValue(pqNumber);


        //Record Number Created
//        PqNumberRecord pqNumberRecord = new PqNumberRecord();
//        pqNumberRecord.setNumberGenerated(val);
//        String[] parts = val.split("\\/");
//        pqNumberRecord.setNumberType(customerCode);
//        pqNumberRecord.setMonth(parts[2]);
//        pqNumberRecord.setYear(parts[4]);
//        pqNumberRecordRepository.save(pqNumberRecord);

        return Optional.ofNullable(pqNumberDTO);
//        return null;
    }

    @Override
    public String generatePriceQuotationNumber(Long customerId, String actionType) {
        PqNumberDTO pqNumberDTO = new PqNumberDTO();
        String pqNumber=null;
        if (actionType.equalsIgnoreCase("create")){
            pqNumber = pqNumberRecordRepository.createPqNumber(customerId.toString());
        }
        else{
            pqNumber = pqNumberRecordRepository.updatePqNumber(customerId.toString());
        }
        return pqNumber;
    }

    @Scheduled(cron = "0 0 18 L * ?")
//    @Scheduled(cron = "*/5 * * * *")
    public void resetPassword(){
        pqNumberRecordRepository.resetPqNumber();
    }

    @Override
    @Transactional
    public Boolean updateNumber(PqNumberRecordDTO pqNumberRecordDTO) {
        PqNumberRecord pqNumberRecord = pqNumberRecordRepository.findById(pqNumberRecordDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "Number Record Id Not found!", "Number Record", "NumberExist"));
        modelMapper.map(pqNumberRecordDTO, pqNumberRecord);
        return Boolean.TRUE;
    }

    @Override
    @Transactional
    public Boolean deleteNumber(Long id) {
        pqNumberRecordRepository.deleteById(id);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> getAllNumber(Integer page, Integer limit, String sortBy, String direction, String numberType, String bulan) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        numberType = StringUtils.isBlank(numberType) ? "%" : "%" + numberType + "%";

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);
        Page<PqNumberRecord> repo;
        if(!StringUtils.isEmpty(bulan)){
            repo = pqNumberRecordRepository.findByNumberTypeLikeIgnoreCaseAndMonth(numberType, bulan, pageable);
        }else{
            repo = pqNumberRecordRepository.findByNumberTypeLikeIgnoreCase(numberType, pageable);
        }
        Page<PqNumberRecord> pqNumberRecords= repo;
        Collection<PqNumberRecordDTO> vos = pqNumberRecords.getContent().stream()
                .map(pqNumberRecord -> {
                    PqNumberRecordDTO pqNumberRecordDTO = PqNumberMapper.INSTANCE.toDto(pqNumberRecord);
                    return  pqNumberRecordDTO;
                }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, pqNumberRecords.getTotalElements(), pqNumberRecords.getTotalPages());
    }

}
