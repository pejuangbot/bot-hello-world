package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.City;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.CityRepository;
import com.nostratech.nurindo.service.CityService;
import com.nostratech.nurindo.service.dto.CityDTO;
import com.nostratech.nurindo.service.mapper.CityMapper;
import com.nostratech.nurindo.util.ErrorConstants;
import com.nostratech.nurindo.util.PaginationUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;

    ModelMapper modelMapper = new ModelMapper();

    @Override
    @Transactional
    public Boolean createCity(CityDTO cityDTO) {
        City city = CityMapper.INSTANCE.toEntity(cityDTO);
        cityRepository.save(city);
        return Boolean.TRUE;
    }

    @Override
    @Transactional
    public Boolean updateCity(CityDTO cityDTO) {
        City city  = cityRepository.findById(cityDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "City Id Not found!", "City", "CityExist"));
        modelMapper.map(cityDTO,city);
        return Boolean.TRUE;
    }

    @Override
    public Boolean deleteCity(Long id) {
        cityRepository.deleteById(id);
        return Boolean.TRUE;
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> getAllCityByName(Integer page, Integer limit, String sortBy, String direction, String cityName, String island, String cityCode) {
        sortBy = StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy;
        direction = StringUtils.isEmpty(direction) ? "desc" : direction;
        cityName = StringUtils.isBlank(cityName) ? "%" : "%" + cityName + "%";
        cityCode = StringUtils.isBlank(cityCode) ? "%" : "%" + cityCode + "%";
        island = StringUtils.isBlank(island) ? "%" : "%" + island + "%";

        Pageable pageable = PageRequest.of(page,limit, PaginationUtil.getSortBy(direction),sortBy);
        Page<City> cityList = cityRepository.findByCityNameLikeIgnoreCaseAndIslandLikeIgnoreCaseAndCityCodeLikeIgnoreCase(cityName,island, cityCode, pageable);
        Collection<CityDTO> vos = cityList.getContent().stream()
                .map(city -> {
                    CityDTO cityDTO = CityMapper.INSTANCE.toDto(city);
                    return  cityDTO;
                }).collect(Collectors.toCollection(LinkedList::new));
        return PaginationUtil.constructMapReturn(vos, cityList.getTotalElements(), cityList.getTotalPages());
    }
}
