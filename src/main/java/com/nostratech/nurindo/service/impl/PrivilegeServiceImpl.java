package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Privilege;
import com.nostratech.nurindo.repository.PrivilegeRepository;
import com.nostratech.nurindo.service.PrivilegeService;
import com.nostratech.nurindo.service.dto.PrivilegeDTO;
import com.nostratech.nurindo.service.mapper.PrivilegeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@Transactional
public class PrivilegeServiceImpl implements PrivilegeService {

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Override
    public PrivilegeDTO save(PrivilegeDTO privilegeDTO) {
        log.debug("Request to save Privilege : {}", privilegeDTO);
        Privilege privilege = PrivilegeMapper.INSTANCE.toEntity(privilegeDTO);
        privilege = privilegeRepository.save(privilege);
        return PrivilegeMapper.INSTANCE.toDto(privilege);
    }

    @Override
    public List<PrivilegeDTO> findAll() {
        return null;
    }

    @Override
    public Optional<PrivilegeDTO> findOne(Long id) {
        return Optional.empty();
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Page<PrivilegeDTO> findAll(Pageable pageable) {
        return null;
    }
}
