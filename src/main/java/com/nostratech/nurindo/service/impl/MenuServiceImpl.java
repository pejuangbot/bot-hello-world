package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.repository.MenuRepository;
import com.nostratech.nurindo.service.MenuService;
import com.nostratech.nurindo.service.dto.MenuDTO;
import com.nostratech.nurindo.service.mapper.MenuMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuRepository menuRepository;

    /**
     * Save a menu
     *
     * @param menuDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public MenuDTO save(MenuDTO menuDTO) {
        log.debug("Request to save Menu : {}", menuDTO);
        Menu menu = MenuMapper.INSTANCE.toEntity(menuDTO);
        menu.setMenuName(menuDTO.getName().toLowerCase().replace(" ", "_"));
        menu = menuRepository.save(menu);
        return MenuMapper.INSTANCE.toDto(menu);
    }

    /**
     * Get all of Menus
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<MenuDTO> findAll() {
        log.debug("Request to get all Roles");
        return menuRepository.findAll().stream()
                .map(MenuMapper.INSTANCE::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get specific menu
     *
     * @param id the id of entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MenuDTO> findOne(Long id) {
        log.debug("Request get Menu : {}", id);
        return menuRepository.findById(id)
                .map(MenuMapper.INSTANCE::toDto);
    }

    /**
     * Delete specific menu
     *
     * @param id the id of entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Menu : {}", id);
        menuRepository.deleteById(id);
    }

    /**
     * Get all of menus by page
     *
     * @param pageable
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MenuDTO> findAll(Pageable pageable) {
        log.debug("pageable");
        return menuRepository.findAll(pageable).map(MenuMapper.INSTANCE::toDto);
    }
}
