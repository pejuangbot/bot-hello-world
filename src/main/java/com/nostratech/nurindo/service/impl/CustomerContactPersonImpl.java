package com.nostratech.nurindo.service.impl;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.CustomerContactPerson;
import com.nostratech.nurindo.exception.BadRequestAlertException;
import com.nostratech.nurindo.repository.CustomerContactPersonRepository;
import com.nostratech.nurindo.service.CustomerContactPersonService;
import com.nostratech.nurindo.service.dto.CustomerContactPersonDTO;
import com.nostratech.nurindo.service.mapper.CustomerContactPersonMapper;
import com.nostratech.nurindo.util.ErrorConstants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


@Slf4j
@Service
@Transactional
public class CustomerContactPersonImpl implements CustomerContactPersonService {

    @Autowired
    CustomerContactPersonRepository customerContactPersonRepository;

    ModelMapper modelMapper = new ModelMapper();

    @Transactional
    @Override
    public Boolean createCustomerContactPerson(CustomerContactPersonDTO customerContactPersonDTO, Customer customer) {
        CustomerContactPerson customerContactPerson = CustomerContactPersonMapper.INSTANCE.toEntity(customerContactPersonDTO);
        customerContactPerson.setCustomer(customer);
        customerContactPersonRepository.save(customerContactPerson);
        return Boolean.TRUE;
    }

    @Transactional
    @Override
    public Boolean updateCustomerContactPerson(CustomerContactPersonDTO customerContactPersonDTO, Long customerId) {
        CustomerContactPerson customerContactPerson  = customerContactPersonRepository.findById(customerContactPersonDTO.getId())
                .orElseThrow(()-> new BadRequestAlertException(ErrorConstants.ENTITY_NOT_FOUND_TYPE, "customer not found!", "customerManagement", "customerexists"));
        customerContactPersonDTO.setCustomerId(customerId);
        modelMapper.map(customerContactPersonDTO, customerContactPerson);
        return Boolean.TRUE;
    }

    @Override
    public Page<CustomerContactPersonDTO> getContactPersonByCustomerId(String customerId, Pageable pageable) {
        return customerContactPersonRepository.findByCustomerId(customerId, pageable).map(CustomerContactPersonMapper.INSTANCE::toDto);
    }

    @Override
    public List<CustomerContactPersonDTO> getByCustomerId(Long customerId) {
        return customerContactPersonRepository.findByCustomerId(customerId)
                .stream()
                .map(CustomerContactPersonMapper.INSTANCE::toDto)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Delete specific role
     *
     * @param id the id of entity
     */
    @Override
    public Boolean delete(Long id) {
        log.debug("Request to delete Role : {}", id);
        customerContactPersonRepository.deleteById(id);
        return Boolean.TRUE;
    }
}
