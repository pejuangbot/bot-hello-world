package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.City;
import com.nostratech.nurindo.domain.FileAttach;
import com.nostratech.nurindo.service.dto.CityDTO;
import com.nostratech.nurindo.service.dto.FileUploadDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface FileUploadMapper extends EntityMapper<FileUploadDTO, FileAttach>{
    FileUploadMapper INSTANCE= Mappers.getMapper(FileUploadMapper.class);

}
