package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.UserCreateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Mapper for the entity User and its DTO called UserCreateDTO.
 *
 */
@Mapper(componentModel = "spring")
public interface UserCreateMapper extends EntityMapper<UserCreateDTO, User>{

    UserCreateMapper INSTANCE = Mappers.getMapper(UserCreateMapper.class);

}
