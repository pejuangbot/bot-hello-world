package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.SalesOrder;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailDTO;
import com.nostratech.nurindo.service.dto.SalesOrderDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface SalesOrderDetailMapper {

    SalesOrderDetailMapper INSTANCE = Mappers.getMapper(SalesOrderDetailMapper.class);

    @Mapping(target = "createdDate", ignore = true)
    SalesOrderDetailDTO toDto(PriceQuotation entity, @MappingTarget SalesOrderDetailDTO dto);


}
