package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.service.dto.RoleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface RoleMapper extends EntityMapper<RoleDTO, Role> {

    RoleMapper INSTANCE = Mappers.getMapper(RoleMapper.class);
}
