package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.service.dto.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer>{

        CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

        @Override
        Customer toEntity(CustomerDTO customerDTO);

        @Override
        CustomerDTO toDto(Customer customer);
}
