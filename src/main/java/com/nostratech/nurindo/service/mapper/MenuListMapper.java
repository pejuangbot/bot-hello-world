package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.service.dto.MenuListDTO;
import com.nostratech.nurindo.service.dto.RoleListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface MenuListMapper extends EntityMapper<MenuListDTO, Menu> {

    MenuListMapper INSTANCE = Mappers.getMapper(MenuListMapper.class);

}
