package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.UserUpdateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

/**
 * Mapper for the entity User and its DTO called UserCreateDTO.
 *
 */
@Mapper(componentModel = "spring")
public interface UserUpdateMapper extends EntityMapper<UserUpdateDTO, User>{

    UserUpdateMapper INSTANCE = Mappers.getMapper(UserUpdateMapper.class);

    @Override
    User toEntity(UserUpdateDTO dto);

    @Override
    UserUpdateDTO toDto(User entity);

}
