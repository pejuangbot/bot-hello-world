package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.service.dto.CustomerListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerListMapper extends  EntityMapper<CustomerListDTO, Customer>{

    CustomerListMapper INSTANCE= Mappers.getMapper(CustomerListMapper.class);

    @Override
    Customer toEntity(CustomerListDTO customerListDTO);

    @Override
    CustomerListDTO toDto(Customer customer);
}