package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.PriceQuotationHistory;
import com.nostratech.nurindo.service.dto.PriceQuotationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PriceQuotationHistoryMapper {

    PriceQuotationHistoryMapper INSTANCE = Mappers.getMapper(PriceQuotationHistoryMapper.class);

    PriceQuotationHistory toEntity(PriceQuotation dto, @MappingTarget PriceQuotationHistory entity);

}
