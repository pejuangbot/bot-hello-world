package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.service.dto.CustomerProductsListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerProductsListMapper extends EntityMapper<CustomerProductsListDTO, CustomerProducts> {

    CustomerProductsListMapper INSTANCE = Mappers.getMapper(CustomerProductsListMapper.class);

    @Override
    CustomerProducts toEntity(CustomerProductsListDTO dto);

    @Override
    CustomerProductsListDTO toDto(CustomerProducts entity);

}