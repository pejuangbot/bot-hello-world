package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.UserListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

/**
 * Mapper for the entity User and its DTO called UserCreateDTO.
 *
 */
@Mapper(componentModel = "spring")
public interface UserListMapper extends EntityMapper<UserListDTO, User>{

    UserListMapper INSTANCE = Mappers.getMapper(UserListMapper.class);

    @Override
    @Mapping(source = "dto", target = "lastModifiedDate", qualifiedByName = "lastModifiedDateToInstant")
    User toEntity(UserListDTO dto);

    @Override
    @Mapping(source = "entity", target = "lastModifiedDate", qualifiedByName = "lastModifiedDateToDto")
    UserListDTO toDto(User entity);

    @Named("lastModifiedDateToDto")
    default Long lastModifiedDateToDto(User user) {
        return user.getLastModifiedDate().toEpochMilli();
    }

    @Named("lastModifiedDateToInstant")
    default Instant lastModifiedDateToInstant(UserListDTO userListDTO) {
        return Instant.ofEpochMilli(userListDTO.getLastModifiedDate());
    }
}
