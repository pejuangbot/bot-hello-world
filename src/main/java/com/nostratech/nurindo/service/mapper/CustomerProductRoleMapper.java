package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.service.dto.CustomerProductRoleDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerProductRoleMapper extends EntityMapper<CustomerProductRoleDTO, CustomerProducts> {

    CustomerProductRoleMapper INSTANCE = Mappers.getMapper(CustomerProductRoleMapper.class);

    @Override
    CustomerProducts toEntity(CustomerProductRoleDTO customerProductRoleDTO);

    @Override
    CustomerProductRoleDTO toDto(CustomerProducts customerProducts);
}
