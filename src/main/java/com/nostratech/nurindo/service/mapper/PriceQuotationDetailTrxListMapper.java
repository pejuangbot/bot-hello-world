package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotationDetailTrx;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailTrxDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailTrxListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PriceQuotationDetailTrxListMapper {

    PriceQuotationDetailTrxListMapper INSTANCE = Mappers.getMapper(PriceQuotationDetailTrxListMapper.class);

    @Mapping(target = "material", ignore = true)
    @Mapping(target = "printer", ignore = true)
    PriceQuotationDetailTrxListDTO toDto(PriceQuotationDetailTrx dto);

}
