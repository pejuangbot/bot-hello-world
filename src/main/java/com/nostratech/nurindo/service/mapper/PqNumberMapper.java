package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PqNumberRecord;
import com.nostratech.nurindo.service.dto.PqNumberDTO;
import com.nostratech.nurindo.service.dto.PqNumberRecordDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PqNumberMapper extends EntityMapper<PqNumberRecordDTO, PqNumberRecord>{

    PqNumberMapper INSTANCE = Mappers.getMapper(PqNumberMapper.class);

    @Override
    PqNumberRecord toEntity(PqNumberRecordDTO pqNumberRecordDTO);

    @Override
    PqNumberRecordDTO toDto(PqNumberRecord pqNumberRecord);
}
