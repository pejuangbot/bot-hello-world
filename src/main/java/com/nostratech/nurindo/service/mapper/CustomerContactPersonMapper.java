package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerContactPerson;
import com.nostratech.nurindo.service.dto.CustomerContactPersonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerContactPersonMapper extends EntityMapper<CustomerContactPersonDTO, CustomerContactPerson> {
    CustomerContactPersonMapper INSTANCE = Mappers.getMapper(CustomerContactPersonMapper.class);

    @Override
    CustomerContactPerson toEntity(CustomerContactPersonDTO customerContactPersonDTO);

    @Override
    CustomerContactPersonDTO toDto(CustomerContactPerson customerContactPerson);
}
