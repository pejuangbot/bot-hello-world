package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.service.dto.ReportPriceQuotationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ReportPriceQuotationMapper extends EntityMapper<ReportPriceQuotationDTO, PriceQuotation>{
    ReportPriceQuotationMapper INSTANCE = Mappers.getMapper(ReportPriceQuotationMapper.class);
}
