package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.SalesOrder;
import com.nostratech.nurindo.service.dto.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

@Mapper(componentModel = "spring")
public interface SalesOrderMapper {

    SalesOrderMapper INSTANCE = Mappers.getMapper(SalesOrderMapper.class);

    @Mapping(target = "priceQuotation", ignore = true)
    @Mapping(source = "dto", target = "shipmentDate", qualifiedByName = "shipmentDateToInstant")
    SalesOrder toEntity(SalesOrderDTO dto, @MappingTarget SalesOrder entity);

    @Mapping(target = "priceQuotation", ignore = true)
    @Mapping(source = "entity", target = "shipmentDate", qualifiedByName = "shipmentDateToLong")
    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToLong")
    @Mapping(source = "notes", target = "salesOrderNotes")
    @Mapping(source = "priceQuotation.notes", target = "notes")
    SalesOrderDetailDTO toDto(SalesOrder entity, @MappingTarget SalesOrderDetailDTO dto);

    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToLong")
    SalesOrderListDTO toDto(SalesOrder entity, @MappingTarget SalesOrderListDTO dto);

    @Named("shipmentDateToInstant")
    default Instant shipmentDateToInstant(SalesOrderDTO salesOrderDTO) {
        return Instant.ofEpochMilli(salesOrderDTO.getShipmentDate());
    }

    @Named("createdDateToLong")
    default Long createdDateToLong(SalesOrder salesOrder) {
        return salesOrder.getCreatedDate().toEpochMilli();
    }

    @Named("shipmentDateToLong")
    default Long shipmentDateToLong(SalesOrder salesOrder) {
        return salesOrder.getShipmentDate().toEpochMilli();
    }

}
