package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.ProductDTO;
import com.nostratech.nurindo.service.dto.ProductDetailDTO;
import com.nostratech.nurindo.service.dto.ProductListDTO;
import com.nostratech.nurindo.service.dto.UserDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

@Mapper(componentModel = "spring")
public interface ProductListMapper {

    ProductListMapper INSTANCE = Mappers.getMapper(ProductListMapper.class);

    @Mapping(source = "dto", target = "createdDate", qualifiedByName = "createdDateToInstant")
    @Mapping(source = "dto", target = "lastModifiedDate", qualifiedByName = "lastModifiedDateToInstant")
    Product toEntity(ProductListDTO dto, @MappingTarget Product entity );

    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToDto")
    @Mapping(source = "entity", target = "lastModifiedDate", qualifiedByName = "lastModifiedDateToDto")
    ProductListDTO toDto(Product entity, @MappingTarget ProductListDTO dto);

    @Named("createdDateToDto")
    default Long createdDateToDto(Product entity) {
        return entity.getCreatedDate().toEpochMilli();
    }

    @Named("createdDateToInstant")
    default Instant createdDateToInstant(ProductListDTO dto) {
        return Instant.ofEpochMilli(dto.getCreatedDate());
    }

    @Named("lastModifiedDateToDto")
    default Long lastModifiedDateToDto(Product entity) {
        return entity.getLastModifiedDate().toEpochMilli();
    }

    @Named("lastModifiedDateToInstant")
    default Instant lastModifiedDateToInstant(ProductListDTO dto) {
        return Instant.ofEpochMilli(dto.getLastModifiedDate());
    }

}
