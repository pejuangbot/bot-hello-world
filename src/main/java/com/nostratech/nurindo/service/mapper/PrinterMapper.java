package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.domain.Printer;
import com.nostratech.nurindo.service.dto.MenuDTO;
import com.nostratech.nurindo.service.dto.PrinterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PrinterMapper {

    PrinterMapper INSTANCE = Mappers.getMapper(PrinterMapper.class);

    Printer toEntity(PrinterDTO dto, @MappingTarget Printer entity);

    PrinterDTO toDto(Printer entity, @MappingTarget PrinterDTO dto);

    List<Printer> toEntity(List<PrinterDTO> dto, @MappingTarget List<Printer> entity);

    List<PrinterDTO> toDto(List<Printer> entity, @MappingTarget List<PrinterDTO> dto);
}
