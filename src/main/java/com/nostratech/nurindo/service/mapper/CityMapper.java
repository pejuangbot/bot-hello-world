package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.City;
import com.nostratech.nurindo.service.dto.CityDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CityMapper extends EntityMapper<CityDTO, City>{
    CityMapper INSTANCE= Mappers.getMapper(CityMapper.class);

    @Override
    City toEntity(CityDTO cityDTO);

    @Override
    CityDTO toDto(City city);
}
