package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.service.dto.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductDTO toDto(Product entity, @MappingTarget ProductDTO dto);

    Product toEntity(ProductDTO dto, @MappingTarget Product entity);
}
