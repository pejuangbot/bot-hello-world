package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.service.dto.CustomerProductsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

@Mapper(componentModel = "spring")
public interface CustomerProductsMapper extends EntityMapper<CustomerProductsDTO, CustomerProducts> {

    CustomerProductsMapper INSTANCE = Mappers.getMapper(CustomerProductsMapper.class);

    @Override
    CustomerProducts toEntity(CustomerProductsDTO dto);

    @Override
//    @Mapping(source = "role.id", target = "id")
//    @Mapping(source = "role.name", target = "name")
    CustomerProductsDTO toDto(CustomerProducts entity);

}
