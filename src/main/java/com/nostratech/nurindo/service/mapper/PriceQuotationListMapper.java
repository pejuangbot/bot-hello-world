package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.service.dto.PriceQuotationDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationListDTO;
import com.nostratech.nurindo.service.dto.UserDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

@Mapper(componentModel = "spring")
public interface PriceQuotationListMapper {

    PriceQuotationListMapper INSTANCE = Mappers.getMapper(PriceQuotationListMapper.class);

    @Mapping(source = "accountExecutive.firstName", target = "accountExecutive")
    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToDto")
    @Mapping(source = "approval.name", target = "needApprovalFrom")
    PriceQuotationListDTO toDto(PriceQuotation entity, @MappingTarget PriceQuotationListDTO dto);

    @Named("createdDateToDto")
    default Long createdDateToDto(PriceQuotation entity) {
        return entity.getCreatedDate().toEpochMilli();
    }
}
