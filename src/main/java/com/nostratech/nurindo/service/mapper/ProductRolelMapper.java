package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.domain.ProductRole;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.ProductDetailDTO;
import com.nostratech.nurindo.service.dto.ProductRoleDTO;
import com.nostratech.nurindo.service.dto.UserDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

@Mapper(componentModel = "spring")
public interface ProductRolelMapper extends EntityMapper<ProductRoleDTO, ProductRole> {

    ProductRolelMapper INSTANCE = Mappers.getMapper(ProductRolelMapper.class);

    @Override
    ProductRole toEntity(ProductRoleDTO dto);

    @Override
    @Mapping(source = "role.id", target = "id")
    @Mapping(source = "role.name", target = "name")
    ProductRoleDTO toDto(ProductRole entity);

}
