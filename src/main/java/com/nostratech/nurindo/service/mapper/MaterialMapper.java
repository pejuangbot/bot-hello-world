package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Material;
import com.nostratech.nurindo.domain.Printer;
import com.nostratech.nurindo.service.dto.MaterialDTO;
import com.nostratech.nurindo.service.dto.PrinterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MaterialMapper{

    MaterialMapper INSTANCE = Mappers.getMapper(MaterialMapper.class);

    Material toEntity(MaterialDTO dto, @MappingTarget Material entity);

    MaterialDTO toDto(Material entity, @MappingTarget MaterialDTO dto);

    List<Material> toEntity(List<MaterialDTO> dto, @MappingTarget List<Material> entity);

    List<MaterialDTO> toDto(List<Material> entity, @MappingTarget List<MaterialDTO> dto);
}
