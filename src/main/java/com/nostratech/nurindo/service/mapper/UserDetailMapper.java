package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.UserDetailDTO;
import com.nostratech.nurindo.service.dto.UserListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import java.time.Instant;

/**
 * Mapper for the entity User and its DTO called UserCreateDTO.
 *
 */
@Mapper(componentModel = "spring")
public interface UserDetailMapper extends EntityMapper<UserDetailDTO, User>{

    UserDetailMapper INSTANCE = Mappers.getMapper(UserDetailMapper.class);

    @Override
    @Mapping(source = "dto", target = "createdDate", qualifiedByName = "createdDateToInstant")
    @Mapping(source = "dto", target = "lastModifiedDate", qualifiedByName = "lastModifiedDateToInstant")
    User toEntity(UserDetailDTO dto);

    @Override
    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToDto")
    @Mapping(source = "entity", target = "lastModifiedDate", qualifiedByName = "lastModifiedDateToDto")
    UserDetailDTO toDto(User entity);

    @Named("createdDateToDto")
    default Long createdDateToDto(User entity) {
        return entity.getCreatedDate().toEpochMilli();
    }

    @Named("createdDateToInstant")
    default Instant createdDateToInstant(UserDetailDTO dto) {
        return Instant.ofEpochMilli(dto.getCreatedDate());
    }

    @Named("lastModifiedDateToDto")
    default Long lastModifiedDateToDto(User entity) {
        return entity.getLastModifiedDate().toEpochMilli();
    }

    @Named("lastModifiedDateToInstant")
    default Instant lastModifiedDateToInstant(UserDetailDTO dto) {
        return Instant.ofEpochMilli(dto.getLastModifiedDate());
    }
}
