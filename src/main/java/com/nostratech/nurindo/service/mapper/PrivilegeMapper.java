package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Privilege;
import com.nostratech.nurindo.service.dto.PrivilegeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PrivilegeMapper extends EntityMapper<PrivilegeDTO, Privilege>{


    PrivilegeMapper INSTANCE = Mappers.getMapper(PrivilegeMapper.class);

    @Override
    @Mapping(source = "menu", target = "menu.id")
    Privilege toEntity(PrivilegeDTO dto);

    @Override
    @Mapping(source = "menu.id", target = "menu")
    PrivilegeDTO toDto(Privilege entity);
}
