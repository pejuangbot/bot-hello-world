package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.PriceQuotationDetailTrx;
import com.nostratech.nurindo.service.dto.PriceQuotationDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailTrxDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PriceQuotationDetailTrxMapper {

    PriceQuotationDetailTrxMapper INSTANCE = Mappers.getMapper(PriceQuotationDetailTrxMapper.class);

    @Mapping(target = "material", ignore = true)
    @Mapping(target = "printer", ignore = true)
    PriceQuotationDetailTrx toEntity(PriceQuotationDetailTrxDTO dto, @MappingTarget PriceQuotationDetailTrx entity);

}
