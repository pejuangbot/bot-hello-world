package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.service.dto.CustomerDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface CustomerDetailMapper extends EntityMapper<CustomerDetailDTO, Customer> {
    CustomerDetailMapper INSTANCE= Mappers.getMapper(CustomerDetailMapper.class);

    @Override
    Customer toEntity(CustomerDetailDTO customerDetailDTO);

    @Override
    CustomerDetailDTO toDto(Customer customer);
}
