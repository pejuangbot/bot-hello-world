package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.service.dto.MenuDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PriceQuotationMapper {

    PriceQuotationMapper INSTANCE = Mappers.getMapper(PriceQuotationMapper.class);

    @Mapping(target = "accountExecutive", ignore = true)
    @Mapping(target = "salesAdmin", ignore = true)
    @Mapping(target = "customer", ignore = true)
    PriceQuotation toEntity(PriceQuotationDTO dto, @MappingTarget PriceQuotation entity);

    @Mapping(source = "accountExecutive.id", target = "accountExecutive")
    @Mapping(source = "salesAdmin.id", target = "salesAdmin")
    @Mapping(source = "customer.id", target = "customer")
    PriceQuotationDTO toDto(PriceQuotation entity, @MappingTarget PriceQuotationDTO dto);
}
