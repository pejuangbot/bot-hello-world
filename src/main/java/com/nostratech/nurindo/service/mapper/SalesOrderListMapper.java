package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.SalesOrder;
import com.nostratech.nurindo.service.dto.PriceQuotationListDTO;
import com.nostratech.nurindo.service.dto.SalesOrderListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface SalesOrderListMapper {

    SalesOrderListMapper INSTANCE = Mappers.getMapper(SalesOrderListMapper.class);

    @Mapping(source = "accountExecutive.firstName", target = "accountExecutive")
    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToDto")
    SalesOrderListDTO toDto(PriceQuotation entity, @MappingTarget SalesOrderListDTO dto);

    @Named("createdDateToDto")
    default Long createdDateToDto(PriceQuotation entity) {
        return entity.getCreatedDate().toEpochMilli();
    }
}
