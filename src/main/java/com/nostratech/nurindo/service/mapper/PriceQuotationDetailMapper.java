package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationListDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface PriceQuotationDetailMapper {

    PriceQuotationDetailMapper INSTANCE = Mappers.getMapper(PriceQuotationDetailMapper.class);

    @Mapping(source = "approval.name", target = "needApprovalFrom")
    @Mapping(source = "entity", target = "createdDate", qualifiedByName = "createdDateToDto")
    PriceQuotationDetailDTO toDto(PriceQuotation entity, @MappingTarget PriceQuotationDetailDTO dto);

    @Named("createdDateToDto")
    default Long createdDateToDto(PriceQuotation entity) {
        return entity.getCreatedDate().toEpochMilli();
    }
}
