package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.UserCreateDTO;
import com.nostratech.nurindo.service.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Mapper for the entity User and its DTO called UserCreateDTO.
 *
 */
@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDTO, User>{

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

}
