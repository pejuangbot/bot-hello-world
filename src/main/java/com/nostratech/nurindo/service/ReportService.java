package com.nostratech.nurindo.service;

import java.util.Map;

public interface ReportService {
    Map<String, Object> exportReportPriceQuotation(String documentType, Long idPq);
}
