package com.nostratech.nurindo.service;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.CustomerContactPerson;
import com.nostratech.nurindo.service.dto.CustomerContactPersonDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CustomerContactPersonService {
    /**
     * Save a customer
     *
     * @param customerContactPersonDTO the entity to save
     * @return the persisted id
     */
    Boolean createCustomerContactPerson(CustomerContactPersonDTO customerContactPersonDTO, Customer customer);


    Boolean updateCustomerContactPerson(CustomerContactPersonDTO customerContactPersonDTO, Long customerId);



    /**
     * Get all of CustomerContactPerson by Customer Id
     *
     * @return list of entities
     */
    Page<CustomerContactPersonDTO> getContactPersonByCustomerId(String customerId, Pageable pageable);

    List<CustomerContactPersonDTO> getByCustomerId(Long customerId);


    /**
     * Delete specific role
     *
     * @param id the id of entity
     */
    Boolean delete(Long id);



}
