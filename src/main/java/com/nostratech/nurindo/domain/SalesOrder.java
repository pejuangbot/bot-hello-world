package com.nostratech.nurindo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Entity
@Table(name = "sales_order")
public class SalesOrder extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "so_number", length = 50, nullable = false)
    private String soNumber;

    @Column(name = "pr_origin", length = 50, nullable = true)
    private String prOrigin;

    @Column(name = "notes", length = 256, nullable = true)
    private String notes;

    @Column(name = "shipment_type", length = 50, nullable = false)
    private String shipmentType;

    @Column(name = "shipment_date", nullable = false)
    private Instant shipmentDate;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "price_quotation")
    private PriceQuotation priceQuotation;

    @Column(length = 10)
    private String status;

    public SalesOrder() {
    }

    public SalesOrder(Long id, String soNumber, String prOrigin, String notes, String shipmentType, Instant shipmentDate, PriceQuotation priceQuotation, String status) {
        this.id = id;
        this.soNumber = soNumber;
        this.prOrigin = prOrigin;
        this.notes = notes;
        this.shipmentType = shipmentType;
        this.shipmentDate = shipmentDate;
        this.priceQuotation = priceQuotation;
        this.status = status;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSoNumber() {
        return soNumber;
    }

    public void setSoNumber(String soNumber) {
        this.soNumber = soNumber;
    }

    public String getPrOrigin() {
        return prOrigin;
    }

    public void setPrOrigin(String prOrigin) {
        this.prOrigin = prOrigin;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getShipmentType() {
        return shipmentType;
    }

    public void setShipmentType(String shipmentType) {
        this.shipmentType = shipmentType;
    }

    public Instant getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(Instant shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    public PriceQuotation getPriceQuotation() {
        return priceQuotation;
    }

    public void setPriceQuotation(PriceQuotation priceQuotation) {
        this.priceQuotation = priceQuotation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SalesOrder{" +
                "id=" + id +
                ", soNumber='" + soNumber + '\'' +
                ", prOrigin='" + prOrigin + '\'' +
                ", notes='" + notes + '\'' +
                ", shipmentType='" + shipmentType + '\'' +
                ", shipmentDate=" + shipmentDate +
                ", priceQuotation=" + priceQuotation +
                ", status='" + status + '\'' +
                '}';
    }
}
