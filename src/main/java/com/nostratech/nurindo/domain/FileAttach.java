package com.nostratech.nurindo.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "file_attach")
@DynamicUpdate
public class FileAttach extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonProperty(value = "file_name")
    private String fileName;

    @JsonProperty(value = "file_url")
    private String fileUrl;

    @JsonProperty(value = "file_description")
    private String fileDescription;

    public FileAttach() {
    }

    public FileAttach(Long id, String fileName, String fileUrl, String fileDescription) {
        this.id = id;
        this.fileName = fileName;
        this.fileUrl = fileUrl;
        this.fileDescription = fileDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileDescription() {
        return fileDescription;
    }

    public void setFileDescription(String fileDescription) {
        this.fileDescription = fileDescription;
    }

    @Override
    public String toString() {
        return "FileAttach{" +
                "id=" + id +
                ", fileName='" + fileName + '\'' +
                ", fileUrl='" + fileUrl + '\'' +
                ", fileDescription='" + fileDescription + '\'' +
                '}';
    }
}
