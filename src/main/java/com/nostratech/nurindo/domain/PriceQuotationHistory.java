package com.nostratech.nurindo.domain;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "price_quotation_history")
public class PriceQuotationHistory extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pq_number", length = 50, nullable = false)
    private String pqNumber;

    @Column(name = "pr_number", length = 50, nullable = false)
    private String prNumber;

    @ManyToOne
    private User accountExecutive;

    @ManyToOne
    private User salesAdmin;

    @ManyToOne
    private Customer customer;

    @Column(name = "project_name", length = 100, nullable = false)
    private String projectName;

    @Column(name = "customer_name", length = 100, nullable = false)
    private String customerName;

    @Column(name = "phone_number", length = 32, nullable = false)
    private String phoneNumber;

    @Column(name = "fax_number", length = 32, nullable = false)
    private String faxNumber;

    @Column(name = "email", length = 254, nullable = false)
    private String email;

    @Column(name = "notes", length = 254, nullable = true)
    private String notes;

    @Column(name = "type", length = 10, nullable = true)
    private String type;

    @ManyToOne
    private PriceQuotation priceQuotation;

    public PriceQuotationHistory() {
    }

    public PriceQuotationHistory(Long id, String pqNumber, String prNumber, User accountExecutive, User salesAdmin, Customer customer, String projectName, String customerName, String phoneNumber, String faxNumber, String email, String notes, String type, PriceQuotation priceQuotation) {
        this.id = id;
        this.pqNumber = pqNumber;
        this.prNumber = prNumber;
        this.accountExecutive = accountExecutive;
        this.salesAdmin = salesAdmin;
        this.customer = customer;
        this.projectName = projectName;
        this.customerName = customerName;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.email = email;
        this.notes = notes;
        this.type = type;
        this.priceQuotation = priceQuotation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPqNumber() {
        return pqNumber;
    }

    public void setPqNumber(String pqNumber) {
        this.pqNumber = pqNumber;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public User getAccountExecutive() {
        return accountExecutive;
    }

    public void setAccountExecutive(User accountExecutive) {
        this.accountExecutive = accountExecutive;
    }

    public User getSalesAdmin() {
        return salesAdmin;
    }

    public void setSalesAdmin(User salesAdmin) {
        this.salesAdmin = salesAdmin;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PriceQuotation getPriceQuotation() {
        return priceQuotation;
    }

    public void setPriceQuotation(PriceQuotation priceQuotation) {
        this.priceQuotation = priceQuotation;
    }

    @Override
    public String toString() {
        return "PriceQuotationHistory{" +
                "id=" + id +
                ", pqNumber='" + pqNumber + '\'' +
                ", prNumber='" + prNumber + '\'' +
                ", accountExecutive=" + accountExecutive +
                ", salesAdmin=" + salesAdmin +
                ", customer=" + customer +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", notes='" + notes + '\'' +
                ", type='" + type + '\'' +
                ", priceQuotation=" + priceQuotation +
                '}';
    }
}
