package com.nostratech.nurindo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "privilege")
public class Privilege extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Menu menu;

    @NotNull
    @Column(name = "creates", nullable = false)
    private Boolean create;

    @NotNull
    @Column(name = "reads", nullable = false)
    private Boolean read;

    @NotNull
    @Column(name = "updates", nullable = false)
    private Boolean update;

    @NotNull
    @Column(name = "deletes", nullable = false)
    private Boolean delete;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Boolean getCreate() {
        return create;
    }

    public void setCreate(Boolean create) {
        this.create = create;
    }

    public Boolean getRead() {
        return read;
    }

    public void setRead(Boolean read) {
        this.read = read;
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(Boolean update) {
        this.update = update;
    }

    public Boolean getDelete() {
        return delete;
    }

    public void setDelete(Boolean delete) {
        this.delete = delete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Privilege privilege = (Privilege) o;

        return !(privilege.getId() == null || getId() == null) && Objects.equals(getId(), privilege.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Privilege{" +
                "menu=" + menu.getName() +
                ", create=" + create +
                ", update=" + update +
                ", read=" + read +
                ", delete=" + delete +
                '}';
    }
}
