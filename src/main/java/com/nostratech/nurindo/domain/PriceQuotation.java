package com.nostratech.nurindo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "price_quotation")
public class PriceQuotation extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pq_number", length = 50, nullable = false)
    private String pqNumber;

    @Column(name = "pr_number", length = 50, nullable = false)
    private String prNumber;

    @ManyToOne
    @JoinColumn(name = "account_executive")
    private User accountExecutive;

    @ManyToOne
    @JoinColumn(name = "sales_admin")
    private User salesAdmin;

    @ManyToOne
    @JoinColumn(name = "customer")
    private Customer customer;

    @Column(name = "project_name", length = 100, nullable = false)
    private String projectName;

    @Column(name = "customer_name", length = 100, nullable = false)
    private String customerName;

    @Column(name = "phone_number", length = 32, nullable = true)
    private String phoneNumber;

    @Column(name = "fax_number", length = 32, nullable = true)
    private String faxNumber;

    @Column(name = "email", length = 254, nullable = true)
    private String email;

    @Column(name = "notes", length = 254, nullable = true)
    private String notes;

    @Column(name = "type", length = 10, nullable = true)
    private String type;

    @Column(name = "tax", length = 20, nullable = true)
    private String tax;

    @Column(name = "discount", length = 3, nullable = true)
    private Double discount;

    @Column(name = "status", length = 20, nullable = true)
    private String status;

    @Column(name = "sub_total")
    private BigDecimal subTotal;

    @Column(name = "discount_price")
    private BigDecimal discountPrice;

    @Column(name = "tax_price")
    private BigDecimal taxPrice;

    @Column(name = "final_price")
    private BigDecimal finalPrice;

    @ManyToOne
    @JoinColumn(name = "approval")
    private Role approval;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "file_attach_price_quotation", joinColumns = {
            @JoinColumn(name = "price_quotation_id", referencedColumnName = "id") }, inverseJoinColumns = {
            @JoinColumn(name = "file_attach_id", referencedColumnName = "id") })
    private List<FileAttach> fileAttaches;

    @Column(name = "po", length = 50, nullable = true)
    private String po;

    @Column(name = "attn", length = 50, nullable = true)
    private String attn;

    @Column(name = "cc", length = 50, nullable = true)
    private String cc;

    public PriceQuotation() {
    }

    public PriceQuotation(Long id, String pqNumber, String prNumber, User accountExecutive, User salesAdmin, Customer customer, String projectName, String customerName, String phoneNumber, String faxNumber, String email, String notes, String type, String tax, Double discount, String status, BigDecimal subTotal, BigDecimal discountPrice, BigDecimal taxPrice, BigDecimal finalPrice, Role approval, List<FileAttach> fileAttaches, String po, String attn, String cc) {
        this.id = id;
        this.pqNumber = pqNumber;
        this.prNumber = prNumber;
        this.accountExecutive = accountExecutive;
        this.salesAdmin = salesAdmin;
        this.customer = customer;
        this.projectName = projectName;
        this.customerName = customerName;
        this.phoneNumber = phoneNumber;
        this.faxNumber = faxNumber;
        this.email = email;
        this.notes = notes;
        this.type = type;
        this.tax = tax;
        this.discount = discount;
        this.status = status;
        this.subTotal = subTotal;
        this.discountPrice = discountPrice;
        this.taxPrice = taxPrice;
        this.finalPrice = finalPrice;
        this.approval = approval;
        this.fileAttaches = fileAttaches;
        this.po = po;
        this.attn = attn;
        this.cc = cc;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPqNumber() {
        return pqNumber;
    }

    public void setPqNumber(String pqNumber) {
        this.pqNumber = pqNumber;
    }

    public String getPrNumber() {
        return prNumber;
    }

    public void setPrNumber(String prNumber) {
        this.prNumber = prNumber;
    }

    public User getAccountExecutive() {
        return accountExecutive;
    }

    public void setAccountExecutive(User accountExecutive) {
        this.accountExecutive = accountExecutive;
    }

    public User getSalesAdmin() {
        return salesAdmin;
    }

    public void setSalesAdmin(User salesAdmin) {
        this.salesAdmin = salesAdmin;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(BigDecimal discountPrice) {
        this.discountPrice = discountPrice;
    }

    public BigDecimal getTaxPrice() {
        return taxPrice;
    }

    public void setTaxPrice(BigDecimal taxPrice) {
        this.taxPrice = taxPrice;
    }

    public BigDecimal getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(BigDecimal finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Role getApproval() {
        return approval;
    }

    public void setApproval(Role approval) {
        this.approval = approval;
    }

    public List<FileAttach> getFileAttaches() {
        return fileAttaches;
    }

    public void setFileAttaches(List<FileAttach> fileAttaches) {
        this.fileAttaches = fileAttaches;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public String getAttn() {
        return attn;
    }

    public void setAttn(String attn) {
        this.attn = attn;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    @Override
    public String toString() {
        return "PriceQuotation{" +
                "id=" + id +
                ", pqNumber='" + pqNumber + '\'' +
                ", prNumber='" + prNumber + '\'' +
                ", accountExecutive=" + accountExecutive +
                ", salesAdmin=" + salesAdmin +
                ", customer=" + customer +
                ", projectName='" + projectName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", faxNumber='" + faxNumber + '\'' +
                ", email='" + email + '\'' +
                ", notes='" + notes + '\'' +
                ", type='" + type + '\'' +
                ", tax='" + tax + '\'' +
                ", discount=" + discount +
                ", status='" + status + '\'' +
                ", subTotal=" + subTotal +
                ", discountPrice=" + discountPrice +
                ", taxPrice=" + taxPrice +
                ", finalPrice=" + finalPrice +
                ", approval=" + approval +
                ", fileAttaches=" + fileAttaches +
                ", po='" + po + '\'' +
                ", attn='" + attn + '\'' +
                ", cc='" + cc + '\'' +
                '}';
    }
}
