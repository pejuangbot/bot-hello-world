package com.nostratech.nurindo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "product")
public class Product extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_code", length = 50, nullable = false)
    private String productCode;

    @Column(length = 100, nullable = false)
    private String product;

    @Column(length = 100, nullable = true)
    private Integer height;

    @Column(length = 100, nullable = true)
    private Integer width;

    @Column(nullable = false)
    private Integer stock;

    @Column(name = "unit_measure", length = 100)
    private String unitMeasure;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "material_product", joinColumns = {
            @JoinColumn(name = "product_id", referencedColumnName = "id") }, inverseJoinColumns = {
            @JoinColumn(name = "material_id", referencedColumnName = "id") })
    private List<Material> materials;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinTable(name = "printer_product", joinColumns = {
            @JoinColumn(name = "product_id", referencedColumnName = "id") }, inverseJoinColumns = {
            @JoinColumn(name = "printer_id", referencedColumnName = "id") })
    private List<Printer> printers;

    @Column(length = 7, nullable = true)
    private String type;

    @Column(name = "msrp", nullable = true)
    private BigDecimal msrp;

    @Column(name = "bottom_price_ae", nullable = true)
    private BigDecimal bottomPriceAE;

    @Column(name = "bottom_price_sm", nullable = true)
    private BigDecimal bottomPriceSM;

    @Column(name = "bottom_price_sa", nullable = true)
    private BigDecimal bottomPriceSA;

    public Product() {
    }

    public Product(Long id, String productCode, String product, Integer height, Integer width, Integer stock, String unitMeasure, List<Material> materials, List<Printer> printers, String type, BigDecimal msrp, BigDecimal bottomPriceAE, BigDecimal bottomPriceSM, BigDecimal bottomPriceSA) {
        this.id = id;
        this.productCode = productCode;
        this.product = product;
        this.height = height;
        this.width = width;
        this.stock = stock;
        this.unitMeasure = unitMeasure;
        this.materials = materials;
        this.printers = printers;
        this.type = type;
        this.msrp = msrp;
        this.bottomPriceAE = bottomPriceAE;
        this.bottomPriceSM = bottomPriceSM;
        this.bottomPriceSA = bottomPriceSA;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getUnitMeasure() {
        return unitMeasure;
    }

    public void setUnitMeasure(String unitMeasure) {
        this.unitMeasure = unitMeasure;
    }

    public List<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public List<Printer> getPrinters() {
        return printers;
    }

    public void setPrinters(List<Printer> printers) {
        this.printers = printers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public BigDecimal getBottomPriceAE() {
        return bottomPriceAE;
    }

    public void setBottomPriceAE(BigDecimal bottomPriceAE) {
        this.bottomPriceAE = bottomPriceAE;
    }

    public BigDecimal getBottomPriceSM() {
        return bottomPriceSM;
    }

    public void setBottomPriceSM(BigDecimal bottomPriceSM) {
        this.bottomPriceSM = bottomPriceSM;
    }

    public BigDecimal getBottomPriceSA() {
        return bottomPriceSA;
    }

    public void setBottomPriceSA(BigDecimal bottomPriceSA) {
        this.bottomPriceSA = bottomPriceSA;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productCode='" + productCode + '\'' +
                ", product='" + product + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", stock=" + stock +
                ", unitMeasure='" + unitMeasure + '\'' +
                ", materials=" + materials +
                ", printers=" + printers +
                ", type='" + type + '\'' +
                ", msrp=" + msrp +
                ", bottomPriceAE=" + bottomPriceAE +
                ", bottomPriceSM=" + bottomPriceSM +
                ", bottomPriceSA=" + bottomPriceSA +
                '}';
    }
}
