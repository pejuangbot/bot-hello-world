package com.nostratech.nurindo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "product_role")
public class ProductRole {

    @Id
    @GeneratedValue
    @Column(name = "product_role_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    private Customer customer;

    @Column(name = "bottom_price", nullable = false)
    private BigDecimal bottomPrice;

    @Column(nullable = false)
    private BigDecimal msrp;

    public ProductRole() {
    }

    public ProductRole(Long id, Product product, Role role, Customer customer, BigDecimal bottomPrice, BigDecimal msrp) {
        this.id = id;
        this.product = product;
        this.role = role;
        this.customer = customer;
        this.bottomPrice = bottomPrice;
        this.msrp = msrp;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public BigDecimal getBottomPrice() {
        return bottomPrice;
    }

    public void setBottomPrice(BigDecimal bottomPrice) {
        this.bottomPrice = bottomPrice;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "ProductRole{" +
                "id=" + id +
                "product=" + product +
                ", role=" + role +
                ", customer=" + customer +
                ", bottomPrice=" + bottomPrice +
                ", msrp=" + msrp +
                '}';
    }
}
