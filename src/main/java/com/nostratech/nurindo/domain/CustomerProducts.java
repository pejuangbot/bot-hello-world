package com.nostratech.nurindo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "customer_products")
public class CustomerProducts extends AbstractAuditingEntity{

    @Id
    @GeneratedValue
    @Column(name = "customer_products_id")
    private Long id;

    @ManyToOne
    @JsonIgnoreProperties("")
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JsonIgnoreProperties("")
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "agreed_price", nullable = true)
    private BigDecimal agreedPrice;

    public CustomerProducts() {
    }

    public CustomerProducts(Long id, Product product, Customer customer, BigDecimal agreedPrice) {
        this.id = id;
        this.product = product;
        this.customer = customer;
        this.agreedPrice = agreedPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getAgreedPrice() {
        return agreedPrice;
    }

    public void setAgreedPrice(BigDecimal agreedPrice) {
        this.agreedPrice = agreedPrice;
    }

    @Override
    public String toString() {
        return "CustomerProducts{" +
                "id=" + id +
                ", product=" + product +
                ", customer=" + customer +
                ", agreedPrice=" + agreedPrice +
                '}';
    }
}
