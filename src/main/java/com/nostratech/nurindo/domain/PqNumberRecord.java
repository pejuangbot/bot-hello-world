package com.nostratech.nurindo.domain;


import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "pq_number_record")
public class PqNumberRecord extends AbstractAuditingEntity implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 50)
    @Column(name = "number_generated")
    private String numberGenerated;

    @Size(max = 5)
    @Column(name = "number")
    private String number;

    @Size(max = 2)
    @Column(name = "month")
    private String month;

    @Size(max = 2)
    @Column(name = "year")
    private String year;

    @Size(max = 20)
    @Column(name = "number_type")
    private String numberType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumberGenerated() {
        return numberGenerated;
    }

    public void setNumberGenerated(String numberGenerated) {
        this.numberGenerated = numberGenerated;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getNumberType() {
        return numberType;
    }

    public void setNumberType(String numberType) {
        this.numberType = numberType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PqNumberRecord pqNumberRecord = (PqNumberRecord) o;
        return !(pqNumberRecord.getId() == null || getId() == null) && Objects.equals(getId(), pqNumberRecord.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PqNumberRecord{" +
                "id=" + id +
                ", numberGenerated=" + numberGenerated +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", numberType='" + numberType + '\'' +
                '}';
    }
}
