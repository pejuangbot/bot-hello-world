package com.nostratech.nurindo.domain;


/**
 * A Customer model.
 */


import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "customer")
@DynamicUpdate
public class Customer extends AbstractAuditingEntity implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 32)
    @Column(name = "last_updated_process", length = 32)
    private String lastUpdateProcess;

    @Column(name = "is_deleted")
    private Integer isDeleted;

    @Size(max = 32)
    @Column(name = "customer_code", length = 32)
    private String customerCode;

    @Size(max = 64)
    @Column(name = "customer_name", length = 64)
    private String customerName;

    @Size(max = 128)
    @Column(name = "address", length = 128)
    private String address;

    @Size(max = 16)
    @Column(name = "city_code", length = 16)
    private String cityCode;

    @Size(max = 5)
    @Column(name = "zip_code", length = 5)
    private String zipCode;

    @Size(max = 20)
    @Column(name = "npwp_id", length = 20)
    private String npwpId;

    @Size(max = 128)
    @Column(name = "npwp_address", length = 128)
    private String npwpAddress;

    @Size(max = 20)
    @Column(name = "npwp_city_code", length = 20)
    private String npwpCityCode;

    @Size(max = 5)
    @Column(name = "npwp_zip_code", length = 5)
    private String npwpZipCode;

    @Size(max = 64)
    @Column(name = "npwp_name", length = 64)
    private String npwpName;

    @Size(max = 24)
    @Column(name = "quotation_format", length = 24)
    private String quotationFormat;

    @Size(max = 16)
    @Column(name = "group_code", length = 16)
    private String groupCode;

    @Size(max = 32)
    @Column(name = "status", length = 32)
    private String status;

    @Column(name = "need_numbering")
    private Integer needNumbering;

    @Size(max = 16)
    @Column(name = "top", length = 26)
    private String top;

    @Size(max = 32)
    @Column(name = "top_um", length = 32)
    private String topUm;

    @Size(max = 128)
    @Column(name = "address_detail", length = 128)
    private String addressDetail;

    @Size(max = 16)
    @Column(name = "sales_code", length = 16)
    private String salesCode;

    @Size(max = 32)
    @Column(name = "customer_type", length = 32)
    private String customerType;

    @Column(name="term_of_payment")
    private  String termOfPayment;

    @Column(name="currency", nullable = true)
    private  String currency;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLastUpdateProcess() {
        return lastUpdateProcess;
    }

    public void setLastUpdateProcess(String lastUpdateProcess) {
        this.lastUpdateProcess = lastUpdateProcess;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getNpwpId() {
        return npwpId;
    }

    public void setNpwpId(String npwpId) {
        this.npwpId = npwpId;
    }

    public String getNpwpAddress() {
        return npwpAddress;
    }

    public void setNpwpAddress(String npwpAddress) {
        this.npwpAddress = npwpAddress;
    }

    public String getNpwpCityCode() {
        return npwpCityCode;
    }

    public void setNpwpCityCode(String npwpCityCode) {
        this.npwpCityCode = npwpCityCode;
    }

    public String getNpwpZipCode() {
        return npwpZipCode;
    }

    public void setNpwpZipCode(String npwpZipCode) {
        this.npwpZipCode = npwpZipCode;
    }

    public String getNpwpName() {
        return npwpName;
    }

    public void setNpwpName(String npwpName) {
        this.npwpName = npwpName;
    }

    public String getQuotationFormat() {
        return quotationFormat;
    }

    public void setQuotationFormat(String quotationFormat) {
        this.quotationFormat = quotationFormat;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getNeedNumbering() {
        return needNumbering;
    }

    public void setNeedNumbering(Integer needNumbering) {
        this.needNumbering = needNumbering;
    }

    public String getTop() {
        return top;
    }

    public void setTop(String top) {
        this.top = top;
    }

    public String getTopUm() {
        return topUm;
    }

    public void setTopUm(String topUm) {
        this.topUm = topUm;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getSalesCode() {
        return salesCode;
    }

    public void setSalesCode(String salesCode) {
        this.salesCode = salesCode;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getTermOfPayment() {
        return termOfPayment;
    }

    public void setTermOfPayment(String termOfPayment) {
        this.termOfPayment = termOfPayment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", lastUpdateProcess='" + lastUpdateProcess + '\'' +
                ", isDeleted=" + isDeleted +
                ", customerCode='" + customerCode + '\'' +
                ", customerName='" + customerName + '\'' +
                ", address='" + address + '\'' +
                ", cityCode='" + cityCode + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", npwpId='" + npwpId + '\'' +
                ", npwpAddress='" + npwpAddress + '\'' +
                ", npwpCityCode='" + npwpCityCode + '\'' +
                ", npwpZipCode='" + npwpZipCode + '\'' +
                ", npwpName='" + npwpName + '\'' +
                ", quotationFormat='" + quotationFormat + '\'' +
                ", groupCode='" + groupCode + '\'' +
                ", status='" + status + '\'' +
                ", needNumbering=" + needNumbering +
                ", top='" + top + '\'' +
                ", topUm='" + topUm + '\'' +
                ", addressDetail='" + addressDetail + '\'' +
                ", salesCode='" + salesCode + '\'' +
                ", customerType='" + customerType + '\'' +
                ", termOfPayment='" + termOfPayment + '\'' +
                ", currency='" + currency + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Customer customer = (Customer) o;
        return !(customer.getId() == null || getId() == null) && Objects.equals(getId(), customer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }
}
