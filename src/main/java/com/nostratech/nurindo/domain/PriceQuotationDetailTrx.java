package com.nostratech.nurindo.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "price_quotation_detail_trx")
public class PriceQuotationDetailTrx extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @Column(name = "msrp", nullable = false)
    private BigDecimal msrp;

    @Column(name = "bottom_price", nullable = false)
    private BigDecimal bottomPrice;

    @Column(name = "height", nullable = true)
    private Integer height;

    @Column(name = "weight", nullable = true)
    private Integer weight;

    @ManyToOne
    @JoinColumn(name = "material")
    private Material material;

    @ManyToOne
    @JoinColumn(name = "printer")
    private Printer printer;

    @Column(name = "resolution", nullable = true)
    private String resolution;

    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "price", nullable = false)
    private BigDecimal price;

    @Column(name = "total_price", nullable = false)
    private BigDecimal totalPrice;

    @ManyToOne
    @JoinColumn(name = "price_quotation")
    private PriceQuotation priceQuotation;

    public PriceQuotationDetailTrx() {
    }

    public PriceQuotationDetailTrx(Long id, String name, BigDecimal msrp, BigDecimal bottomPrice, Integer height, Integer weight, Material material, Printer printer, String resolution, Integer quantity, BigDecimal price, BigDecimal totalPrice, PriceQuotation priceQuotation) {
        this.id = id;
        this.name = name;
        this.msrp = msrp;
        this.bottomPrice = bottomPrice;
        this.height = height;
        this.weight = weight;
        this.material = material;
        this.printer = printer;
        this.resolution = resolution;
        this.quantity = quantity;
        this.price = price;
        this.totalPrice = totalPrice;
        this.priceQuotation = priceQuotation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public Printer getPrinter() {
        return printer;
    }

    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

    public String getResolution() {
        return resolution;
    }

    public void setResolution(String resolution) {
        this.resolution = resolution;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public PriceQuotation getPriceQuotation() {
        return priceQuotation;
    }

    public void setPriceQuotation(PriceQuotation priceQuotation) {
        this.priceQuotation = priceQuotation;
    }

    public BigDecimal getMsrp() {
        return msrp;
    }

    public void setMsrp(BigDecimal msrp) {
        this.msrp = msrp;
    }

    public BigDecimal getBottomPrice() {
        return bottomPrice;
    }

    public void setBottomPrice(BigDecimal bottomPrice) {
        this.bottomPrice = bottomPrice;
    }

    @Override
    public String toString() {
        return "PriceQuotationDetailTrx{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", msrp=" + msrp +
                ", bottomPrice=" + bottomPrice +
                ", height=" + height +
                ", weight=" + weight +
                ", material='" + material + '\'' +
                ", printer='" + printer + '\'' +
                ", resolution='" + resolution + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", totalPrice=" + totalPrice +
                ", priceQuotation=" + priceQuotation +
                '}';
    }
}
