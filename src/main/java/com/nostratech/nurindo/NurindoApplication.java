package com.nostratech.nurindo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NurindoApplication {

	public static void main(String[] args) {
		SpringApplication.run(NurindoApplication.class, args);
	}
}
