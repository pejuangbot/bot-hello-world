package com.nostratech.nurindo.util;

import com.nostratech.nurindo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
@PropertySource("classpath:configuration/email.properties")
public class EmailUtil {
    @Autowired
    private JavaMailSender sender;

    @Value(value = "${url.master}")
    String urlMaster;

    @Value(value = "${url.path}")
    String urlPathr;

    public Boolean sendEmail(String recepients, String subject, String text){
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setTo(recepients);
            helper.setSubject(subject);
            helper.setText(text, true);
        } catch (MessagingException e) {
            return false;
        }
        sender.send(message);
        return Boolean.TRUE;
    }


}
