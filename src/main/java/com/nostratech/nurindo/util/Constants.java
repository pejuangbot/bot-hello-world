package com.nostratech.nurindo.util;

/**
 * Application constants.
 */
public class Constants {

    public static final class User {
        // Regex for acceptable logins
        public static final String USERNAME_REGEX = "^[_.@A-Za-z0-9-]*$";

        public static final String SYSTEM_ACCOUNT = "system";
        public static final String ANONYMOUS_USER = "anonymoususer";
        public static final String DEFAULT_LANGUAGE = "en";
    }

    public static final class PageParameter {
        public static final String LIST_DATA = "listData";
        public static final String TOTAL_PAGES = "totalPages";
        public static final String TOTAL_ELEMENTS = "totalElements";
    }

    public static final class PriceQuotationStatus {
        public static final String NEW = "New";
        public static final String SO_CREATED = "SO Created";
        public static final String DRAFT = "Draft";
        public static final String DELETED = "Deleted";
        public static final String NEED_APPROVAL = "Need Approval";
    }

    public static final class SalesOrderStatus {
        public static final String NEW = "New";
        public static final String JO_CREATED = "JO Created";
        public static final String DELETED = "Deleted";
    }

    public static final class Tax {
        public static final String PPN_10 = "PPN (10%)";
        public static final String PPH_5 = "PPH (5%)";
        public static final String PPN_PPH = "PPN & PPH (10+5%)";
    }

    public static final class ProductType {
        public static final String PRODUCT = "Product";
        public static final String SERVICE = "Service";
    }

    public static final class Role {
        public static final String SUPERVISOR_ADMINISTRATOR = "Supervisor Administrator";
        public static final String SALES_MANAGER = "Sales Manager";
        public static final String ACCOUNT_EXCUTIVE = "Account Executive";
        public static final String SALES_ADMIN = "Sales Admin";
    }

}
