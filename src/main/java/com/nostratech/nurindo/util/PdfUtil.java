package com.nostratech.nurindo.util;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
@PropertySource("classpath:configuration/file.properties")
public class PdfUtil {

    public static final Logger logger = LoggerFactory.getLogger(PdfUtil.class);

    final static String NUMBER_FORMAT = "#,##0";
    final static String DATE_FORMAT = "dd-MMM-yyyy";

    @Value("${report.pathDestination}")
    String pathPdfFile;

    @Value("${report.pathTemplate}")
    String pathTemplateFile;

    public String generatePdfFileJasperFromListObject(HashMap params, List<Object> detail, String fileName, String templateName) {
        String dest = pathPdfFile + fileName;
        String template = pathTemplateFile + templateName;
        try {
            JRDataSource ds = new JRBeanCollectionDataSource(detail);
            if (params == null) {
                params = new HashMap();
            }

            params.put("ObjectDataSource", ds);
            JasperPrint jp = JasperFillManager.fillReport(template, params, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jp, dest);
        } catch (Exception e) {

            logger.info(e.getMessage());
        }
        return dest;
    }
}
