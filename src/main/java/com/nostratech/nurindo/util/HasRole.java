package com.nostratech.nurindo.util;


import com.nostratech.nurindo.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Component
@Slf4j
public class HasRole {

    public boolean check(String... clientIdParams){
        List<String> clientIdListParam = Arrays.asList(clientIdParams);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        try{
            User user = (User) authentication.getPrincipal();
            List<String> userClientId = Arrays.asList(user.getEmail());
            return !Collections.disjoint(userClientId, clientIdListParam);
        }catch (Exception e){
            log.error("HasClientId : {}", e.getMessage());
            return false;
        }
    }

}
