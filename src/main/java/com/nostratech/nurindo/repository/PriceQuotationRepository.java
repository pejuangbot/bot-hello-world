package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Price Quotation entity.
 */
@Repository
public interface PriceQuotationRepository extends JpaRepository<PriceQuotation, Long> {

    Page<PriceQuotation> findByPqNumberLikeIgnoreCaseAndStatusNotAndAccountExecutiveIsNotNull(String pqNumber, String status, Pageable pageable);

    Page<PriceQuotation> findByPqNumberLikeIgnoreCaseAndStatusNot(String pqNumber, String status, Pageable pageable);

    Page<PriceQuotation> findByPqNumberLikeIgnoreCaseAndApprovalAndStatusNot(String pqNumber, Role role, String status, Pageable pageable);

    Page<PriceQuotation> findByPqNumberLikeIgnoreCaseAndCustomerAndStatusNot(String pqNumber, Customer customer, String status, Pageable pageable);

}
