package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    Page<City> findByCityNameLikeIgnoreCaseAndIslandLikeIgnoreCaseAndCityCodeLikeIgnoreCase(String cityName, String island, String cityCode, Pageable pageable);

    Optional<City> findByCityCodeIgnoreCase(String cityCode);
}
