package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository <Role, Long> {

    Optional<Role> findOneByName(String name);

    List<Role> findByNameLikeOrderByLastModifiedDateDesc(String name);

}
