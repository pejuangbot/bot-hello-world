package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.domain.ProductRole;
import com.nostratech.nurindo.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRoleRepository extends JpaRepository<ProductRole, Long> {

    List<ProductRole> findByProduct(Product product);
    ProductRole findByRoleAndProduct(Role role, Product product);

}
