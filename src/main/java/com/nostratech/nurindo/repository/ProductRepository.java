package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    Product findByProductCode(String productCode);

    Product findByProductIgnoreCase(String productName);

    Page<Product> findByProductCodeLikeIgnoreCaseAndProductLikeIgnoreCase(String productCode, String productName, Pageable pageable);

}
