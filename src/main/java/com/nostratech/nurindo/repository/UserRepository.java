package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findOneByUsername(String username);

    Optional<User> findOneByEmailIgnoreCase(String email);

    Page<User> findByUsernameLikeIgnoreCase(String username, Pageable pageable);

    Page<User> findByRolesNameLikeIgnoreCase(String roles, Pageable pageable);

    List<User> findByRolesNameIgnoreCase(String roles);
}
