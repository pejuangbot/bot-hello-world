package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.FileAttach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileAttachRepository extends JpaRepository<FileAttach, Long> {

    

}
