package com.nostratech.nurindo.repository;


import com.nostratech.nurindo.domain.PqNumberRecord;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface PqNumberRecordRepository extends JpaRepository<PqNumberRecord, Long>{

    Page<PqNumberRecord> findByNumberTypeLikeIgnoreCaseAndMonth(String numberType, String month, Pageable pageable);

    Page<PqNumberRecord> findByNumberTypeLikeIgnoreCase(String numberType, Pageable pageable);

    @Query(value = "SELECT getpqnumber(:numbertype)",
    nativeQuery = true)
    String getPqNumber(@Param("numbertype") String numbertype);

    @Query(value = "SELECT createpqnumber(:cust_id)",
            nativeQuery = true)
    String createPqNumber(@Param("cust_id") String customerId);

    @Query(value = "SELECT updatepqnumber(:cust_id)",
            nativeQuery = true)
    String updatePqNumber(@Param("cust_id") String customerId);

    @Query(value="SELECT resetpqnumber()",
            nativeQuery = true)
    String resetPqNumber();


}
