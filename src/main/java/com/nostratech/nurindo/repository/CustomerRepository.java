package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{
    Page<Customer> findByCustomerCodeLike(String customerCode, Pageable pageable);

    Optional<Customer> findByIdAndCustomerTypeIgnoreCase(Long customerId, String customerType);

    Page<Customer> findByCustomerNameLikeIgnoreCase(String customerName, Pageable pageable);

    Page<Customer> findByCustomerNameLikeIgnoreCaseAndCustomerTypeIgnoreCase(String customerName, String customerType, Pageable pageable);


    List<Customer> findByCustomerCodeLike(String customerCode);

}
