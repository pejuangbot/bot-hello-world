package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.SalesOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface SalesOrderRepository extends JpaRepository<SalesOrder, Long> {

    Page<SalesOrder> findByPriceQuotationPqNumberLikeAndStatusNot(String pqNumber, String status, Pageable pageable);

    SalesOrder findByPriceQuotation(PriceQuotation priceQuotation);


}
