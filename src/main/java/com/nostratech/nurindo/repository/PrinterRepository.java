package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.Material;
import com.nostratech.nurindo.domain.Printer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrinterRepository extends JpaRepository<Printer, Long> {

}
