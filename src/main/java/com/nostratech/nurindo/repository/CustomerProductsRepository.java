package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Repository
public interface CustomerProductsRepository extends JpaRepository<CustomerProducts, Long> {

    List<CustomerProducts> findByProduct(Product product);
    List<CustomerProducts> findByCustomerId(Long customerId);
//    List<CustomerProducts> findByCustomerIdAndProductProductLike(Long customerId, String productName);
    Page<CustomerProducts> findByCustomerIdAndProductProductLikeIgnoreCaseAndProductTypeLikeIgnoreCase(Long customerId, String productName, String productType, Pageable pageable);
//    CustomerProducts findByRoleAndProduct(Role role, Product product);

}
