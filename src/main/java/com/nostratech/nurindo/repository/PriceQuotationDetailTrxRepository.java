package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.PriceQuotationDetailTrx;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data JPA repository for the Price Quotation Detail Transaction entity.
 */
@Repository
public interface PriceQuotationDetailTrxRepository extends JpaRepository<PriceQuotationDetailTrx, Long> {

    List<PriceQuotationDetailTrx> findByPriceQuotation(PriceQuotation priceQuotation);

}
