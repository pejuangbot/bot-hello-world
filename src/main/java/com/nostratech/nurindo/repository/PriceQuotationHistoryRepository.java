package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.PriceQuotationHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Price Quotation History entity.
 */
@Repository
public interface PriceQuotationHistoryRepository extends JpaRepository<PriceQuotationHistory, Long> {

}
