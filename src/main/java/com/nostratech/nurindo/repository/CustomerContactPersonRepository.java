package com.nostratech.nurindo.repository;

import com.nostratech.nurindo.domain.CustomerContactPerson;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerContactPersonRepository extends JpaRepository<CustomerContactPerson, Long> {

    Page<CustomerContactPerson> findByCustomerId(String customerId, Pageable pageable);

    List<CustomerContactPerson> findByCustomerId(Long customerId);

    @Modifying
    public void deleteByCustomerId(Long customerId);
}
