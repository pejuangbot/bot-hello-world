package com.nostratech.nurindo.config;

import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.MenuListDTO;
import com.nostratech.nurindo.service.dto.RoleListDTO;
import com.nostratech.nurindo.service.mapper.MenuListMapper;
import com.nostratech.nurindo.service.mapper.RoleListMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@Transactional
public class RoleListConfig {

    @Transactional(readOnly = true)
    public List<RoleListDTO> getRoleAndMenu(OAuth2Authentication authentication) {
        User user = (User) authentication.getPrincipal();
        List<RoleListDTO> roleList = user.getRoles().stream()
                .map(role -> {
                    RoleListDTO roleListDTO = RoleListMapper.INSTANCE.toDto(role);
                    List<MenuListDTO> menuListDTOS = role.getPrivileges().stream()
                            .map(privilege -> {
                                MenuListDTO menuListDTO = MenuListMapper.INSTANCE.toDto(privilege.getMenu());
                                menuListDTO.setCreate(privilege.getCreate());
                                menuListDTO.setDelete(privilege.getDelete());
                                menuListDTO.setRead(privilege.getRead());
                                menuListDTO.setUpdate(privilege.getUpdate());
                                return menuListDTO;
                            }).collect(Collectors.toCollection(LinkedList::new));
                    roleListDTO.setMenuList(menuListDTOS);
                    return roleListDTO;
                }).collect(Collectors.toCollection(LinkedList::new));

        return roleList;
    }
}
