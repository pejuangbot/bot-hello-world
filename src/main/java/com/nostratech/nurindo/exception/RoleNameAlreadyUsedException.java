package com.nostratech.nurindo.exception;

import com.nostratech.nurindo.util.ErrorConstants;

public class RoleNameAlreadyUsedException extends BadRequestAlertException {

    private static final long serialVersionUID = 1L;

    public RoleNameAlreadyUsedException() {
        super(ErrorConstants.ROLENAME_ALREADY_USED_TYPE, "Rolename already used!", "userManagement", "rolenameexists");
    }
}
