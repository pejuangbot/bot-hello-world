package com.nostratech.nurindo.exception;

import com.nostratech.nurindo.service.dto.CustomHttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;


@ControllerAdvice
public class CustomValidationExceptionHandling {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<CustomHttpServletResponse> invalidInput(MethodArgumentNotValidException ex, HttpServletRequest requestToCache, HttpServletResponse responseToCache) {
        BindingResult result = ex.getBindingResult();
        CustomHttpServletResponse response = new CustomHttpServletResponse();
        response.setTimestamp(new Date());
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.setError("Validation Error");
        response.setMessage(result.getFieldError().getField() + " " + result.getFieldError().getDefaultMessage());
        response.setPath(requestToCache.getServletPath());
        return new ResponseEntity<CustomHttpServletResponse>(response, HttpStatus.BAD_REQUEST);
    }
}