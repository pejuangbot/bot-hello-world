#!/bin/sh

WORKING_DIR="/home/nostra"
APP="nurindo-dev"
VERSION="0.0.1-SNAPSHOT"
APP_NAME="$WORKING_DIR/$APP/nurindo-$VERSION.jar"
ROOT_APP="$WORKING_DIR/$APP"
CONF="-server -XX:MaxNewSize=64m -XX:NewSize=64m -Xms256m -Xmx700m -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5006"

JVM_ARGSD="$CONF -Dspring.profiles.active=development"
JVM_ARGSS="$CONF -Dspring.profiles.active=staging"
JVM_ARGSP="$CONF -Dspring.profiles.active=production"
JVM_ARGST="$CONF -Dspring.profiles.active=testing"
DATE=`date '+%Y-%m-%d %H:%M:%S'`


if [ "$1" ]; then

        if [ "$1" = "start" ];then

        if [ "$2" = "development" ]; then
        echo "starting development $APP_NAME..."
        nohup java -DROOT_APP=$ROOT_APP -jar $JVM_ARGSD $APP_NAME > backend-$DATE.log &

        elif [ "$2" = "staging" ]; then
        echo "starting staging $APP_NAME..."
        nohup java -DROOT_APP=$ROOT_APP -jar $JVM_ARGSS $APP_NAME > backend-$DATE.log &

        elif [ "$2" = "production" ]; then
        echo "starting production $APP_NAME..."
        nohup java -DROOT_APP=$ROOT_APP -jar $JVM_ARGSP $APP_NAME > backend-$DATE.log &

        elif [ "$2" = "testing" ]; then
        echo "starting testing $APP_NAME..."
        nohup java -DROOT_APP=$ROOT_APP -jar $JVM_ARGST $APP_NAME > backend-$DATE.log &

        else
        echo "Please give argument 'development', 'testing' or 'production' ..."

        fi

        elif [ "$1" = "stop" ]; then
                echo "stopping $APP_NAME..."
                pkill -f $APP_NAME
                echo "$APP_NAME successfully stopped."

        fi


else
    echo "Please give first argument 'start' or 'stop' and the second argument is 'development', 'testing' or 'production'"
fi
