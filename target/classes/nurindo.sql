/*
 Navicat Premium Data Transfer

 Source Server         : Nurindo
 Source Server Type    : PostgreSQL
 Source Server Version : 100005
 Source Host           : localhost:5432
 Source Catalog        : nurindo
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100005
 File Encoding         : 65001

 Date: 10/10/2018 17:32:14
*/


-- ----------------------------
-- Sequence structure for menu_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."menu_id_seq";
CREATE SEQUENCE "public"."menu_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for privilege_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."privilege_id_seq";
CREATE SEQUENCE "public"."privilege_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for role_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."role_id_seq";
CREATE SEQUENCE "public"."role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."menu";
CREATE TABLE "public"."menu" (
  "id" int8 NOT NULL DEFAULT nextval('menu_id_seq'::regclass),
  "created_by" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" timestamp(6) NOT NULL,
  "last_modified_by" varchar(50) COLLATE "pg_catalog"."default",
  "last_modified_date" timestamp(6),
  "activated" bool NOT NULL,
  "description" varchar(100) COLLATE "pg_catalog"."default",
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "parent" int8,
  "version" int8
)
;
ALTER TABLE "public"."menu" OWNER TO "postgres";

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO "public"."menu" VALUES (1, 'system', '2018-10-10 17:14:34', 'system', '2018-10-10 17:14:39', 't', NULL, 'Sales Admin', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS "public"."oauth_client_details";
CREATE TABLE "public"."oauth_client_details" (
  "client_id" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "resource_ids" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "client_secret" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "scope" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "authorized_grant_types" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "web_server_redirect_uri" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "authorities" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "access_token_validity" int4,
  "refresh_token_validity" int4,
  "additional_information" varchar(4096) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "autoapprove" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;
ALTER TABLE "public"."oauth_client_details" OWNER TO "postgres";

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
BEGIN;
INSERT INTO "public"."oauth_client_details" VALUES ('adminapp', 'mw/adminapp,ms/admin,ms/user', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'role_admin,role_superadmin', 'authorization_code,password,refresh_token,implicit', NULL, NULL, 900, 3600, '{}', NULL);
COMMIT;

-- ----------------------------
-- Table structure for privilege
-- ----------------------------
DROP TABLE IF EXISTS "public"."privilege";
CREATE TABLE "public"."privilege" (
  "id" int8 NOT NULL DEFAULT nextval('privilege_id_seq'::regclass),
  "created_by" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" timestamp(6) NOT NULL,
  "last_modified_by" varchar(50) COLLATE "pg_catalog"."default",
  "last_modified_date" timestamp(6),
  "creates" bool NOT NULL,
  "deletes" bool NOT NULL,
  "reads" bool NOT NULL,
  "updates" bool NOT NULL,
  "menu_id" int8,
  "role_id" int8,
  "version" int8
)
;
ALTER TABLE "public"."privilege" OWNER TO "postgres";

-- ----------------------------
-- Records of privilege
-- ----------------------------
BEGIN;
INSERT INTO "public"."privilege" VALUES (1, 'system', '2018-10-10 17:15:30', 'system', '2018-10-10 17:15:36', 't', 't', 't', 't', 1, 1, 1);
COMMIT;

-- ----------------------------
-- Table structure for privilege_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."privilege_role";
CREATE TABLE "public"."privilege_role" (
  "role_id" int8 NOT NULL,
  "privilege_id" int8 NOT NULL
)
;
ALTER TABLE "public"."privilege_role" OWNER TO "postgres";

-- ----------------------------
-- Records of privilege_role
-- ----------------------------
BEGIN;
INSERT INTO "public"."privilege_role" VALUES (1, 1);
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS "public"."role";
CREATE TABLE "public"."role" (
  "id" int8 NOT NULL DEFAULT nextval('role_id_seq'::regclass),
  "created_by" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" timestamp(6) NOT NULL,
  "last_modified_by" varchar(50) COLLATE "pg_catalog"."default",
  "last_modified_date" timestamp(6),
  "activated" bool NOT NULL,
  "description" varchar(100) COLLATE "pg_catalog"."default",
  "name" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "version" int8
)
;
ALTER TABLE "public"."role" OWNER TO "postgres";

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO "public"."role" VALUES (2, 'system', '2018-10-10 17:12:01', 'system', '2018-10-10 17:12:06', 't', NULL, 'ROLE_USER', 1);
INSERT INTO "public"."role" VALUES (1, 'system', '2018-10-10 17:13:12', 'system', '2018-10-10 17:13:17', 't', NULL, 'ROLE_ADMIN', 1);
COMMIT;

-- ----------------------------
-- Table structure for role_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."role_user";
CREATE TABLE "public"."role_user" (
  "user_id" int8 NOT NULL,
  "role_id" int8 NOT NULL
)
;
ALTER TABLE "public"."role_user" OWNER TO "postgres";

-- ----------------------------
-- Records of role_user
-- ----------------------------
BEGIN;
INSERT INTO "public"."role_user" VALUES (1, 1);
COMMIT;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int8 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "created_by" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "created_date" timestamp(6) NOT NULL,
  "last_modified_by" varchar(50) COLLATE "pg_catalog"."default",
  "last_modified_date" timestamp(6),
  "activated" bool NOT NULL,
  "activation_key" varchar(20) COLLATE "pg_catalog"."default",
  "email" varchar(254) COLLATE "pg_catalog"."default",
  "first_name" varchar(50) COLLATE "pg_catalog"."default",
  "image_url" varchar(256) COLLATE "pg_catalog"."default",
  "lang_key" varchar(6) COLLATE "pg_catalog"."default",
  "last_name" varchar(50) COLLATE "pg_catalog"."default",
  "password_hash" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "username" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "version" int8,
  "account_expired" bool,
  "account_locked" bool,
  "credentials_expired" bool,
  "enabled" bool NOT NULL
)
;
ALTER TABLE "public"."users" OWNER TO "postgres";

-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN;
INSERT INTO "public"."users" VALUES (1, 'system', '2018-10-10 17:05:51', 'system', '2018-10-10 17:05:58', 't', '123456', 'yuki@nostratech.com', 'Yuki', NULL, NULL, 'Buwana', '{bcrypt}$2a$10$EOs8VROb14e7ZnydvXECA.4LoIhPOoFHKvVF/iBZ/ker17Eocz4Vi', 'yukibuwana', 1, 'f', 'f', 'f', 't');
COMMIT;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."menu_id_seq"
OWNED BY "public"."menu"."id";
SELECT setval('"public"."menu_id_seq"', 2, false);
ALTER SEQUENCE "public"."privilege_id_seq"
OWNED BY "public"."privilege"."id";
SELECT setval('"public"."privilege_id_seq"', 2, false);
ALTER SEQUENCE "public"."role_id_seq"
OWNED BY "public"."role"."id";
SELECT setval('"public"."role_id_seq"', 2, false);
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 2, false);

-- ----------------------------
-- Primary Key structure for table menu
-- ----------------------------
ALTER TABLE "public"."menu" ADD CONSTRAINT "menu_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table oauth_client_details
-- ----------------------------
ALTER TABLE "public"."oauth_client_details" ADD CONSTRAINT "oauth_client_details_pkey" PRIMARY KEY ("client_id");

-- ----------------------------
-- Primary Key structure for table privilege
-- ----------------------------
ALTER TABLE "public"."privilege" ADD CONSTRAINT "privilege_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE "public"."role" ADD CONSTRAINT "role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "uk_6dotkott2kjsp8vw4d0m25fb7" UNIQUE ("email");
ALTER TABLE "public"."users" ADD CONSTRAINT "uk_r43af9ap4edm43mmtq01oddj6" UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table privilege
-- ----------------------------
ALTER TABLE "public"."privilege" ADD CONSTRAINT "fkf0rapce3kwjefytyv5mmvew8u" FOREIGN KEY ("role_id") REFERENCES "public"."role" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."privilege" ADD CONSTRAINT "fkqckdmdk8jouw4r8o53uq88xlo" FOREIGN KEY ("menu_id") REFERENCES "public"."menu" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table privilege_role
-- ----------------------------
ALTER TABLE "public"."privilege_role" ADD CONSTRAINT "fkmjdxh3xvb9u84vp2nt7fi1fcb" FOREIGN KEY ("privilege_id") REFERENCES "public"."privilege" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."privilege_role" ADD CONSTRAINT "fks1shi5inbkewred4y50c9rihl" FOREIGN KEY ("role_id") REFERENCES "public"."role" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table role_user
-- ----------------------------
ALTER TABLE "public"."role_user" ADD CONSTRAINT "fkhvai2k29vlwpt9wod4sw4ghmn" FOREIGN KEY ("user_id") REFERENCES "public"."users" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."role_user" ADD CONSTRAINT "fkiqpmjd2qb4rdkej916ymonic6" FOREIGN KEY ("role_id") REFERENCES "public"."role" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
