package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Material;
import com.nostratech.nurindo.service.dto.MaterialDTO;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class MaterialMapperImpl implements MaterialMapper {

    @Override
    public Material toEntity(MaterialDTO dto, Material entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setId( dto.getId() );
        entity.setName( dto.getName() );

        return entity;
    }

    @Override
    public MaterialDTO toDto(Material entity, MaterialDTO dto) {
        if ( entity == null ) {
            return null;
        }

        dto.setId( entity.getId() );
        dto.setName( entity.getName() );

        return dto;
    }

    @Override
    public List<Material> toEntity(List<MaterialDTO> dto, List<Material> entity) {
        if ( dto == null ) {
            return null;
        }

        entity.clear();
        for ( MaterialDTO materialDTO : dto ) {
            entity.add( materialDTOToMaterial( materialDTO ) );
        }

        return entity;
    }

    @Override
    public List<MaterialDTO> toDto(List<Material> entity, List<MaterialDTO> dto) {
        if ( entity == null ) {
            return null;
        }

        dto.clear();
        for ( Material material : entity ) {
            dto.add( materialToMaterialDTO( material ) );
        }

        return dto;
    }

    protected Material materialDTOToMaterial(MaterialDTO materialDTO) {
        if ( materialDTO == null ) {
            return null;
        }

        Material material = new Material();

        material.setId( materialDTO.getId() );
        material.setName( materialDTO.getName() );

        return material;
    }

    protected MaterialDTO materialToMaterialDTO(Material material) {
        if ( material == null ) {
            return null;
        }

        MaterialDTO materialDTO = new MaterialDTO();

        materialDTO.setId( material.getId() );
        materialDTO.setName( material.getName() );

        return materialDTO;
    }
}
