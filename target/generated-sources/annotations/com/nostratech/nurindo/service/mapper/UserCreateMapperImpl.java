package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.RoleBaseDTO;
import com.nostratech.nurindo.service.dto.UserCreateDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class UserCreateMapperImpl implements UserCreateMapper {

    @Override
    public User toEntity(UserCreateDTO dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        user.setId( dto.getId() );
        user.setUsername( dto.getUsername() );
        user.setEmail( dto.getEmail() );
        user.setRoles( roleBaseDTOListToRoleList( dto.getRoles() ) );

        return user;
    }

    @Override
    public UserCreateDTO toDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserCreateDTO userCreateDTO = new UserCreateDTO();

        userCreateDTO.setUsername( entity.getUsername() );
        userCreateDTO.setEmail( entity.getEmail() );
        userCreateDTO.setId( entity.getId() );
        userCreateDTO.setRoles( roleListToRoleBaseDTOList( entity.getRoles() ) );

        return userCreateDTO;
    }

    @Override
    public List<User> toEntity(List<UserCreateDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( dtoList.size() );
        for ( UserCreateDTO userCreateDTO : dtoList ) {
            list.add( toEntity( userCreateDTO ) );
        }

        return list;
    }

    @Override
    public List<UserCreateDTO> toDto(List<User> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserCreateDTO> list = new ArrayList<UserCreateDTO>( entityList.size() );
        for ( User user : entityList ) {
            list.add( toDto( user ) );
        }

        return list;
    }

    protected Role roleBaseDTOToRole(RoleBaseDTO roleBaseDTO) {
        if ( roleBaseDTO == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( roleBaseDTO.getId() );
        role.setName( roleBaseDTO.getName() );

        return role;
    }

    protected List<Role> roleBaseDTOListToRoleList(List<RoleBaseDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Role> list1 = new ArrayList<Role>( list.size() );
        for ( RoleBaseDTO roleBaseDTO : list ) {
            list1.add( roleBaseDTOToRole( roleBaseDTO ) );
        }

        return list1;
    }

    protected RoleBaseDTO roleToRoleBaseDTO(Role role) {
        if ( role == null ) {
            return null;
        }

        RoleBaseDTO roleBaseDTO = new RoleBaseDTO();

        roleBaseDTO.setId( role.getId() );
        roleBaseDTO.setName( role.getName() );

        return roleBaseDTO;
    }

    protected List<RoleBaseDTO> roleListToRoleBaseDTOList(List<Role> list) {
        if ( list == null ) {
            return null;
        }

        List<RoleBaseDTO> list1 = new ArrayList<RoleBaseDTO>( list.size() );
        for ( Role role : list ) {
            list1.add( roleToRoleBaseDTO( role ) );
        }

        return list1;
    }
}
