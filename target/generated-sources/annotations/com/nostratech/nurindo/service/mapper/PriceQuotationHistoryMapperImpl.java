package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.PriceQuotationHistory;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PriceQuotationHistoryMapperImpl implements PriceQuotationHistoryMapper {

    @Override
    public PriceQuotationHistory toEntity(PriceQuotation dto, PriceQuotationHistory entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setVersion( dto.getVersion() );
        entity.setCreatedBy( dto.getCreatedBy() );
        entity.setCreatedDate( dto.getCreatedDate() );
        entity.setLastModifiedBy( dto.getLastModifiedBy() );
        entity.setLastModifiedDate( dto.getLastModifiedDate() );
        entity.setId( dto.getId() );
        entity.setPqNumber( dto.getPqNumber() );
        entity.setPrNumber( dto.getPrNumber() );
        entity.setAccountExecutive( dto.getAccountExecutive() );
        entity.setSalesAdmin( dto.getSalesAdmin() );
        entity.setCustomer( dto.getCustomer() );
        entity.setProjectName( dto.getProjectName() );
        entity.setCustomerName( dto.getCustomerName() );
        entity.setPhoneNumber( dto.getPhoneNumber() );
        entity.setFaxNumber( dto.getFaxNumber() );
        entity.setEmail( dto.getEmail() );
        entity.setNotes( dto.getNotes() );
        entity.setType( dto.getType() );

        return entity;
    }
}
