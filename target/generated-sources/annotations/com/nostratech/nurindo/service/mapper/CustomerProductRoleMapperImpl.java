package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.service.dto.CustomerProductRoleDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerProductRoleMapperImpl implements CustomerProductRoleMapper {

    @Override
    public List<CustomerProducts> toEntity(List<CustomerProductRoleDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CustomerProducts> list = new ArrayList<CustomerProducts>( dtoList.size() );
        for ( CustomerProductRoleDTO customerProductRoleDTO : dtoList ) {
            list.add( toEntity( customerProductRoleDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerProductRoleDTO> toDto(List<CustomerProducts> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerProductRoleDTO> list = new ArrayList<CustomerProductRoleDTO>( entityList.size() );
        for ( CustomerProducts customerProducts : entityList ) {
            list.add( toDto( customerProducts ) );
        }

        return list;
    }

    @Override
    public CustomerProducts toEntity(CustomerProductRoleDTO customerProductRoleDTO) {
        if ( customerProductRoleDTO == null ) {
            return null;
        }

        CustomerProducts customerProducts = new CustomerProducts();

        customerProducts.setAgreedPrice( customerProductRoleDTO.getAgreedPrice() );

        return customerProducts;
    }

    @Override
    public CustomerProductRoleDTO toDto(CustomerProducts customerProducts) {
        if ( customerProducts == null ) {
            return null;
        }

        CustomerProductRoleDTO customerProductRoleDTO = new CustomerProductRoleDTO();

        customerProductRoleDTO.setAgreedPrice( customerProducts.getAgreedPrice() );

        return customerProductRoleDTO;
    }
}
