package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Printer;
import com.nostratech.nurindo.service.dto.PrinterDTO;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PrinterMapperImpl implements PrinterMapper {

    @Override
    public Printer toEntity(PrinterDTO dto, Printer entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setId( dto.getId() );
        entity.setName( dto.getName() );

        return entity;
    }

    @Override
    public PrinterDTO toDto(Printer entity, PrinterDTO dto) {
        if ( entity == null ) {
            return null;
        }

        dto.setId( entity.getId() );
        dto.setName( entity.getName() );

        return dto;
    }

    @Override
    public List<Printer> toEntity(List<PrinterDTO> dto, List<Printer> entity) {
        if ( dto == null ) {
            return null;
        }

        entity.clear();
        for ( PrinterDTO printerDTO : dto ) {
            entity.add( printerDTOToPrinter( printerDTO ) );
        }

        return entity;
    }

    @Override
    public List<PrinterDTO> toDto(List<Printer> entity, List<PrinterDTO> dto) {
        if ( entity == null ) {
            return null;
        }

        dto.clear();
        for ( Printer printer : entity ) {
            dto.add( printerToPrinterDTO( printer ) );
        }

        return dto;
    }

    protected Printer printerDTOToPrinter(PrinterDTO printerDTO) {
        if ( printerDTO == null ) {
            return null;
        }

        Printer printer = new Printer();

        printer.setId( printerDTO.getId() );
        printer.setName( printerDTO.getName() );

        return printer;
    }

    protected PrinterDTO printerToPrinterDTO(Printer printer) {
        if ( printer == null ) {
            return null;
        }

        PrinterDTO printerDTO = new PrinterDTO();

        printerDTO.setId( printer.getId() );
        printerDTO.setName( printer.getName() );

        return printerDTO;
    }
}
