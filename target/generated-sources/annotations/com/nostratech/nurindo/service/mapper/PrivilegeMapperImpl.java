package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.domain.Privilege;
import com.nostratech.nurindo.service.dto.PrivilegeDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PrivilegeMapperImpl implements PrivilegeMapper {

    @Override
    public List<Privilege> toEntity(List<PrivilegeDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Privilege> list = new ArrayList<Privilege>( dtoList.size() );
        for ( PrivilegeDTO privilegeDTO : dtoList ) {
            list.add( toEntity( privilegeDTO ) );
        }

        return list;
    }

    @Override
    public List<PrivilegeDTO> toDto(List<Privilege> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<PrivilegeDTO> list = new ArrayList<PrivilegeDTO>( entityList.size() );
        for ( Privilege privilege : entityList ) {
            list.add( toDto( privilege ) );
        }

        return list;
    }

    @Override
    public Privilege toEntity(PrivilegeDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Privilege privilege = new Privilege();

        privilege.setMenu( privilegeDTOToMenu( dto ) );
        privilege.setId( dto.getId() );
        privilege.setCreate( dto.getCreate() );
        privilege.setRead( dto.getRead() );
        privilege.setUpdate( dto.getUpdate() );
        privilege.setDelete( dto.getDelete() );

        return privilege;
    }

    @Override
    public PrivilegeDTO toDto(Privilege entity) {
        if ( entity == null ) {
            return null;
        }

        PrivilegeDTO privilegeDTO = new PrivilegeDTO();

        Long id = entityMenuId( entity );
        if ( id != null ) {
            privilegeDTO.setMenu( id );
        }
        privilegeDTO.setId( entity.getId() );
        privilegeDTO.setCreate( entity.getCreate() );
        privilegeDTO.setRead( entity.getRead() );
        privilegeDTO.setUpdate( entity.getUpdate() );
        privilegeDTO.setDelete( entity.getDelete() );

        return privilegeDTO;
    }

    protected Menu privilegeDTOToMenu(PrivilegeDTO privilegeDTO) {
        if ( privilegeDTO == null ) {
            return null;
        }

        Menu menu = new Menu();

        menu.setId( privilegeDTO.getMenu() );

        return menu;
    }

    private Long entityMenuId(Privilege privilege) {
        if ( privilege == null ) {
            return null;
        }
        Menu menu = privilege.getMenu();
        if ( menu == null ) {
            return null;
        }
        Long id = menu.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
