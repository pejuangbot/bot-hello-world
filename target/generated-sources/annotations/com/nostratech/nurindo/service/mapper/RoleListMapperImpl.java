package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.service.dto.RoleListDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class RoleListMapperImpl implements RoleListMapper {

    @Override
    public Role toEntity(RoleListDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( dto.getId() );
        role.setName( dto.getName() );
        role.setDescription( dto.getDescription() );
        role.setActivated( dto.getActivated() );

        return role;
    }

    @Override
    public RoleListDTO toDto(Role entity) {
        if ( entity == null ) {
            return null;
        }

        RoleListDTO roleListDTO = new RoleListDTO();

        roleListDTO.setId( entity.getId() );
        roleListDTO.setName( entity.getName() );
        roleListDTO.setDescription( entity.getDescription() );
        roleListDTO.setActivated( entity.getActivated() );

        return roleListDTO;
    }

    @Override
    public List<Role> toEntity(List<RoleListDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Role> list = new ArrayList<Role>( dtoList.size() );
        for ( RoleListDTO roleListDTO : dtoList ) {
            list.add( toEntity( roleListDTO ) );
        }

        return list;
    }

    @Override
    public List<RoleListDTO> toDto(List<Role> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<RoleListDTO> list = new ArrayList<RoleListDTO>( entityList.size() );
        for ( Role role : entityList ) {
            list.add( toDto( role ) );
        }

        return list;
    }
}
