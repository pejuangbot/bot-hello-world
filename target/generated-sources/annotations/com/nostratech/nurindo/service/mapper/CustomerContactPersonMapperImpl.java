package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerContactPerson;
import com.nostratech.nurindo.service.dto.CustomerContactPersonDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerContactPersonMapperImpl implements CustomerContactPersonMapper {

    @Override
    public List<CustomerContactPerson> toEntity(List<CustomerContactPersonDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CustomerContactPerson> list = new ArrayList<CustomerContactPerson>( dtoList.size() );
        for ( CustomerContactPersonDTO customerContactPersonDTO : dtoList ) {
            list.add( toEntity( customerContactPersonDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerContactPersonDTO> toDto(List<CustomerContactPerson> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerContactPersonDTO> list = new ArrayList<CustomerContactPersonDTO>( entityList.size() );
        for ( CustomerContactPerson customerContactPerson : entityList ) {
            list.add( toDto( customerContactPerson ) );
        }

        return list;
    }

    @Override
    public CustomerContactPerson toEntity(CustomerContactPersonDTO customerContactPersonDTO) {
        if ( customerContactPersonDTO == null ) {
            return null;
        }

        CustomerContactPerson customerContactPerson = new CustomerContactPerson();

        customerContactPerson.setId( customerContactPersonDTO.getId() );
        customerContactPerson.setFullName( customerContactPersonDTO.getFullName() );
        customerContactPerson.setTitle( customerContactPersonDTO.getTitle() );
        customerContactPerson.setEmail( customerContactPersonDTO.getEmail() );
        customerContactPerson.setPhoneNumber( customerContactPersonDTO.getPhoneNumber() );
        customerContactPerson.setFaxNumber( customerContactPersonDTO.getFaxNumber() );
        customerContactPerson.setMobileNumber( customerContactPersonDTO.getMobileNumber() );
        customerContactPerson.setStatus( customerContactPersonDTO.getStatus() );
        customerContactPerson.setDescription( customerContactPersonDTO.getDescription() );

        return customerContactPerson;
    }

    @Override
    public CustomerContactPersonDTO toDto(CustomerContactPerson customerContactPerson) {
        if ( customerContactPerson == null ) {
            return null;
        }

        CustomerContactPersonDTO customerContactPersonDTO = new CustomerContactPersonDTO();

        customerContactPersonDTO.setId( customerContactPerson.getId() );
        customerContactPersonDTO.setFullName( customerContactPerson.getFullName() );
        customerContactPersonDTO.setTitle( customerContactPerson.getTitle() );
        customerContactPersonDTO.setEmail( customerContactPerson.getEmail() );
        customerContactPersonDTO.setPhoneNumber( customerContactPerson.getPhoneNumber() );
        customerContactPersonDTO.setFaxNumber( customerContactPerson.getFaxNumber() );
        customerContactPersonDTO.setMobileNumber( customerContactPerson.getMobileNumber() );
        customerContactPersonDTO.setStatus( customerContactPerson.getStatus() );
        customerContactPersonDTO.setDescription( customerContactPerson.getDescription() );

        return customerContactPersonDTO;
    }
}
