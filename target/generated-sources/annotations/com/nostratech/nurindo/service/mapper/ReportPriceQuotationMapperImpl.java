package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.service.dto.ReportPriceQuotationDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class ReportPriceQuotationMapperImpl implements ReportPriceQuotationMapper {

    @Override
    public PriceQuotation toEntity(ReportPriceQuotationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        PriceQuotation priceQuotation = new PriceQuotation();

        priceQuotation.setSubTotal( dto.getSubTotal() );

        return priceQuotation;
    }

    @Override
    public ReportPriceQuotationDTO toDto(PriceQuotation entity) {
        if ( entity == null ) {
            return null;
        }

        ReportPriceQuotationDTO reportPriceQuotationDTO = new ReportPriceQuotationDTO();

        reportPriceQuotationDTO.setSubTotal( entity.getSubTotal() );

        return reportPriceQuotationDTO;
    }

    @Override
    public List<PriceQuotation> toEntity(List<ReportPriceQuotationDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<PriceQuotation> list = new ArrayList<PriceQuotation>( dtoList.size() );
        for ( ReportPriceQuotationDTO reportPriceQuotationDTO : dtoList ) {
            list.add( toEntity( reportPriceQuotationDTO ) );
        }

        return list;
    }

    @Override
    public List<ReportPriceQuotationDTO> toDto(List<PriceQuotation> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ReportPriceQuotationDTO> list = new ArrayList<ReportPriceQuotationDTO>( entityList.size() );
        for ( PriceQuotation priceQuotation : entityList ) {
            list.add( toDto( priceQuotation ) );
        }

        return list;
    }
}
