package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PqNumberRecord;
import com.nostratech.nurindo.service.dto.PqNumberRecordDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PqNumberMapperImpl implements PqNumberMapper {

    @Override
    public List<PqNumberRecord> toEntity(List<PqNumberRecordDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<PqNumberRecord> list = new ArrayList<PqNumberRecord>( dtoList.size() );
        for ( PqNumberRecordDTO pqNumberRecordDTO : dtoList ) {
            list.add( toEntity( pqNumberRecordDTO ) );
        }

        return list;
    }

    @Override
    public List<PqNumberRecordDTO> toDto(List<PqNumberRecord> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<PqNumberRecordDTO> list = new ArrayList<PqNumberRecordDTO>( entityList.size() );
        for ( PqNumberRecord pqNumberRecord : entityList ) {
            list.add( toDto( pqNumberRecord ) );
        }

        return list;
    }

    @Override
    public PqNumberRecord toEntity(PqNumberRecordDTO pqNumberRecordDTO) {
        if ( pqNumberRecordDTO == null ) {
            return null;
        }

        PqNumberRecord pqNumberRecord = new PqNumberRecord();

        pqNumberRecord.setId( pqNumberRecordDTO.getId() );
        pqNumberRecord.setNumberGenerated( pqNumberRecordDTO.getNumberGenerated() );
        pqNumberRecord.setMonth( pqNumberRecordDTO.getMonth() );
        pqNumberRecord.setYear( pqNumberRecordDTO.getYear() );
        pqNumberRecord.setNumberType( pqNumberRecordDTO.getNumberType() );

        return pqNumberRecord;
    }

    @Override
    public PqNumberRecordDTO toDto(PqNumberRecord pqNumberRecord) {
        if ( pqNumberRecord == null ) {
            return null;
        }

        PqNumberRecordDTO pqNumberRecordDTO = new PqNumberRecordDTO();

        pqNumberRecordDTO.setId( pqNumberRecord.getId() );
        pqNumberRecordDTO.setNumberGenerated( pqNumberRecord.getNumberGenerated() );
        pqNumberRecordDTO.setMonth( pqNumberRecord.getMonth() );
        pqNumberRecordDTO.setYear( pqNumberRecord.getYear() );
        pqNumberRecordDTO.setNumberType( pqNumberRecord.getNumberType() );

        return pqNumberRecordDTO;
    }
}
