package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.service.dto.CustomerDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerMapperImpl implements CustomerMapper {

    @Override
    public List<Customer> toEntity(List<CustomerDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Customer> list = new ArrayList<Customer>( dtoList.size() );
        for ( CustomerDTO customerDTO : dtoList ) {
            list.add( toEntity( customerDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerDTO> toDto(List<Customer> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerDTO> list = new ArrayList<CustomerDTO>( entityList.size() );
        for ( Customer customer : entityList ) {
            list.add( toDto( customer ) );
        }

        return list;
    }

    @Override
    public Customer toEntity(CustomerDTO customerDTO) {
        if ( customerDTO == null ) {
            return null;
        }

        Customer customer = new Customer();

        customer.setId( customerDTO.getId() );
        customer.setLastUpdateProcess( customerDTO.getLastUpdateProcess() );
        customer.setIsDeleted( customerDTO.getIsDeleted() );
        customer.setCustomerCode( customerDTO.getCustomerCode() );
        customer.setCustomerName( customerDTO.getCustomerName() );
        customer.setAddress( customerDTO.getAddress() );
        customer.setCityCode( customerDTO.getCityCode() );
        customer.setZipCode( customerDTO.getZipCode() );
        customer.setNpwpId( customerDTO.getNpwpId() );
        customer.setNpwpAddress( customerDTO.getNpwpAddress() );
        customer.setNpwpCityCode( customerDTO.getNpwpCityCode() );
        customer.setNpwpZipCode( customerDTO.getNpwpZipCode() );
        customer.setNpwpName( customerDTO.getNpwpName() );
        customer.setQuotationFormat( customerDTO.getQuotationFormat() );
        customer.setGroupCode( customerDTO.getGroupCode() );
        customer.setStatus( customerDTO.getStatus() );
        customer.setNeedNumbering( customerDTO.getNeedNumbering() );
        customer.setTop( customerDTO.getTop() );
        customer.setTopUm( customerDTO.getTopUm() );
        customer.setAddressDetail( customerDTO.getAddressDetail() );
        customer.setSalesCode( customerDTO.getSalesCode() );
        customer.setCustomerType( customerDTO.getCustomerType() );
        customer.setTermOfPayment( customerDTO.getTermOfPayment() );
        customer.setCurrency( customerDTO.getCurrency() );

        return customer;
    }

    @Override
    public CustomerDTO toDto(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        CustomerDTO customerDTO = new CustomerDTO();

        customerDTO.setId( customer.getId() );
        customerDTO.setLastUpdateProcess( customer.getLastUpdateProcess() );
        customerDTO.setIsDeleted( customer.getIsDeleted() );
        customerDTO.setCustomerCode( customer.getCustomerCode() );
        customerDTO.setCustomerName( customer.getCustomerName() );
        customerDTO.setAddress( customer.getAddress() );
        customerDTO.setCityCode( customer.getCityCode() );
        customerDTO.setZipCode( customer.getZipCode() );
        customerDTO.setNpwpId( customer.getNpwpId() );
        customerDTO.setNpwpAddress( customer.getNpwpAddress() );
        customerDTO.setNpwpCityCode( customer.getNpwpCityCode() );
        customerDTO.setNpwpZipCode( customer.getNpwpZipCode() );
        customerDTO.setNpwpName( customer.getNpwpName() );
        customerDTO.setQuotationFormat( customer.getQuotationFormat() );
        customerDTO.setGroupCode( customer.getGroupCode() );
        customerDTO.setStatus( customer.getStatus() );
        customerDTO.setNeedNumbering( customer.getNeedNumbering() );
        customerDTO.setTop( customer.getTop() );
        customerDTO.setTopUm( customer.getTopUm() );
        customerDTO.setAddressDetail( customer.getAddressDetail() );
        customerDTO.setSalesCode( customer.getSalesCode() );
        customerDTO.setCustomerType( customer.getCustomerType() );
        customerDTO.setTermOfPayment( customer.getTermOfPayment() );
        customerDTO.setCurrency( customer.getCurrency() );

        return customerDTO;
    }
}
