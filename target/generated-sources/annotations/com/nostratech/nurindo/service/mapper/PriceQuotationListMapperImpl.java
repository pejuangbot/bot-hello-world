package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.PriceQuotationListDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PriceQuotationListMapperImpl implements PriceQuotationListMapper {

    @Override
    public PriceQuotationListDTO toDto(PriceQuotation entity, PriceQuotationListDTO dto) {
        if ( entity == null ) {
            return null;
        }

        String firstName = entityAccountExecutiveFirstName( entity );
        if ( firstName != null ) {
            dto.setAccountExecutive( firstName );
        }
        dto.setCreatedDate( createdDateToDto( entity ) );
        String name = entityApprovalName( entity );
        if ( name != null ) {
            dto.setNeedApprovalFrom( name );
        }
        dto.setId( entity.getId() );
        dto.setPqNumber( entity.getPqNumber() );
        dto.setProjectName( entity.getProjectName() );
        dto.setCustomerName( entity.getCustomerName() );
        dto.setStatus( entity.getStatus() );

        return dto;
    }

    private String entityAccountExecutiveFirstName(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        User accountExecutive = priceQuotation.getAccountExecutive();
        if ( accountExecutive == null ) {
            return null;
        }
        String firstName = accountExecutive.getFirstName();
        if ( firstName == null ) {
            return null;
        }
        return firstName;
    }

    private String entityApprovalName(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        Role approval = priceQuotation.getApproval();
        if ( approval == null ) {
            return null;
        }
        String name = approval.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }
}
