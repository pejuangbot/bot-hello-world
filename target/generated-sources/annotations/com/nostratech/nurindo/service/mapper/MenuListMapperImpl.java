package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Menu;
import com.nostratech.nurindo.service.dto.MenuListDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class MenuListMapperImpl implements MenuListMapper {

    @Override
    public Menu toEntity(MenuListDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Menu menu = new Menu();

        menu.setId( dto.getId() );
        menu.setMenuName( dto.getMenuName() );
        menu.setName( dto.getName() );
        menu.setDescription( dto.getDescription() );
        menu.setParent( dto.getParent() );
        menu.setActivated( dto.getActivated() );

        return menu;
    }

    @Override
    public MenuListDTO toDto(Menu entity) {
        if ( entity == null ) {
            return null;
        }

        MenuListDTO menuListDTO = new MenuListDTO();

        menuListDTO.setId( entity.getId() );
        menuListDTO.setMenuName( entity.getMenuName() );
        menuListDTO.setName( entity.getName() );
        menuListDTO.setDescription( entity.getDescription() );
        menuListDTO.setParent( entity.getParent() );
        menuListDTO.setActivated( entity.getActivated() );

        return menuListDTO;
    }

    @Override
    public List<Menu> toEntity(List<MenuListDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Menu> list = new ArrayList<Menu>( dtoList.size() );
        for ( MenuListDTO menuListDTO : dtoList ) {
            list.add( toEntity( menuListDTO ) );
        }

        return list;
    }

    @Override
    public List<MenuListDTO> toDto(List<Menu> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<MenuListDTO> list = new ArrayList<MenuListDTO>( entityList.size() );
        for ( Menu menu : entityList ) {
            list.add( toDto( menu ) );
        }

        return list;
    }
}
