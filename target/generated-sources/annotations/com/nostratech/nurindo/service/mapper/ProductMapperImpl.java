package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Product;
import com.nostratech.nurindo.service.dto.ProductDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDTO toDto(Product entity, ProductDTO dto) {
        if ( entity == null ) {
            return null;
        }

        dto.setId( entity.getId() );
        dto.setProductCode( entity.getProductCode() );
        dto.setProduct( entity.getProduct() );
        dto.setHeight( entity.getHeight() );
        dto.setWidth( entity.getWidth() );
        dto.setStock( entity.getStock() );
        dto.setUnitMeasure( entity.getUnitMeasure() );
        dto.setType( entity.getType() );
        dto.setMsrp( entity.getMsrp() );
        dto.setBottomPriceAE( entity.getBottomPriceAE() );
        dto.setBottomPriceSM( entity.getBottomPriceSM() );
        dto.setBottomPriceSA( entity.getBottomPriceSA() );

        return dto;
    }

    @Override
    public Product toEntity(ProductDTO dto, Product entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setId( dto.getId() );
        entity.setProductCode( dto.getProductCode() );
        entity.setProduct( dto.getProduct() );
        entity.setHeight( dto.getHeight() );
        entity.setWidth( dto.getWidth() );
        entity.setStock( dto.getStock() );
        entity.setUnitMeasure( dto.getUnitMeasure() );
        entity.setType( dto.getType() );
        entity.setMsrp( dto.getMsrp() );
        entity.setBottomPriceAE( dto.getBottomPriceAE() );
        entity.setBottomPriceSM( dto.getBottomPriceSM() );
        entity.setBottomPriceSA( dto.getBottomPriceSA() );

        return entity;
    }
}
