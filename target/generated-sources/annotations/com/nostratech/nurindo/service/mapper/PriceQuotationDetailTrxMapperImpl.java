package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotationDetailTrx;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailTrxDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PriceQuotationDetailTrxMapperImpl implements PriceQuotationDetailTrxMapper {

    @Override
    public PriceQuotationDetailTrx toEntity(PriceQuotationDetailTrxDTO dto, PriceQuotationDetailTrx entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setName( dto.getName() );
        entity.setHeight( dto.getHeight() );
        entity.setWeight( dto.getWeight() );
        entity.setResolution( dto.getResolution() );
        entity.setQuantity( dto.getQuantity() );
        entity.setPrice( dto.getPrice() );
        entity.setTotalPrice( dto.getTotalPrice() );
        entity.setMsrp( dto.getMsrp() );
        entity.setBottomPrice( dto.getBottomPrice() );

        return entity;
    }
}
