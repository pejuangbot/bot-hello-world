package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.CustomerListDTO;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailDTO;
import com.nostratech.nurindo.service.dto.UserDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PriceQuotationDetailMapperImpl implements PriceQuotationDetailMapper {

    @Override
    public PriceQuotationDetailDTO toDto(PriceQuotation entity, PriceQuotationDetailDTO dto) {
        if ( entity == null ) {
            return null;
        }

        dto.setCreatedDate( createdDateToDto( entity ) );
        String name = entityApprovalName( entity );
        if ( name != null ) {
            dto.setNeedApprovalFrom( name );
        }
        dto.setId( entity.getId() );
        dto.setPqNumber( entity.getPqNumber() );
        dto.setPrNumber( entity.getPrNumber() );
        if ( entity.getSalesAdmin() != null ) {
            if ( dto.getSalesAdmin() == null ) {
                dto.setSalesAdmin( new UserDTO() );
            }
            userToUserDTO( entity.getSalesAdmin(), dto.getSalesAdmin() );
        }
        else {
            dto.setSalesAdmin( null );
        }
        if ( entity.getCustomer() != null ) {
            if ( dto.getCustomer() == null ) {
                dto.setCustomer( new CustomerListDTO() );
            }
            customerToCustomerListDTO( entity.getCustomer(), dto.getCustomer() );
        }
        else {
            dto.setCustomer( null );
        }
        dto.setProjectName( entity.getProjectName() );
        dto.setCustomerName( entity.getCustomerName() );
        dto.setPhoneNumber( entity.getPhoneNumber() );
        dto.setFaxNumber( entity.getFaxNumber() );
        dto.setEmail( entity.getEmail() );
        dto.setNotes( entity.getNotes() );
        dto.setType( entity.getType() );
        dto.setTax( entity.getTax() );
        dto.setDiscount( entity.getDiscount() );
        dto.setSubTotal( entity.getSubTotal() );
        dto.setDiscountPrice( entity.getDiscountPrice() );
        dto.setTaxPrice( entity.getTaxPrice() );
        dto.setFinalPrice( entity.getFinalPrice() );
        dto.setStatus( entity.getStatus() );
        dto.setPo( entity.getPo() );
        dto.setAttn( entity.getAttn() );
        dto.setCc( entity.getCc() );

        return dto;
    }

    private String entityApprovalName(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        Role approval = priceQuotation.getApproval();
        if ( approval == null ) {
            return null;
        }
        String name = approval.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    protected void userToUserDTO(User user, UserDTO mappingTarget) {
        if ( user == null ) {
            return;
        }

        mappingTarget.setId( user.getId() );
        mappingTarget.setUsername( user.getUsername() );
    }

    protected void customerToCustomerListDTO(Customer customer, CustomerListDTO mappingTarget) {
        if ( customer == null ) {
            return;
        }

        mappingTarget.setId( customer.getId() );
        mappingTarget.setLastUpdateProcess( customer.getLastUpdateProcess() );
        mappingTarget.setIsDeleted( customer.getIsDeleted() );
        mappingTarget.setCustomerCode( customer.getCustomerCode() );
        mappingTarget.setCustomerName( customer.getCustomerName() );
        mappingTarget.setAddress( customer.getAddress() );
        mappingTarget.setCityCode( customer.getCityCode() );
        mappingTarget.setZipCode( customer.getZipCode() );
        mappingTarget.setNpwpId( customer.getNpwpId() );
        mappingTarget.setNpwpAddress( customer.getNpwpAddress() );
        mappingTarget.setNpwpCityCode( customer.getNpwpCityCode() );
        mappingTarget.setNpwpZipCode( customer.getNpwpZipCode() );
        mappingTarget.setNpwpName( customer.getNpwpName() );
        mappingTarget.setQuotationFormat( customer.getQuotationFormat() );
        mappingTarget.setGroupCode( customer.getGroupCode() );
        mappingTarget.setStatus( customer.getStatus() );
        mappingTarget.setNeedNumbering( customer.getNeedNumbering() );
        mappingTarget.setTop( customer.getTop() );
        mappingTarget.setTopUm( customer.getTopUm() );
        mappingTarget.setAddressDetail( customer.getAddressDetail() );
        mappingTarget.setSalesCode( customer.getSalesCode() );
        mappingTarget.setCustomerType( customer.getCustomerType() );
        mappingTarget.setTermOfPayment( customer.getTermOfPayment() );
        mappingTarget.setCurrency( customer.getCurrency() );
        mappingTarget.setCreatedDate( customer.getCreatedDate() );
        mappingTarget.setCreatedBy( customer.getCreatedBy() );
        mappingTarget.setLastModifiedDate( customer.getLastModifiedDate() );
        mappingTarget.setLastModifiedBy( customer.getLastModifiedBy() );
    }
}
