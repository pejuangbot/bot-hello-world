package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.service.dto.CustomerListDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerListMapperImpl implements CustomerListMapper {

    @Override
    public List<Customer> toEntity(List<CustomerListDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Customer> list = new ArrayList<Customer>( dtoList.size() );
        for ( CustomerListDTO customerListDTO : dtoList ) {
            list.add( toEntity( customerListDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerListDTO> toDto(List<Customer> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerListDTO> list = new ArrayList<CustomerListDTO>( entityList.size() );
        for ( Customer customer : entityList ) {
            list.add( toDto( customer ) );
        }

        return list;
    }

    @Override
    public Customer toEntity(CustomerListDTO customerListDTO) {
        if ( customerListDTO == null ) {
            return null;
        }

        Customer customer = new Customer();

        customer.setCreatedBy( customerListDTO.getCreatedBy() );
        customer.setCreatedDate( customerListDTO.getCreatedDate() );
        customer.setLastModifiedBy( customerListDTO.getLastModifiedBy() );
        customer.setLastModifiedDate( customerListDTO.getLastModifiedDate() );
        customer.setId( customerListDTO.getId() );
        customer.setLastUpdateProcess( customerListDTO.getLastUpdateProcess() );
        customer.setIsDeleted( customerListDTO.getIsDeleted() );
        customer.setCustomerCode( customerListDTO.getCustomerCode() );
        customer.setCustomerName( customerListDTO.getCustomerName() );
        customer.setAddress( customerListDTO.getAddress() );
        customer.setCityCode( customerListDTO.getCityCode() );
        customer.setZipCode( customerListDTO.getZipCode() );
        customer.setNpwpId( customerListDTO.getNpwpId() );
        customer.setNpwpAddress( customerListDTO.getNpwpAddress() );
        customer.setNpwpCityCode( customerListDTO.getNpwpCityCode() );
        customer.setNpwpZipCode( customerListDTO.getNpwpZipCode() );
        customer.setNpwpName( customerListDTO.getNpwpName() );
        customer.setQuotationFormat( customerListDTO.getQuotationFormat() );
        customer.setGroupCode( customerListDTO.getGroupCode() );
        customer.setStatus( customerListDTO.getStatus() );
        customer.setNeedNumbering( customerListDTO.getNeedNumbering() );
        customer.setTop( customerListDTO.getTop() );
        customer.setTopUm( customerListDTO.getTopUm() );
        customer.setAddressDetail( customerListDTO.getAddressDetail() );
        customer.setSalesCode( customerListDTO.getSalesCode() );
        customer.setCustomerType( customerListDTO.getCustomerType() );
        customer.setTermOfPayment( customerListDTO.getTermOfPayment() );
        customer.setCurrency( customerListDTO.getCurrency() );

        return customer;
    }

    @Override
    public CustomerListDTO toDto(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        CustomerListDTO customerListDTO = new CustomerListDTO();

        customerListDTO.setId( customer.getId() );
        customerListDTO.setLastUpdateProcess( customer.getLastUpdateProcess() );
        customerListDTO.setIsDeleted( customer.getIsDeleted() );
        customerListDTO.setCustomerCode( customer.getCustomerCode() );
        customerListDTO.setCustomerName( customer.getCustomerName() );
        customerListDTO.setAddress( customer.getAddress() );
        customerListDTO.setCityCode( customer.getCityCode() );
        customerListDTO.setZipCode( customer.getZipCode() );
        customerListDTO.setNpwpId( customer.getNpwpId() );
        customerListDTO.setNpwpAddress( customer.getNpwpAddress() );
        customerListDTO.setNpwpCityCode( customer.getNpwpCityCode() );
        customerListDTO.setNpwpZipCode( customer.getNpwpZipCode() );
        customerListDTO.setNpwpName( customer.getNpwpName() );
        customerListDTO.setQuotationFormat( customer.getQuotationFormat() );
        customerListDTO.setGroupCode( customer.getGroupCode() );
        customerListDTO.setStatus( customer.getStatus() );
        customerListDTO.setNeedNumbering( customer.getNeedNumbering() );
        customerListDTO.setTop( customer.getTop() );
        customerListDTO.setTopUm( customer.getTopUm() );
        customerListDTO.setAddressDetail( customer.getAddressDetail() );
        customerListDTO.setSalesCode( customer.getSalesCode() );
        customerListDTO.setCustomerType( customer.getCustomerType() );
        customerListDTO.setTermOfPayment( customer.getTermOfPayment() );
        customerListDTO.setCurrency( customer.getCurrency() );
        customerListDTO.setCreatedDate( customer.getCreatedDate() );
        customerListDTO.setCreatedBy( customer.getCreatedBy() );
        customerListDTO.setLastModifiedDate( customer.getLastModifiedDate() );
        customerListDTO.setLastModifiedBy( customer.getLastModifiedBy() );

        return customerListDTO;
    }
}
