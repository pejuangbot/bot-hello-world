package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.service.dto.CustomerProductsListDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerProductsListMapperImpl implements CustomerProductsListMapper {

    @Override
    public List<CustomerProducts> toEntity(List<CustomerProductsListDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CustomerProducts> list = new ArrayList<CustomerProducts>( dtoList.size() );
        for ( CustomerProductsListDTO customerProductsListDTO : dtoList ) {
            list.add( toEntity( customerProductsListDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerProductsListDTO> toDto(List<CustomerProducts> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerProductsListDTO> list = new ArrayList<CustomerProductsListDTO>( entityList.size() );
        for ( CustomerProducts customerProducts : entityList ) {
            list.add( toDto( customerProducts ) );
        }

        return list;
    }

    @Override
    public CustomerProducts toEntity(CustomerProductsListDTO dto) {
        if ( dto == null ) {
            return null;
        }

        CustomerProducts customerProducts = new CustomerProducts();

        customerProducts.setId( dto.getId() );
        customerProducts.setAgreedPrice( dto.getAgreedPrice() );

        return customerProducts;
    }

    @Override
    public CustomerProductsListDTO toDto(CustomerProducts entity) {
        if ( entity == null ) {
            return null;
        }

        CustomerProductsListDTO customerProductsListDTO = new CustomerProductsListDTO();

        customerProductsListDTO.setId( entity.getId() );
        customerProductsListDTO.setAgreedPrice( entity.getAgreedPrice() );

        return customerProductsListDTO;
    }
}
