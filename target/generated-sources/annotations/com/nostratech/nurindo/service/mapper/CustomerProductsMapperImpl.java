package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.CustomerProducts;
import com.nostratech.nurindo.service.dto.CustomerProductsDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerProductsMapperImpl implements CustomerProductsMapper {

    @Override
    public List<CustomerProducts> toEntity(List<CustomerProductsDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CustomerProducts> list = new ArrayList<CustomerProducts>( dtoList.size() );
        for ( CustomerProductsDTO customerProductsDTO : dtoList ) {
            list.add( toEntity( customerProductsDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerProductsDTO> toDto(List<CustomerProducts> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerProductsDTO> list = new ArrayList<CustomerProductsDTO>( entityList.size() );
        for ( CustomerProducts customerProducts : entityList ) {
            list.add( toDto( customerProducts ) );
        }

        return list;
    }

    @Override
    public CustomerProducts toEntity(CustomerProductsDTO dto) {
        if ( dto == null ) {
            return null;
        }

        CustomerProducts customerProducts = new CustomerProducts();

        customerProducts.setId( dto.getId() );
        customerProducts.setAgreedPrice( dto.getAgreedPrice() );

        return customerProducts;
    }

    @Override
    public CustomerProductsDTO toDto(CustomerProducts entity) {
        if ( entity == null ) {
            return null;
        }

        CustomerProductsDTO customerProductsDTO = new CustomerProductsDTO();

        customerProductsDTO.setId( entity.getId() );
        customerProductsDTO.setAgreedPrice( entity.getAgreedPrice() );

        return customerProductsDTO;
    }
}
