package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.PriceQuotationDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PriceQuotationMapperImpl implements PriceQuotationMapper {

    @Override
    public PriceQuotation toEntity(PriceQuotationDTO dto, PriceQuotation entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setId( dto.getId() );
        entity.setPrNumber( dto.getPrNumber() );
        entity.setProjectName( dto.getProjectName() );
        entity.setCustomerName( dto.getCustomerName() );
        entity.setPhoneNumber( dto.getPhoneNumber() );
        entity.setFaxNumber( dto.getFaxNumber() );
        entity.setEmail( dto.getEmail() );
        entity.setNotes( dto.getNotes() );
        entity.setType( dto.getType() );
        entity.setTax( dto.getTax() );
        entity.setDiscount( dto.getDiscount() );
        entity.setStatus( dto.getStatus() );
        entity.setSubTotal( dto.getSubTotal() );
        entity.setDiscountPrice( dto.getDiscountPrice() );
        entity.setTaxPrice( dto.getTaxPrice() );
        entity.setFinalPrice( dto.getFinalPrice() );
        entity.setPo( dto.getPo() );
        entity.setAttn( dto.getAttn() );
        entity.setCc( dto.getCc() );

        return entity;
    }

    @Override
    public PriceQuotationDTO toDto(PriceQuotation entity, PriceQuotationDTO dto) {
        if ( entity == null ) {
            return null;
        }

        Long id = entityAccountExecutiveId( entity );
        if ( id != null ) {
            dto.setAccountExecutive( id );
        }
        Long id1 = entitySalesAdminId( entity );
        if ( id1 != null ) {
            dto.setSalesAdmin( id1 );
        }
        Long id2 = entityCustomerId( entity );
        if ( id2 != null ) {
            dto.setCustomer( id2 );
        }
        dto.setId( entity.getId() );
        dto.setPrNumber( entity.getPrNumber() );
        dto.setProjectName( entity.getProjectName() );
        dto.setCustomerName( entity.getCustomerName() );
        dto.setPhoneNumber( entity.getPhoneNumber() );
        dto.setFaxNumber( entity.getFaxNumber() );
        dto.setEmail( entity.getEmail() );
        dto.setNotes( entity.getNotes() );
        dto.setType( entity.getType() );
        dto.setTax( entity.getTax() );
        dto.setDiscount( entity.getDiscount() );
        dto.setSubTotal( entity.getSubTotal() );
        dto.setDiscountPrice( entity.getDiscountPrice() );
        dto.setTaxPrice( entity.getTaxPrice() );
        dto.setFinalPrice( entity.getFinalPrice() );
        dto.setStatus( entity.getStatus() );
        dto.setPo( entity.getPo() );
        dto.setAttn( entity.getAttn() );
        dto.setCc( entity.getCc() );

        return dto;
    }

    private Long entityAccountExecutiveId(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        User accountExecutive = priceQuotation.getAccountExecutive();
        if ( accountExecutive == null ) {
            return null;
        }
        Long id = accountExecutive.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long entitySalesAdminId(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        User salesAdmin = priceQuotation.getSalesAdmin();
        if ( salesAdmin == null ) {
            return null;
        }
        Long id = salesAdmin.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }

    private Long entityCustomerId(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        Customer customer = priceQuotation.getCustomer();
        if ( customer == null ) {
            return null;
        }
        Long id = customer.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
