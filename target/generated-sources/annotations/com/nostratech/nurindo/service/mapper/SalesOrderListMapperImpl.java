package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.SalesOrderListDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class SalesOrderListMapperImpl implements SalesOrderListMapper {

    @Override
    public SalesOrderListDTO toDto(PriceQuotation entity, SalesOrderListDTO dto) {
        if ( entity == null ) {
            return null;
        }

        String firstName = entityAccountExecutiveFirstName( entity );
        if ( firstName != null ) {
            dto.setAccountExecutive( firstName );
        }
        dto.setCreatedDate( createdDateToDto( entity ) );
        dto.setId( entity.getId() );
        dto.setPqNumber( entity.getPqNumber() );
        dto.setProjectName( entity.getProjectName() );
        dto.setCustomerName( entity.getCustomerName() );
        dto.setStatus( entity.getStatus() );

        return dto;
    }

    private String entityAccountExecutiveFirstName(PriceQuotation priceQuotation) {
        if ( priceQuotation == null ) {
            return null;
        }
        User accountExecutive = priceQuotation.getAccountExecutive();
        if ( accountExecutive == null ) {
            return null;
        }
        String firstName = accountExecutive.getFirstName();
        if ( firstName == null ) {
            return null;
        }
        return firstName;
    }
}
