package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.ProductRole;
import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.service.dto.ProductRoleDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class ProductRolelMapperImpl implements ProductRolelMapper {

    @Override
    public List<ProductRole> toEntity(List<ProductRoleDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<ProductRole> list = new ArrayList<ProductRole>( dtoList.size() );
        for ( ProductRoleDTO productRoleDTO : dtoList ) {
            list.add( toEntity( productRoleDTO ) );
        }

        return list;
    }

    @Override
    public List<ProductRoleDTO> toDto(List<ProductRole> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ProductRoleDTO> list = new ArrayList<ProductRoleDTO>( entityList.size() );
        for ( ProductRole productRole : entityList ) {
            list.add( toDto( productRole ) );
        }

        return list;
    }

    @Override
    public ProductRole toEntity(ProductRoleDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ProductRole productRole = new ProductRole();

        productRole.setBottomPrice( dto.getBottomPrice() );
        productRole.setMsrp( dto.getMsrp() );
        productRole.setId( dto.getId() );

        return productRole;
    }

    @Override
    public ProductRoleDTO toDto(ProductRole entity) {
        if ( entity == null ) {
            return null;
        }

        ProductRoleDTO productRoleDTO = new ProductRoleDTO();

        String name = entityRoleName( entity );
        if ( name != null ) {
            productRoleDTO.setName( name );
        }
        Long id = entityRoleId( entity );
        if ( id != null ) {
            productRoleDTO.setId( id );
        }
        productRoleDTO.setBottomPrice( entity.getBottomPrice() );
        productRoleDTO.setMsrp( entity.getMsrp() );

        return productRoleDTO;
    }

    private String entityRoleName(ProductRole productRole) {
        if ( productRole == null ) {
            return null;
        }
        Role role = productRole.getRole();
        if ( role == null ) {
            return null;
        }
        String name = role.getName();
        if ( name == null ) {
            return null;
        }
        return name;
    }

    private Long entityRoleId(ProductRole productRole) {
        if ( productRole == null ) {
            return null;
        }
        Role role = productRole.getRole();
        if ( role == null ) {
            return null;
        }
        Long id = role.getId();
        if ( id == null ) {
            return null;
        }
        return id;
    }
}
