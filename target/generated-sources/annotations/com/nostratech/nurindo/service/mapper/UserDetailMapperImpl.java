package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.RoleBaseDTO;
import com.nostratech.nurindo.service.dto.UserDetailDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class UserDetailMapperImpl implements UserDetailMapper {

    @Override
    public List<User> toEntity(List<UserDetailDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( dtoList.size() );
        for ( UserDetailDTO userDetailDTO : dtoList ) {
            list.add( toEntity( userDetailDTO ) );
        }

        return list;
    }

    @Override
    public List<UserDetailDTO> toDto(List<User> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserDetailDTO> list = new ArrayList<UserDetailDTO>( entityList.size() );
        for ( User user : entityList ) {
            list.add( toDto( user ) );
        }

        return list;
    }

    @Override
    public User toEntity(UserDetailDTO dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        user.setLastModifiedDate( lastModifiedDateToInstant( dto ) );
        user.setCreatedDate( createdDateToInstant( dto ) );
        user.setId( dto.getId() );
        user.setUsername( dto.getUsername() );
        user.setEmail( dto.getEmail() );
        if ( dto.getEnabled() != null ) {
            user.setEnabled( dto.getEnabled() );
        }
        user.setRoles( roleBaseDTOListToRoleList( dto.getRoles() ) );

        return user;
    }

    @Override
    public UserDetailDTO toDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserDetailDTO userDetailDTO = new UserDetailDTO();

        userDetailDTO.setLastModifiedDate( lastModifiedDateToDto( entity ) );
        userDetailDTO.setCreatedDate( createdDateToDto( entity ) );
        userDetailDTO.setUsername( entity.getUsername() );
        userDetailDTO.setEmail( entity.getEmail() );
        userDetailDTO.setId( entity.getId() );
        userDetailDTO.setRoles( roleListToRoleBaseDTOList( entity.getRoles() ) );
        userDetailDTO.setEnabled( entity.isEnabled() );

        return userDetailDTO;
    }

    protected Role roleBaseDTOToRole(RoleBaseDTO roleBaseDTO) {
        if ( roleBaseDTO == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( roleBaseDTO.getId() );
        role.setName( roleBaseDTO.getName() );

        return role;
    }

    protected List<Role> roleBaseDTOListToRoleList(List<RoleBaseDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Role> list1 = new ArrayList<Role>( list.size() );
        for ( RoleBaseDTO roleBaseDTO : list ) {
            list1.add( roleBaseDTOToRole( roleBaseDTO ) );
        }

        return list1;
    }

    protected RoleBaseDTO roleToRoleBaseDTO(Role role) {
        if ( role == null ) {
            return null;
        }

        RoleBaseDTO roleBaseDTO = new RoleBaseDTO();

        roleBaseDTO.setId( role.getId() );
        roleBaseDTO.setName( role.getName() );

        return roleBaseDTO;
    }

    protected List<RoleBaseDTO> roleListToRoleBaseDTOList(List<Role> list) {
        if ( list == null ) {
            return null;
        }

        List<RoleBaseDTO> list1 = new ArrayList<RoleBaseDTO>( list.size() );
        for ( Role role : list ) {
            list1.add( roleToRoleBaseDTO( role ) );
        }

        return list1;
    }
}
