package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.City;
import com.nostratech.nurindo.service.dto.CityDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CityMapperImpl implements CityMapper {

    @Override
    public List<City> toEntity(List<CityDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<City> list = new ArrayList<City>( dtoList.size() );
        for ( CityDTO cityDTO : dtoList ) {
            list.add( toEntity( cityDTO ) );
        }

        return list;
    }

    @Override
    public List<CityDTO> toDto(List<City> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CityDTO> list = new ArrayList<CityDTO>( entityList.size() );
        for ( City city : entityList ) {
            list.add( toDto( city ) );
        }

        return list;
    }

    @Override
    public City toEntity(CityDTO cityDTO) {
        if ( cityDTO == null ) {
            return null;
        }

        City city = new City();

        city.setId( cityDTO.getId() );
        city.setCityCode( cityDTO.getCityCode() );
        city.setCityName( cityDTO.getCityName() );
        city.setIsland( cityDTO.getIsland() );
        city.setDescription( cityDTO.getDescription() );

        return city;
    }

    @Override
    public CityDTO toDto(City city) {
        if ( city == null ) {
            return null;
        }

        CityDTO cityDTO = new CityDTO();

        cityDTO.setId( city.getId() );
        cityDTO.setCityCode( city.getCityCode() );
        cityDTO.setCityName( city.getCityName() );
        cityDTO.setIsland( city.getIsland() );
        cityDTO.setDescription( city.getDescription() );

        return cityDTO;
    }
}
