package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Customer;
import com.nostratech.nurindo.service.dto.CustomerDetailDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class CustomerDetailMapperImpl implements CustomerDetailMapper {

    @Override
    public List<Customer> toEntity(List<CustomerDetailDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Customer> list = new ArrayList<Customer>( dtoList.size() );
        for ( CustomerDetailDTO customerDetailDTO : dtoList ) {
            list.add( toEntity( customerDetailDTO ) );
        }

        return list;
    }

    @Override
    public List<CustomerDetailDTO> toDto(List<Customer> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerDetailDTO> list = new ArrayList<CustomerDetailDTO>( entityList.size() );
        for ( Customer customer : entityList ) {
            list.add( toDto( customer ) );
        }

        return list;
    }

    @Override
    public Customer toEntity(CustomerDetailDTO customerDetailDTO) {
        if ( customerDetailDTO == null ) {
            return null;
        }

        Customer customer = new Customer();

        customer.setCreatedDate( customerDetailDTO.getCreatedDate() );
        customer.setLastModifiedBy( customerDetailDTO.getLastModifiedBy() );
        customer.setLastModifiedDate( customerDetailDTO.getLastModifiedDate() );
        customer.setId( customerDetailDTO.getId() );
        customer.setLastUpdateProcess( customerDetailDTO.getLastUpdateProcess() );
        customer.setIsDeleted( customerDetailDTO.getIsDeleted() );
        customer.setCustomerCode( customerDetailDTO.getCustomerCode() );
        customer.setCustomerName( customerDetailDTO.getCustomerName() );
        customer.setAddress( customerDetailDTO.getAddress() );
        customer.setCityCode( customerDetailDTO.getCityCode() );
        customer.setZipCode( customerDetailDTO.getZipCode() );
        customer.setNpwpId( customerDetailDTO.getNpwpId() );
        customer.setNpwpAddress( customerDetailDTO.getNpwpAddress() );
        customer.setNpwpCityCode( customerDetailDTO.getNpwpCityCode() );
        customer.setNpwpZipCode( customerDetailDTO.getNpwpZipCode() );
        customer.setNpwpName( customerDetailDTO.getNpwpName() );
        customer.setQuotationFormat( customerDetailDTO.getQuotationFormat() );
        customer.setGroupCode( customerDetailDTO.getGroupCode() );
        customer.setStatus( customerDetailDTO.getStatus() );
        customer.setNeedNumbering( customerDetailDTO.getNeedNumbering() );
        customer.setTop( customerDetailDTO.getTop() );
        customer.setTopUm( customerDetailDTO.getTopUm() );
        customer.setAddressDetail( customerDetailDTO.getAddressDetail() );
        customer.setSalesCode( customerDetailDTO.getSalesCode() );
        customer.setCustomerType( customerDetailDTO.getCustomerType() );

        return customer;
    }

    @Override
    public CustomerDetailDTO toDto(Customer customer) {
        if ( customer == null ) {
            return null;
        }

        CustomerDetailDTO customerDetailDTO = new CustomerDetailDTO();

        customerDetailDTO.setId( customer.getId() );
        customerDetailDTO.setLastUpdateProcess( customer.getLastUpdateProcess() );
        customerDetailDTO.setIsDeleted( customer.getIsDeleted() );
        customerDetailDTO.setCustomerCode( customer.getCustomerCode() );
        customerDetailDTO.setCustomerName( customer.getCustomerName() );
        customerDetailDTO.setAddress( customer.getAddress() );
        customerDetailDTO.setCityCode( customer.getCityCode() );
        customerDetailDTO.setZipCode( customer.getZipCode() );
        customerDetailDTO.setNpwpId( customer.getNpwpId() );
        customerDetailDTO.setNpwpAddress( customer.getNpwpAddress() );
        customerDetailDTO.setNpwpCityCode( customer.getNpwpCityCode() );
        customerDetailDTO.setNpwpZipCode( customer.getNpwpZipCode() );
        customerDetailDTO.setNpwpName( customer.getNpwpName() );
        customerDetailDTO.setQuotationFormat( customer.getQuotationFormat() );
        customerDetailDTO.setGroupCode( customer.getGroupCode() );
        customerDetailDTO.setStatus( customer.getStatus() );
        customerDetailDTO.setNeedNumbering( customer.getNeedNumbering() );
        customerDetailDTO.setTop( customer.getTop() );
        customerDetailDTO.setTopUm( customer.getTopUm() );
        customerDetailDTO.setAddressDetail( customer.getAddressDetail() );
        customerDetailDTO.setSalesCode( customer.getSalesCode() );
        customerDetailDTO.setCustomerType( customer.getCustomerType() );
        customerDetailDTO.setCreatedDate( customer.getCreatedDate() );
        customerDetailDTO.setLastModifiedDate( customer.getLastModifiedDate() );
        customerDetailDTO.setLastModifiedBy( customer.getLastModifiedBy() );

        return customerDetailDTO;
    }
}
