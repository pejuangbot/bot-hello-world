package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.FileAttach;
import com.nostratech.nurindo.service.dto.FileUploadDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class FileUploadMapperImpl implements FileUploadMapper {

    @Override
    public FileAttach toEntity(FileUploadDTO dto) {
        if ( dto == null ) {
            return null;
        }

        FileAttach fileAttach = new FileAttach();

        fileAttach.setFileName( dto.getFileName() );
        fileAttach.setFileUrl( dto.getFileUrl() );
        fileAttach.setFileDescription( dto.getFileDescription() );

        return fileAttach;
    }

    @Override
    public FileUploadDTO toDto(FileAttach entity) {
        if ( entity == null ) {
            return null;
        }

        FileUploadDTO fileUploadDTO = new FileUploadDTO();

        fileUploadDTO.setFileName( entity.getFileName() );
        fileUploadDTO.setFileUrl( entity.getFileUrl() );
        fileUploadDTO.setFileDescription( entity.getFileDescription() );

        return fileUploadDTO;
    }

    @Override
    public List<FileAttach> toEntity(List<FileUploadDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<FileAttach> list = new ArrayList<FileAttach>( dtoList.size() );
        for ( FileUploadDTO fileUploadDTO : dtoList ) {
            list.add( toEntity( fileUploadDTO ) );
        }

        return list;
    }

    @Override
    public List<FileUploadDTO> toDto(List<FileAttach> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<FileUploadDTO> list = new ArrayList<FileUploadDTO>( entityList.size() );
        for ( FileAttach fileAttach : entityList ) {
            list.add( toDto( fileAttach ) );
        }

        return list;
    }
}
