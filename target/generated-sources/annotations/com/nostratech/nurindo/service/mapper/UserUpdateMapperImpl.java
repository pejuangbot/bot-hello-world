package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.Role;
import com.nostratech.nurindo.domain.User;
import com.nostratech.nurindo.service.dto.RoleBaseDTO;
import com.nostratech.nurindo.service.dto.UserUpdateDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class UserUpdateMapperImpl implements UserUpdateMapper {

    @Override
    public List<User> toEntity(List<UserUpdateDTO> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<User> list = new ArrayList<User>( dtoList.size() );
        for ( UserUpdateDTO userUpdateDTO : dtoList ) {
            list.add( toEntity( userUpdateDTO ) );
        }

        return list;
    }

    @Override
    public List<UserUpdateDTO> toDto(List<User> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<UserUpdateDTO> list = new ArrayList<UserUpdateDTO>( entityList.size() );
        for ( User user : entityList ) {
            list.add( toDto( user ) );
        }

        return list;
    }

    @Override
    public User toEntity(UserUpdateDTO dto) {
        if ( dto == null ) {
            return null;
        }

        User user = new User();

        user.setId( dto.getId() );
        user.setUsername( dto.getUsername() );
        user.setEmail( dto.getEmail() );
        if ( dto.getEnabled() != null ) {
            user.setEnabled( dto.getEnabled() );
        }
        user.setRoles( roleBaseDTOListToRoleList( dto.getRoles() ) );

        return user;
    }

    @Override
    public UserUpdateDTO toDto(User entity) {
        if ( entity == null ) {
            return null;
        }

        UserUpdateDTO userUpdateDTO = new UserUpdateDTO();

        userUpdateDTO.setUsername( entity.getUsername() );
        userUpdateDTO.setEmail( entity.getEmail() );
        userUpdateDTO.setId( entity.getId() );
        userUpdateDTO.setRoles( roleListToRoleBaseDTOList( entity.getRoles() ) );
        userUpdateDTO.setEnabled( entity.isEnabled() );

        return userUpdateDTO;
    }

    protected Role roleBaseDTOToRole(RoleBaseDTO roleBaseDTO) {
        if ( roleBaseDTO == null ) {
            return null;
        }

        Role role = new Role();

        role.setId( roleBaseDTO.getId() );
        role.setName( roleBaseDTO.getName() );

        return role;
    }

    protected List<Role> roleBaseDTOListToRoleList(List<RoleBaseDTO> list) {
        if ( list == null ) {
            return null;
        }

        List<Role> list1 = new ArrayList<Role>( list.size() );
        for ( RoleBaseDTO roleBaseDTO : list ) {
            list1.add( roleBaseDTOToRole( roleBaseDTO ) );
        }

        return list1;
    }

    protected RoleBaseDTO roleToRoleBaseDTO(Role role) {
        if ( role == null ) {
            return null;
        }

        RoleBaseDTO roleBaseDTO = new RoleBaseDTO();

        roleBaseDTO.setId( role.getId() );
        roleBaseDTO.setName( role.getName() );

        return roleBaseDTO;
    }

    protected List<RoleBaseDTO> roleListToRoleBaseDTOList(List<Role> list) {
        if ( list == null ) {
            return null;
        }

        List<RoleBaseDTO> list1 = new ArrayList<RoleBaseDTO>( list.size() );
        for ( Role role : list ) {
            list1.add( roleToRoleBaseDTO( role ) );
        }

        return list1;
    }
}
