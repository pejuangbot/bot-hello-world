package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotationDetailTrx;
import com.nostratech.nurindo.service.dto.PriceQuotationDetailTrxListDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:24+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class PriceQuotationDetailTrxListMapperImpl implements PriceQuotationDetailTrxListMapper {

    @Override
    public PriceQuotationDetailTrxListDTO toDto(PriceQuotationDetailTrx dto) {
        if ( dto == null ) {
            return null;
        }

        PriceQuotationDetailTrxListDTO priceQuotationDetailTrxListDTO = new PriceQuotationDetailTrxListDTO();

        priceQuotationDetailTrxListDTO.setName( dto.getName() );
        priceQuotationDetailTrxListDTO.setHeight( dto.getHeight() );
        priceQuotationDetailTrxListDTO.setQuantity( dto.getQuantity() );
        priceQuotationDetailTrxListDTO.setPrice( dto.getPrice() );
        priceQuotationDetailTrxListDTO.setTotalPrice( dto.getTotalPrice() );
        priceQuotationDetailTrxListDTO.setMsrp( dto.getMsrp() );
        priceQuotationDetailTrxListDTO.setBottomPrice( dto.getBottomPrice() );
        priceQuotationDetailTrxListDTO.setWeight( dto.getWeight() );
        priceQuotationDetailTrxListDTO.setResolution( dto.getResolution() );

        return priceQuotationDetailTrxListDTO;
    }
}
