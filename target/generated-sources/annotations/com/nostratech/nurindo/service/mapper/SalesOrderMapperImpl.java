package com.nostratech.nurindo.service.mapper;

import com.nostratech.nurindo.domain.PriceQuotation;
import com.nostratech.nurindo.domain.SalesOrder;
import com.nostratech.nurindo.service.dto.SalesOrderDTO;
import com.nostratech.nurindo.service.dto.SalesOrderDetailDTO;
import com.nostratech.nurindo.service.dto.SalesOrderListDTO;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-11-12T15:28:23+0700",
    comments = "version: 1.2.0.Final, compiler: javac, environment: Java 1.8.0_101 (Oracle Corporation)"
)
@Component
public class SalesOrderMapperImpl implements SalesOrderMapper {

    @Override
    public SalesOrder toEntity(SalesOrderDTO dto, SalesOrder entity) {
        if ( dto == null ) {
            return null;
        }

        entity.setShipmentDate( shipmentDateToInstant( dto ) );
        entity.setId( dto.getId() );
        entity.setSoNumber( dto.getSoNumber() );
        entity.setPrOrigin( dto.getPrOrigin() );
        entity.setNotes( dto.getNotes() );
        entity.setShipmentType( dto.getShipmentType() );
        entity.setStatus( dto.getStatus() );

        return entity;
    }

    @Override
    public SalesOrderDetailDTO toDto(SalesOrder entity, SalesOrderDetailDTO dto) {
        if ( entity == null ) {
            return null;
        }

        dto.setCreatedDate( createdDateToLong( entity ) );
        String notes = entityPriceQuotationNotes( entity );
        if ( notes != null ) {
            dto.setNotes( notes );
        }
        dto.setShipmentDate( shipmentDateToLong( entity ) );
        dto.setSalesOrderNotes( entity.getNotes() );
        dto.setId( entity.getId() );
        dto.setSoNumber( entity.getSoNumber() );
        dto.setStatus( entity.getStatus() );
        dto.setPrOrigin( entity.getPrOrigin() );
        dto.setShipmentType( entity.getShipmentType() );

        return dto;
    }

    @Override
    public SalesOrderListDTO toDto(SalesOrder entity, SalesOrderListDTO dto) {
        if ( entity == null ) {
            return null;
        }

        dto.setCreatedDate( createdDateToLong( entity ) );
        dto.setId( entity.getId() );
        dto.setStatus( entity.getStatus() );
        dto.setSoNumber( entity.getSoNumber() );

        return dto;
    }

    private String entityPriceQuotationNotes(SalesOrder salesOrder) {
        if ( salesOrder == null ) {
            return null;
        }
        PriceQuotation priceQuotation = salesOrder.getPriceQuotation();
        if ( priceQuotation == null ) {
            return null;
        }
        String notes = priceQuotation.getNotes();
        if ( notes == null ) {
            return null;
        }
        return notes;
    }
}
